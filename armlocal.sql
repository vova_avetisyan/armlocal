/*
 Navicat MySQL Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 100119
 Source Host           : localhost:3306
 Source Schema         : armlocal

 Target Server Type    : MySQL
 Target Server Version : 100119
 File Encoding         : 65001

 Date: 03/01/2021 20:23:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for addons
-- ----------------------------
DROP TABLE IF EXISTS `addons`;
CREATE TABLE `addons`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `unique_identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int NOT NULL,
  `created_at` int NULL DEFAULT NULL,
  `updated_at` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of addons
-- ----------------------------
INSERT INTO `addons` VALUES (1, 'Facebook messenger plugin', 'fb_messenger', '1.0', 1, 1545292800, NULL);
INSERT INTO `addons` VALUES (2, 'Shop', 'shop', '1.0', 1, 1545292800, NULL);

-- ----------------------------
-- Table structure for amenities
-- ----------------------------
DROP TABLE IF EXISTS `amenities`;
CREATE TABLE `amenities`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `slug` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of amenities
-- ----------------------------

-- ----------------------------
-- Table structure for beauty_service
-- ----------------------------
DROP TABLE IF EXISTS `beauty_service`;
CREATE TABLE `beauty_service`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `listing_id` int NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `price` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `service_times` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `photo` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of beauty_service
-- ----------------------------
INSERT INTO `beauty_service` VALUES (1, 4, 'serv 1', '37', '23:38,12:39,2', '9145ad57103785b281a55fa1a7add88c.jpg');
INSERT INTO `beauty_service` VALUES (2, 4, 'serv 2', '45', '14:39,16:39,1', '9e83112f922d7636a610c7be6b3e66c3.jpg');

-- ----------------------------
-- Table structure for blog_comments
-- ----------------------------
DROP TABLE IF EXISTS `blog_comments`;
CREATE TABLE `blog_comments`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `comment` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `blog_id` int NULL DEFAULT NULL,
  `user_id` int NULL DEFAULT NULL,
  `under_comment_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '0 == main comment, 0 != under comment',
  `added_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `modified_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of blog_comments
-- ----------------------------

-- ----------------------------
-- Table structure for blogs
-- ----------------------------
DROP TABLE IF EXISTS `blogs`;
CREATE TABLE `blogs`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `blog_text` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `category_id` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `user_id` int NULL DEFAULT NULL,
  `status` int NOT NULL,
  `blog_thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `blog_cover` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `added_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `modified_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of blogs
-- ----------------------------

-- ----------------------------
-- Table structure for booking
-- ----------------------------
DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `requester_id` int NOT NULL,
  `listing_id` int NOT NULL,
  `booking_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `additional_information` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `listing_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `status` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of booking
-- ----------------------------

-- ----------------------------
-- Table structure for car_details
-- ----------------------------
DROP TABLE IF EXISTS `car_details`;
CREATE TABLE `car_details`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `listing_id` int NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `fuel_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `city_mpg` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `highway_mpg` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `drivetrain` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `engine` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `mileage` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `exterior_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `interior_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `stock` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `transmission` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `vin` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `variant` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `photo` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `new_used` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of car_details
-- ----------------------------
INSERT INTO `car_details` VALUES (1, 8, 'Certified 2019 Nissan Sentra SRs', 'Buy your next car 100% online. Get instant upfront pricing on all our vehicles. Complete everything from the comfort of your home - without ever having to visit our dealership. Home delivery within 50 miles. Nissan Certified Pre-Owned Certified, CVT with Xtronic, Charcoal w/Cloth Seat Trim. Certified. Deep Blue Pearl 2019 Nissan Sentra SR FWD CVT with Xtronic 1.8L 4-Cylinder DOHC 16V Recent Arrival! Odometer is 34458 miles below market average! 29/37 City/Highway MPG Nissan Certified Pre-Owned Details: * Limited Warranty: 84 Month/100,000 Mile (whichever comes first) from original in-service date * Warranty Deductible: $100 * 167 Point Inspection * Transferable Warranty * Vehicle History * Roadside Assistance * Includes Car Rental and Trip Interruption Reimbursement Call STAR Nissan today at 847-278-0614 and ask us about our Used+ Program Specials.', '15500', 'Gasoline', '14 ', '20', '4WD', '4.6L V8 24V MPFI SOHC', '91,504', 'White', 'Gray', '10509', '6-Speed Automatic', '1FTEW1E85AFC41575', 'car,nissan', '144d5a834cf89ec3ef24d466bb01bd8b.jpg');
INSERT INTO `car_details` VALUES (2, 10, '2020 Nissan Kicks SV', 'CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags', '27995', 'Gasoline', '14 ', '20', '4WD', '4.6L V8 24V MPFI SOHC', '88,504', 'Blue', 'Gray', '10510', '6-Speed Automatic', '1FTEW1E85AFC41575', 'car,auto,speed,nissan', 'f4ad8c2a41898f7336e5f61692726465.jpg');
INSERT INTO `car_details` VALUES (3, 11, '2020 Chevrolet Trax LS', 'CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags', '30110', 'Gasoline', '14 ', '20', '4WD', '4.6L V8 24V MPFI SOHC', '87,312', 'White', 'Gray', '20509', '6-Speed Automatic', '1FTEW1E85AFC41575', 'car,auto,speed,chevrolet', '96fb3e1341917c258bf6571819f93b65.jpg');
INSERT INTO `car_details` VALUES (4, 12, '2021 Chevrolet Spark LS', 'CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags', '26895', 'Gasoline', '14 ', '20', '4WD', '4.6L V8 24V MPFI SOHC', '91,505', 'Blue', 'Blue', '10509', '6-Speed Automatic', '1FTEW1E85AFC41575', 'car,auto,speed,chevrolet', 'e82512b2018ce8c5530c5b48a85d94a0.jpg');
INSERT INTO `car_details` VALUES (5, 13, '2021 Chevrolet Malibu RS', 'CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags', '27995', 'Gasoline', '14 ', '20', '4WD', '4.6L V8 24V MPFI SOHC', '91,504', 'Black', 'Gray', '10509', '6-Speed Automatic', '1FTEW1E85AFC41575', 'car,auto,speed,chevrolet', '0afbe2a5b95483d4b52e4057814da812.jpg');
INSERT INTO `car_details` VALUES (6, 14, '2020 BMW i3 120Ah w/Range Extender', 'CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags', '25150', 'Gasoline', '14 ', '20', '4WD', '4.6L V8 24V MPFI SOHC', '91,504', 'Blue', 'Gray', '20509', '6-Speed Automatic', '1FTEW1E85AFC41575', 'car,auto,speed,bmw', '822030794677b0a78ee5cc78e80ff5fc.jpg');
INSERT INTO `car_details` VALUES (7, 15, '2019 BMW X3 xDrive30i', 'CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags', '27995', 'Gasoline', '14 ', '20', '4WD', '4.6L V8 24V MPFI SOHC', '91,504', 'Blue', 'Red', '11509', '6-Speed Automatic', '1FTEW1E85AFC41575', 'car,auto,speed,bmw', 'c41abcc1db9ae9ddd60f73ab6ab73ec6.jpg');
INSERT INTO `car_details` VALUES (8, 16, '2012 BMW 750 Li xDrive', 'CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags', '19990', 'Gasoline', '14 ', '20', '4WD', '4.6L V8 24V MPFI SOHC', '91,504', 'black', 'Gray', '10509', '6-Speed Automatic', '1FTEW1E85AFC41575', 'car,auto,speed,bmw', '28bc048aa816a25d4b54a5658b95a8f2.jpg');
INSERT INTO `car_details` VALUES (9, 17, '2015 Acura RDX Technology Package', 'CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags', '19998', 'Gasoline', '14 ', '20', '4WD', '4.6L V8 24V MPFI SOHC', '92,504', 'Blue', 'White', '10509', '6-Speed Automatic', '1FTEW1E85AFC41575', 'car,acua,auto,speed', 'e5fa111bb9331342e15de74860555b78.jpg');
INSERT INTO `car_details` VALUES (10, 18, '2018 Acura RLX Technology Package', 'CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags', '27995', 'Gasoline', '14 ', '20', '4WD', '4.6L V8 24V MPFI SOHC', '91,504', 'White', 'Gray', '30509', '6-Speed Automatic', '1FTEW1E85AFC41575', 'car,acua,auto,speed', '33e99c2732a84ab9081c66b7ee2e43b3.jpg');
INSERT INTO `car_details` VALUES (11, 19, '2018 Acura TLX FWD', 'CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags', '28995', 'Gasoline', '14 ', '20', '4WD', '4.6L V8 24V MPFI SOHC', '93,504', 'Blue', 'Orange', '10509', '6-Speed Automatic', '1FTEW1E85AFC41575', 'car,acua,auto,speed', 'b0bebf4566f4c36ac68e7c77454d9b6a.jpg');
INSERT INTO `car_details` VALUES (12, 20, '2019 Acura RDX A-Spec', 'CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags', '34650', 'Gasoline', '14 ', '20', '4WD', '4.6L V8 24V MPFI SOHC', '91,504', 'black', 'Gray', '40609', '6-Speed Automatic', '1FTEW1E85AFC41575', 'car,acua,auto,speed', '0ad59dba5dd523458297b3811f3fd53b.jpg');
INSERT INTO `car_details` VALUES (13, 21, '2018 Acura RDX Base', 'CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags', '27995', 'Gasoline', '14 ', '20', '4WD', '4.6L V8 24V MPFI SOHC', '95,504', 'Blue', 'Yellow', '10519', '6-Speed Automatic', '1FTEW1E85AFC41575', 'car,acua,auto,speed', 'bacf85738d82fbf14a3748ccb5422f6e.jpg');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `parent` int NULL DEFAULT 0,
  `icon_class` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `slug` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 114 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (2, 0, 'fas fa-h-square', 'Hotel', 'hotel', 'b76ca05ed6d4be94024a805ec9ae1521.jpg');
INSERT INTO `category` VALUES (3, 0, 'fas fa-utensils', 'Restaurant', 'restaurant', 'fe697f1511bc3cede5ea9283cf7adfc1.jpg');
INSERT INTO `category` VALUES (4, 0, 'fas fa-flask', 'Beauty', 'beauty', '57289dbd8835b2434790ed09a9842bc3.jpg');
INSERT INTO `category` VALUES (5, 0, 'fas fa-briefcase', 'Job', 'job', 'ced4445e31b22534051a5c4dccf5dec1.jpg');
INSERT INTO `category` VALUES (6, 0, 'fas fa-bus', 'Car', 'car', '047514b049b759c1e01d7b1e119c1c1b.jpg');
INSERT INTO `category` VALUES (7, 0, 'fas fa-home', 'Real estate', 'real-estate', '2760abe0ab0eaa8be28f5d4220c1bf38.jpg');
INSERT INTO `category` VALUES (10, 6, 'fas fa-bus', 'Acura', 'acura', NULL);
INSERT INTO `category` VALUES (11, 6, 'fas fa-bus', 'BMW', 'bmw', NULL);
INSERT INTO `category` VALUES (12, 6, 'fas fa-bus', 'Chevrolet', 'chevrolet', NULL);
INSERT INTO `category` VALUES (13, 6, 'fas fa-bus', 'Ford', 'ford', NULL);
INSERT INTO `category` VALUES (14, 6, 'fas fa-bus', 'Honda', 'honda', NULL);
INSERT INTO `category` VALUES (15, 6, 'fas fa-bus', 'Jeep', 'jeep', NULL);
INSERT INTO `category` VALUES (16, 6, 'fas fa-bus', 'Lexus', 'lexus', NULL);
INSERT INTO `category` VALUES (17, 6, 'fas fa-bus', 'Mercedes-Benz', 'mercedes-benz', NULL);
INSERT INTO `category` VALUES (18, 6, 'fas fa-bus', 'Nissan', 'nissan', NULL);
INSERT INTO `category` VALUES (19, 6, 'fas fa-bus', 'Toyota', 'toyota', NULL);
INSERT INTO `category` VALUES (20, 7, 'fas fa-home', 'Any', 'any', NULL);
INSERT INTO `category` VALUES (21, 7, 'fas fa-home', 'Townhome', 'townhome', NULL);
INSERT INTO `category` VALUES (22, 7, 'fas fa-home', 'Single Family', 'single-family', NULL);
INSERT INTO `category` VALUES (23, 7, 'fas fa-home', 'Apartment', 'apartment', NULL);
INSERT INTO `category` VALUES (24, 7, 'fas fa-home', 'Condo', 'condo', NULL);
INSERT INTO `category` VALUES (25, 5, 'fas fa-briefcase', 'Software development', 'software-development', NULL);
INSERT INTO `category` VALUES (26, 5, 'fas fa-briefcase', 'Quality Assurance /Control', 'quality-assurance-control', NULL);
INSERT INTO `category` VALUES (27, 5, 'fas fa-briefcase', 'Web/Graphic design', 'web-graphic-design', NULL);
INSERT INTO `category` VALUES (28, 5, 'fas fa-briefcase', 'Product/Project Management', 'product-project-management', NULL);
INSERT INTO `category` VALUES (29, 5, 'fas fa-briefcase', 'Other IT', 'other-it', NULL);
INSERT INTO `category` VALUES (30, 5, 'fas fa-briefcase', 'Sales/service management', 'sales-service-management', NULL);
INSERT INTO `category` VALUES (31, 5, 'fas fa-briefcase', 'Administrative/office-work', 'administrative-office-work', NULL);
INSERT INTO `category` VALUES (32, 5, 'fas fa-briefcase', 'Tourism / Hospitality / HoReCa', 'tourism-hospitality-horeca', NULL);
INSERT INTO `category` VALUES (33, 5, 'fas fa-briefcase', 'Marketing/Advertising', 'marketing-advertising', NULL);
INSERT INTO `category` VALUES (34, 5, 'fas fa-briefcase', 'Communications / Journalism / PR', 'communications-journalism-pr', NULL);
INSERT INTO `category` VALUES (35, 5, 'fas fa-briefcase', 'Accounting / Bookkeeping / Cash register', 'accounting-bookkeeping-cash-register', NULL);
INSERT INTO `category` VALUES (36, 5, 'fas fa-briefcase', 'Finance Management', 'finance-management', NULL);
INSERT INTO `category` VALUES (37, 5, 'fas fa-briefcase', 'Banking/credit', 'banking-credit', NULL);
INSERT INTO `category` VALUES (38, 5, 'fas fa-briefcase', 'Education/training', 'education-training', NULL);
INSERT INTO `category` VALUES (39, 5, 'fas fa-briefcase', 'Legal', 'legal', NULL);
INSERT INTO `category` VALUES (40, 5, 'fas fa-briefcase', 'Audit/Compliance', 'audit-compliance', NULL);
INSERT INTO `category` VALUES (41, 5, 'fas fa-briefcase', 'Healthcare/Pharmaceutical', 'healthcare-pharmaceutical', NULL);
INSERT INTO `category` VALUES (42, 5, 'fas fa-briefcase', 'Construction', 'construction', NULL);
INSERT INTO `category` VALUES (43, 5, 'fas fa-briefcase', 'Human Resources', 'human-resources', NULL);
INSERT INTO `category` VALUES (44, 5, 'fas fa-briefcase', 'Procurement / Logistics / Courier', 'procurement-logistics-courier', NULL);
INSERT INTO `category` VALUES (45, 5, 'fas fa-briefcase', 'Production', 'production', NULL);
INSERT INTO `category` VALUES (46, 5, 'fas fa-briefcase', 'Business/Management', 'business-management', NULL);
INSERT INTO `category` VALUES (47, 5, 'fas fa-briefcase', 'Art/Design/Architecture', 'art-design-architecture', NULL);
INSERT INTO `category` VALUES (48, 5, 'fas fa-briefcase', 'General/professional / Other services', 'general-professional-other-services', NULL);
INSERT INTO `category` VALUES (49, 5, 'fas fa-briefcase', 'IT security/Networks', 'it-security-networks', NULL);
INSERT INTO `category` VALUES (50, 5, 'fas fa-briefcase', 'NGO/Nonprofit', 'ngo-nonprofit', NULL);
INSERT INTO `category` VALUES (51, 5, 'fas fa-briefcase', 'Hospitality/Entertainment', 'hospitality-entertainment', NULL);
INSERT INTO `category` VALUES (52, 5, 'fas fa-briefcase', 'Data Science/Data Analytics', 'data-science-data-analytics', NULL);
INSERT INTO `category` VALUES (53, 5, 'fas fa-briefcase', 'Foreign language', 'foreign-language', NULL);
INSERT INTO `category` VALUES (54, 5, 'fas fa-briefcase', 'Economics', 'economics', NULL);
INSERT INTO `category` VALUES (55, 5, 'fas fa-briefcase', 'Hardware Design / Engineering', 'hardware-design-engineering', NULL);
INSERT INTO `category` VALUES (56, 5, 'fas fa-briefcase', 'Data Collection &amp; Analytics', 'data-collection-amp-analytics', NULL);
INSERT INTO `category` VALUES (57, 5, 'fas fa-briefcase', 'Mechanical/Engineering', 'mechanical-engineering', NULL);
INSERT INTO `category` VALUES (58, 5, 'fas fa-briefcase', 'System Admin/Engineer', 'system-admin-engineer', NULL);
INSERT INTO `category` VALUES (59, 5, 'fas fa-briefcase', 'Retail business', 'retail-business', NULL);
INSERT INTO `category` VALUES (60, 5, 'fas fa-briefcase', 'Network Administration', 'network-administration', NULL);
INSERT INTO `category` VALUES (61, 5, 'fas fa-briefcase', 'Consultancy', 'consultancy', NULL);
INSERT INTO `category` VALUES (62, 5, 'fas fa-briefcase', 'Content writing', 'content-writing', NULL);
INSERT INTO `category` VALUES (63, 5, 'fas fa-briefcase', 'Security', 'security', NULL);
INSERT INTO `category` VALUES (64, 5, 'fas fa-briefcase', 'Aviation', 'aviation', NULL);
INSERT INTO `category` VALUES (65, 6, 'fas fa-bus', 'Alfa Romeo', 'alfa-romeo', NULL);
INSERT INTO `category` VALUES (66, 6, 'fas fa-bus', 'Aston Martin', 'aston-martin', NULL);
INSERT INTO `category` VALUES (67, 6, 'fas fa-bus', 'Audi', 'audi', NULL);
INSERT INTO `category` VALUES (68, 6, 'fas fa-bus', 'Austin-Healey', 'austin-healey', NULL);
INSERT INTO `category` VALUES (69, 6, 'fas fa-bus', 'Bentley', 'bentley', NULL);
INSERT INTO `category` VALUES (70, 6, 'fas fa-bus', 'Bugatti', 'bugatti', NULL);
INSERT INTO `category` VALUES (71, 6, 'fas fa-bus', 'Buick', 'buick', NULL);
INSERT INTO `category` VALUES (72, 6, 'fas fa-bus', 'Cadillac', 'cadillac', NULL);
INSERT INTO `category` VALUES (73, 6, 'fas fa-bus', 'Chrysler', 'chrysler', NULL);
INSERT INTO `category` VALUES (74, 6, 'fas fa-bus', 'Dodge', 'dodge', NULL);
INSERT INTO `category` VALUES (75, 6, 'fas fa-bus', 'FIAT', 'fiat', NULL);
INSERT INTO `category` VALUES (76, 6, 'fas fa-bus', 'Ferrari', 'ferrari', NULL);
INSERT INTO `category` VALUES (77, 6, 'fas fa-bus', 'Fisker', 'fisker', NULL);
INSERT INTO `category` VALUES (78, 6, 'fas fa-bus', 'GMC', 'gmc', NULL);
INSERT INTO `category` VALUES (79, 6, 'fas fa-bus', 'Genesis', 'genesis', NULL);
INSERT INTO `category` VALUES (80, 6, 'fas fa-bus', 'Hummer', 'hummer', NULL);
INSERT INTO `category` VALUES (81, 6, 'fas fa-bus', 'Hyundai', 'hyundai', NULL);
INSERT INTO `category` VALUES (82, 6, 'fas fa-bus', 'INFINITI', 'infiniti', NULL);
INSERT INTO `category` VALUES (83, 6, 'fas fa-bus', 'Isuzu', 'isuzu', NULL);
INSERT INTO `category` VALUES (84, 6, 'fas fa-bus', 'Jaguar', 'jaguar', NULL);
INSERT INTO `category` VALUES (85, 6, 'fas fa-bus', 'Karma', 'karma', NULL);
INSERT INTO `category` VALUES (86, 6, 'fas fa-bus', 'Kia', 'kia', NULL);
INSERT INTO `category` VALUES (87, 6, 'fas fa-bus', 'Lamborghini', 'lamborghini', NULL);
INSERT INTO `category` VALUES (88, 6, 'fas fa-bus', 'Land Rover', 'land-rover', NULL);
INSERT INTO `category` VALUES (89, 6, 'fas fa-bus', 'Lincoln', 'lincoln', NULL);
INSERT INTO `category` VALUES (90, 6, 'fas fa-bus', 'Lotus', 'lotus', NULL);
INSERT INTO `category` VALUES (91, 6, 'fas fa-bus', 'MINI', 'mini', NULL);
INSERT INTO `category` VALUES (92, 6, 'fas fa-bus', 'Maserati', 'maserati', NULL);
INSERT INTO `category` VALUES (93, 6, 'fas fa-bus', 'Maybach', 'maybach', NULL);
INSERT INTO `category` VALUES (94, 6, 'fas fa-bus', 'Mazda', 'mazda', NULL);
INSERT INTO `category` VALUES (95, 6, 'fas fa-bus', 'McLaren', 'mclaren', NULL);
INSERT INTO `category` VALUES (96, 6, 'fas fa-bus', 'Mercury', 'mercury', NULL);
INSERT INTO `category` VALUES (97, 6, 'fas fa-bus', 'Mitsubishi', 'mitsubishi', NULL);
INSERT INTO `category` VALUES (98, 6, 'fas fa-bus', 'Oldsmobile', 'oldsmobile', NULL);
INSERT INTO `category` VALUES (99, 6, 'fas fa-bus', 'Plymouth', 'plymouth', NULL);
INSERT INTO `category` VALUES (100, 6, 'fas fa-bus', 'Pontiac', 'pontiac', NULL);
INSERT INTO `category` VALUES (101, 6, 'fas fa-bus', 'Porsche', 'porsche', NULL);
INSERT INTO `category` VALUES (102, 6, 'fas fa-bus', 'RAM', 'ram', NULL);
INSERT INTO `category` VALUES (103, 6, 'fas fa-bus', 'Rolls-Royce', 'rolls-royce', NULL);
INSERT INTO `category` VALUES (104, 6, 'fas fa-bus', 'Saab', 'saab', NULL);
INSERT INTO `category` VALUES (105, 6, 'fas fa-bus', 'Saturn', 'saturn', NULL);
INSERT INTO `category` VALUES (106, 6, 'fas fa-bus', 'Scion', 'scion', NULL);
INSERT INTO `category` VALUES (107, 6, 'fas fa-bus', 'Subaru', 'subaru', NULL);
INSERT INTO `category` VALUES (108, 6, 'fas fa-bus', 'Suzuki', 'suzuki', NULL);
INSERT INTO `category` VALUES (109, 6, 'fas fa-bus', 'Tesla', 'tesla', NULL);
INSERT INTO `category` VALUES (110, 6, 'fas fa-bus', 'Triumph', 'triumph', NULL);
INSERT INTO `category` VALUES (111, 6, 'fas fa-bus', 'Volkswagen', 'volkswagen', NULL);
INSERT INTO `category` VALUES (112, 6, 'fas fa-bus', 'Volvo', 'volvo', NULL);
INSERT INTO `category` VALUES (113, 6, 'fas fa-bus', 'smart', 'smart', NULL);

-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions`  (
  `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int NOT NULL DEFAULT 0,
  `data` blob NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('qa1spqvln9ji2kos5v8v0h95qdtv2h47', '127.0.0.1', 1608927114, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630383932373036313B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('jocr30ikvk5qm44s9vm57ssg1nspef3j', '127.0.0.1', 1608927824, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630383932373831343B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('14rue672oohl11ap1936m9o1lq9st2k6', '127.0.0.1', 1608928428, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630383932383135393B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('fhophctkq28vicfph5pjamffnic625fu', '127.0.0.1', 1608928760, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630383932383436333B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('g4llaaeuipo9s0hi72kdfli80069lhi3', '127.0.0.1', 1608929016, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630383932383737323B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('1efpq72di4p3j5u3q51s4v1b7atj0p9u', '127.0.0.1', 1608929306, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630383932393039343B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('up3kppi4kjvhfirh8dlagfu0amnotoa1', '127.0.0.1', 1608929415, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630383932393430343B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('3kghmcsg1kfb568b44thm7e4ar9d4i9t', '127.0.0.1', 1608930990, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630383933303832303B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('nmrabfdu9gqsrha1jbr04tl7pkr2n9lf', '127.0.0.1', 1608931432, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630383933313230313B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('canjhe7am1ic29r52hkob5sq1oskslk2', '127.0.0.1', 1608931758, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630383933313534373B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('ub45ifedc40t28n7r00gu20052q424jb', '127.0.0.1', 1608932218, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630383933313931393B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('as8ilb9h774rk6mssr5vav7d61p71v5n', '127.0.0.1', 1608932507, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630383933323232323B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('oj14v4iedh33hmfqvvat45vjp78pgd0u', '127.0.0.1', 1608932565, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630383933323536353B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('v06u2atbpe7iamfeipaof3pihgiv7ro2', '127.0.0.1', 1608932959, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630383933323935393B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('vu2lqe6pju4rm964jga9sidiufne4ffp', '127.0.0.1', 1609017657, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393031373336363B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('ld631vt51mschnqad4engrbij7mhk1k9', '127.0.0.1', 1609017669, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393031373636393B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('cpi49flg0gh5bm00v9m0lkfj2dglu4ou', '127.0.0.1', 1609020071, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393031393838363B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('p84vqe6h8eo17lua6fk9eur7uvft49dn', '127.0.0.1', 1609020527, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393032303238323B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('sng6e0f1ur6niht57844rko12g9f5j4t', '127.0.0.1', 1609020932, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393032303633353B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('dv9a4jca8aatbh436bfecuhoqbd9dvdk', '127.0.0.1', 1609021145, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393032303936393B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('1pf22eb9723r6h4tf3ogho72ehmouhvk', '127.0.0.1', 1609021927, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393032313931303B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('0b7k5ligo0b9hueksndq0o5qvjjq0d99', '127.0.0.1', 1609095734, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393039353439303B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('n7duldbhju49dvvj463nl976vccjetsb', '127.0.0.1', 1609095898, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393039353839373B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('p3n6prossar7pf2memcl8bascncrlrmq', '127.0.0.1', 1609101435, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393130313433313B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('6784nng3a6h1hkd6gv8kh5lc88ph8j65', '127.0.0.1', 1609103227, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393130323937303B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('m1q14v6s7lbrtmcv0jrif7im5pc470mn', '127.0.0.1', 1609103377, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393130333330353B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2233223B726F6C655F69647C733A313A2232223B726F6C657C733A343A2255736572223B6E616D657C733A31343A22427573696E657373204D616E2031223B757365725F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('3s3gupkese8amqrstbburc7uha0h83vb', '127.0.0.1', 1609104293, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393130343239333B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('lct66blhu4cqlq9fdpmqbbqgriuaubp2', '127.0.0.1', 1609185052, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393138343936333B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('lbhamgih56l9vath6f8mcoj24ntcj77l', '127.0.0.1', 1609185508, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393138353238383B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('naduolqaikgnl9uj7qe0lp5vj0v9dkdk', '127.0.0.1', 1609185931, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393138353634313B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('ri0ii91q6fn1fufht6df0madmolfau9j', '127.0.0.1', 1609186355, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393138363136353B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('42es4og9faukc3d1puintan4bb44k69m', '127.0.0.1', 1609186749, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393138363534333B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('u8oh33a5uiva0nqjeqoscs80ffmmhq4s', '127.0.0.1', 1609187163, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393138363839333B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('j8urcrthkriucclnehaph4qbqamgmb1f', '127.0.0.1', 1609187597, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393138373535393B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('qtabu0ko3t3g53j6rp4a21o223hpvjd9', '127.0.0.1', 1609188531, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393138383234393B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('qgt4pb5rl45pm1k8kfgut31r6jeonbqg', '127.0.0.1', 1609188867, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393138383539353B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('h9k5tufnlg7vaqmcpnp8av6m7mbgobq0', '127.0.0.1', 1609267612, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393236373631323B);
INSERT INTO `ci_sessions` VALUES ('0pcq5ip4bq7sjisodetv4mgr81p9jg19', '127.0.0.1', 1609268701, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393236383636333B);
INSERT INTO `ci_sessions` VALUES ('800a2nmn5m2mve0benhupeg3sv36ovls', '127.0.0.1', 1609286837, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393238363833373B);
INSERT INTO `ci_sessions` VALUES ('88q1vos24am5bqogqptig7g0hms3gbrm', '127.0.0.1', 1609339782, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393333393738323B);
INSERT INTO `ci_sessions` VALUES ('ttk9inmeltukgvpp4p27u2b6k77m76r2', '127.0.0.1', 1609512314, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531323330333B);
INSERT INTO `ci_sessions` VALUES ('3r3qs8bthrupbd151uj5hemtgih41n7e', '127.0.0.1', 1609512740, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531323733393B);
INSERT INTO `ci_sessions` VALUES ('k6mso6c0uqd13u8l82tq9vv7vf70v7f5', '127.0.0.1', 1609512740, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531323734303B);
INSERT INTO `ci_sessions` VALUES ('ehd4t96allgi2bpcl6pvb7bhurg9kmtv', '127.0.0.1', 1609512908, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531323734303B);
INSERT INTO `ci_sessions` VALUES ('qqdv0bsscpnkr3s4ugd8tv46gmqh0kbs', '127.0.0.1', 1609513865, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531333538343B);
INSERT INTO `ci_sessions` VALUES ('8vro00nvgi967snqbvfgd5k8l5259nql', '127.0.0.1', 1609514228, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531333939353B);
INSERT INTO `ci_sessions` VALUES ('81h9p8d91474rocoa0sm9bsgnkqqka9l', '127.0.0.1', 1609514483, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531343337383B);
INSERT INTO `ci_sessions` VALUES ('sait4l8h828rqhrbddo8slijt7k07fli', '127.0.0.1', 1609515091, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531343837333B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('o7g4d7snov2iba50qt3lcvg2lus62nm2', '127.0.0.1', 1609515483, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531353138333B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('vas34d8s8ntqbipq0l0l2bdj231a07h1', '127.0.0.1', 1609515484, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531353438343B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('m50drku2alvr9qve7h8lv6o2sddbc1kg', '127.0.0.1', 1609515773, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531353438343B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('6sng2gvkse2g3jh0ngjj6gp33mhmg5d1', '127.0.0.1', 1609515974, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531353830303B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('pb7a4e5331lmct4283bqq9j2ituu85jg', '127.0.0.1', 1609518288, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531383035373B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('jub0kb5qat3bttlsbsuncgctk4qhg9u8', '127.0.0.1', 1609518935, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531383634383B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('p649m52epicsk53o1eu8k3ucavurqlpj', '127.0.0.1', 1609519201, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531383936333B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('4k32u2gcjaokau351qccjmmolvt3gtdr', '127.0.0.1', 1609519707, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531393434383B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('s1mkcrks8kdcand98htjhna2bssnfk12', '127.0.0.1', 1609520013, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393531393736323B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('udkugnbo48a7acvmbcp4papeqj6ck459', '127.0.0.1', 1609520360, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393532303138373B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('v4cvrb6q26iertjq0udd0e1dj3lq9nmb', '127.0.0.1', 1609520632, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393532303534373B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('jiblne1qh3heq5ho3o8hasa96uft3oei', '127.0.0.1', 1609520904, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393532303930343B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('hhh2qieka4gtbl6jj47q3fseo2ir16i8', '127.0.0.1', 1609675898, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393637353831363B);
INSERT INTO `ci_sessions` VALUES ('ua34nrf6iuo3r472tbp2uo9e19iar6rp', '127.0.0.1', 1609677689, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393637373638333B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('cn6mjh62dijabc6d9oqdc295q5m7huik', '127.0.0.1', 1609678308, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393637383034303B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2233223B726F6C655F69647C733A313A2232223B726F6C657C733A343A2255736572223B6E616D657C733A31343A22427573696E657373204D616E2031223B757365725F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('n8igp3hqsal1b49ba8cip61nj50j9us5', '127.0.0.1', 1609678648, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393637383337343B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('g1apuk4lonsh720nuc0u5g0tnq8stgam', '127.0.0.1', 1609678770, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393637383638323B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('n2tvr72bephq8ikgu2dtm6b5g61t65pp', '127.0.0.1', 1609679198, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393637383938363B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('cl1jp0p2uqveehsbdtffuhl4qa4um2kp', '127.0.0.1', 1609679807, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393637393531393B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('mev8518vn4n2a7d8vh2tq5g0mgbgbaoa', '127.0.0.1', 1609680180, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393637393931353B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('rv4saofac461durmhdd0qei7dhhv69th', '127.0.0.1', 1609680842, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393638303631383B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('v6c4mgqt3j6dab0equerg05ooi5fk37j', '127.0.0.1', 1609681029, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393638313032333B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('s5dq5pt8erfi4fkmfimef1ti132tvok2', '127.0.0.1', 1609686550, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393638363331353B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('bnk0bnvhtvbdfino7cbggbdr23rvolqu', '127.0.0.1', 1609686966, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393638363636363B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('f8j5862phfdsjp12ap8phdrg9jdel6m0', '127.0.0.1', 1609687217, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393638373032353B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('8647fof24sbs7i8gk2orp0rme8om6fmh', '127.0.0.1', 1609687533, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393638373332373B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('5dfc38nvkb82crhnoc68gr6kqdp96rcd', '127.0.0.1', 1609688041, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393638373938303B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('n1dmg7nfotfcgtchc0ibe73q0ssfcvb1', '127.0.0.1', 1609688367, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393638383333343B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('kuknc3dk7luou9plf89liise0v48ss3p', '127.0.0.1', 1609689282, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393638383938343B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('g3c3hn6v9j4lk3ut87tss4no2bsjuvue', '127.0.0.1', 1609689521, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393638393238353B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('gc7tmfmoaf3a8cp2ju271hsojq0vf7pk', '127.0.0.1', 1609689834, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393638393634383B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('9njit444rvd6oa5pbhhnbg883lglu9u8', '127.0.0.1', 1609690428, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393639303134313B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('nhvgukuc0ehh648ja2a0bhutrc67a7km', '127.0.0.1', 1609690737, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393639303434323B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('57c3286vcn2n5s0lg686hi3sjh0b5pi3', '127.0.0.1', 1609690776, 0x5F5F63695F6C6173745F726567656E65726174657C693A313630393639303735333B69735F6C6F676765645F696E7C693A313B757365725F69647C733A313A2231223B726F6C655F69647C733A313A2231223B726F6C657C733A353A2241646D696E223B6E616D657C733A353A2241646D696E223B61646D696E5F6C6F67696E7C733A313A2231223B);

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `country_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 315 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES (1, 'Abilene', 'abilene', 230);
INSERT INTO `city` VALUES (2, 'Akron', 'akron', 230);
INSERT INTO `city` VALUES (3, 'Albuquerque', 'albuquerque', 230);
INSERT INTO `city` VALUES (4, 'Alexandria', 'alexandria', 230);
INSERT INTO `city` VALUES (5, 'Allen', 'allen', 230);
INSERT INTO `city` VALUES (6, 'Allentown', 'allentown', 230);
INSERT INTO `city` VALUES (7, 'Amarillo', 'amarillo', 230);
INSERT INTO `city` VALUES (8, 'Anaheim', 'anaheim', 230);
INSERT INTO `city` VALUES (9, 'Anchorage', 'anchorage', 230);
INSERT INTO `city` VALUES (10, 'Ann Arbor', 'ann arbor', 230);
INSERT INTO `city` VALUES (11, 'Antioch', 'antioch', 230);
INSERT INTO `city` VALUES (12, 'Arlington', 'arlington', 230);
INSERT INTO `city` VALUES (13, 'Arvada', 'arvada', 230);
INSERT INTO `city` VALUES (14, 'Athens', 'athens', 230);
INSERT INTO `city` VALUES (15, 'Atlanta', 'atlanta', 230);
INSERT INTO `city` VALUES (16, 'Augusta', 'augusta', 230);
INSERT INTO `city` VALUES (17, 'Aurora', 'aurora', 230);
INSERT INTO `city` VALUES (18, 'Austin', 'austin', 230);
INSERT INTO `city` VALUES (19, 'Bakersfield', 'bakersfield', 230);
INSERT INTO `city` VALUES (20, 'Baltimore', 'baltimore', 230);
INSERT INTO `city` VALUES (21, 'Baton Rouge', 'baton rouge', 230);
INSERT INTO `city` VALUES (22, 'Beaumont', 'beaumont', 230);
INSERT INTO `city` VALUES (23, 'Bellevue', 'bellevue', 230);
INSERT INTO `city` VALUES (24, 'Bend', 'bend', 230);
INSERT INTO `city` VALUES (25, 'Berkeley', 'berkeley', 230);
INSERT INTO `city` VALUES (26, 'Billings', 'billings', 230);
INSERT INTO `city` VALUES (27, 'Birmingham', 'birmingham', 230);
INSERT INTO `city` VALUES (28, 'Boise', 'boise', 230);
INSERT INTO `city` VALUES (29, 'Boston', 'boston', 230);
INSERT INTO `city` VALUES (30, 'Boulder', 'boulder', 230);
INSERT INTO `city` VALUES (31, 'Bridgeport', 'bridgeport', 230);
INSERT INTO `city` VALUES (32, 'Broken Arrow', 'broken arrow', 230);
INSERT INTO `city` VALUES (33, 'Brownsville', 'brownsville', 230);
INSERT INTO `city` VALUES (34, 'Buffalo', 'buffalo', 230);
INSERT INTO `city` VALUES (35, 'Burbank', 'burbank', 230);
INSERT INTO `city` VALUES (36, 'Cambridge', 'cambridge', 230);
INSERT INTO `city` VALUES (37, 'Cape Coral', 'cape coral', 230);
INSERT INTO `city` VALUES (38, 'Carlsbad', 'carlsbad', 230);
INSERT INTO `city` VALUES (39, 'Carmel', 'carmel', 230);
INSERT INTO `city` VALUES (40, 'Carolina', 'carolina', 230);
INSERT INTO `city` VALUES (41, 'Caroline', 'caroline', 230);
INSERT INTO `city` VALUES (42, 'Carrollton', 'carrollton', 230);
INSERT INTO `city` VALUES (43, 'Cary', 'cary', 230);
INSERT INTO `city` VALUES (44, 'Cedar Rapids', 'cedar rapids', 230);
INSERT INTO `city` VALUES (45, 'Centennial', 'centennial', 230);
INSERT INTO `city` VALUES (46, 'Chandler', 'chandler', 230);
INSERT INTO `city` VALUES (47, 'Charleston', 'charleston', 230);
INSERT INTO `city` VALUES (48, 'Charlotte', 'charlotte', 230);
INSERT INTO `city` VALUES (49, 'Chattanooga', 'chattanooga', 230);
INSERT INTO `city` VALUES (50, 'Chesapeake', 'chesapeake', 230);
INSERT INTO `city` VALUES (51, 'Chicago', 'chicago', 230);
INSERT INTO `city` VALUES (52, 'Chico', 'chico', 230);
INSERT INTO `city` VALUES (53, 'Chula Vista', 'chula vista', 230);
INSERT INTO `city` VALUES (54, 'Cincinnati', 'cincinnati', 230);
INSERT INTO `city` VALUES (55, 'Clarkesville', 'clarkesville', 230);
INSERT INTO `city` VALUES (56, 'Clearwater', 'clearwater', 230);
INSERT INTO `city` VALUES (57, 'Cleveland', 'cleveland', 230);
INSERT INTO `city` VALUES (58, 'Clinton', 'clinton', 230);
INSERT INTO `city` VALUES (59, 'Clovis', 'clovis', 230);
INSERT INTO `city` VALUES (60, 'College Station', 'college station', 230);
INSERT INTO `city` VALUES (61, 'Colorado Springs', 'colorado springs', 230);
INSERT INTO `city` VALUES (62, 'Columbia', 'columbia', 230);
INSERT INTO `city` VALUES (63, 'Columbus', 'columbus', 230);
INSERT INTO `city` VALUES (64, 'Concord', 'concord', 230);
INSERT INTO `city` VALUES (65, 'Coral', 'coral', 230);
INSERT INTO `city` VALUES (66, 'Corona', 'corona', 230);
INSERT INTO `city` VALUES (67, 'Corpus Christi', 'corpus christi', 230);
INSERT INTO `city` VALUES (68, 'Costa Mesa', 'costa mesa', 230);
INSERT INTO `city` VALUES (69, 'Dallas', 'dallas', 230);
INSERT INTO `city` VALUES (70, 'Daly City', 'daly city', 230);
INSERT INTO `city` VALUES (71, 'Davenport', 'davenport', 230);
INSERT INTO `city` VALUES (72, 'David', 'david', 230);
INSERT INTO `city` VALUES (73, 'Dayton', 'dayton', 230);
INSERT INTO `city` VALUES (74, 'Denver', 'denver', 230);
INSERT INTO `city` VALUES (75, 'Des Moines', 'des moines', 230);
INSERT INTO `city` VALUES (76, 'Detroit', 'detroit', 230);
INSERT INTO `city` VALUES (77, 'Dewey', 'dewey', 230);
INSERT INTO `city` VALUES (78, 'Durham', 'durham', 230);
INSERT INTO `city` VALUES (79, 'Edinburg', 'edinburg', 230);
INSERT INTO `city` VALUES (80, 'El Cajon', 'el cajon', 230);
INSERT INTO `city` VALUES (81, 'El Monte', 'el monte', 230);
INSERT INTO `city` VALUES (82, 'El Paso', 'el paso', 230);
INSERT INTO `city` VALUES (83, 'Elgin', 'elgin', 230);
INSERT INTO `city` VALUES (84, 'Elizabeth', 'elizabeth', 230);
INSERT INTO `city` VALUES (85, 'Elk Grove', 'elk grove', 230);
INSERT INTO `city` VALUES (86, 'Escondido', 'escondido', 230);
INSERT INTO `city` VALUES (87, 'Eugene', 'eugene', 230);
INSERT INTO `city` VALUES (88, 'Evansville', 'evansville', 230);
INSERT INTO `city` VALUES (89, 'Everett', 'everett', 230);
INSERT INTO `city` VALUES (90, 'Fairfield', 'fairfield', 230);
INSERT INTO `city` VALUES (91, 'Fargo', 'fargo', 230);
INSERT INTO `city` VALUES (92, 'Fayetteville', 'fayetteville', 230);
INSERT INTO `city` VALUES (93, 'Fontana', 'fontana', 230);
INSERT INTO `city` VALUES (94, 'Fort Collins', 'fort collins', 230);
INSERT INTO `city` VALUES (95, 'Fort Lauderdale', 'fort lauderdale', 230);
INSERT INTO `city` VALUES (96, 'Fort Wayne', 'fort wayne', 230);
INSERT INTO `city` VALUES (97, 'Fort Worth', 'fort worth', 230);
INSERT INTO `city` VALUES (98, 'Fremont', 'fremont', 230);
INSERT INTO `city` VALUES (99, 'Fresno', 'fresno', 230);
INSERT INTO `city` VALUES (100, 'Frisco', 'frisco', 230);
INSERT INTO `city` VALUES (101, 'Fullerton', 'fullerton', 230);
INSERT INTO `city` VALUES (102, 'Gainesville', 'gainesville', 230);
INSERT INTO `city` VALUES (103, 'Garden Grove', 'garden grove', 230);
INSERT INTO `city` VALUES (104, 'Garland', 'garland', 230);
INSERT INTO `city` VALUES (105, 'Gilbert', 'gilbert', 230);
INSERT INTO `city` VALUES (106, 'Glendale', 'glendale', 230);
INSERT INTO `city` VALUES (107, 'Grand Prairie', 'grand prairie', 230);
INSERT INTO `city` VALUES (108, 'Grand Rapids', 'grand rapids', 230);
INSERT INTO `city` VALUES (109, 'Greeley', 'greeley', 230);
INSERT INTO `city` VALUES (110, 'Green Bay', 'green bay', 230);
INSERT INTO `city` VALUES (111, 'Greensboro', 'greensboro', 230);
INSERT INTO `city` VALUES (112, 'Gresham', 'gresham', 230);
INSERT INTO `city` VALUES (113, 'Hamburg', 'hamburg', 230);
INSERT INTO `city` VALUES (114, 'Hampton', 'hampton', 230);
INSERT INTO `city` VALUES (115, 'Hayward', 'hayward', 230);
INSERT INTO `city` VALUES (116, 'Henderson', 'henderson', 230);
INSERT INTO `city` VALUES (117, 'Hialeah', 'hialeah', 230);
INSERT INTO `city` VALUES (118, 'High Point', 'high point', 230);
INSERT INTO `city` VALUES (119, 'Hillsboro', 'hillsboro', 230);
INSERT INTO `city` VALUES (120, 'Hollywood', 'hollywood', 230);
INSERT INTO `city` VALUES (121, 'Honolulu', 'honolulu', 230);
INSERT INTO `city` VALUES (122, 'Houston', 'houston', 230);
INSERT INTO `city` VALUES (123, 'Huntington Beach', 'huntington beach', 230);
INSERT INTO `city` VALUES (124, 'Huntsville', 'huntsville', 230);
INSERT INTO `city` VALUES (125, 'Independence', 'independence', 230);
INSERT INTO `city` VALUES (126, 'Indiana', 'indiana', 230);
INSERT INTO `city` VALUES (127, 'Indianapolis', 'indianapolis', 230);
INSERT INTO `city` VALUES (128, 'Inglewood', 'inglewood', 230);
INSERT INTO `city` VALUES (129, 'Irvine', 'irvine', 230);
INSERT INTO `city` VALUES (130, 'Irving', 'irving', 230);
INSERT INTO `city` VALUES (131, 'Jackson', 'jackson', 230);
INSERT INTO `city` VALUES (132, 'Jacksonville', 'jacksonville', 230);
INSERT INTO `city` VALUES (133, 'Jersey', 'jersey', 230);
INSERT INTO `city` VALUES (134, 'Jersey City', 'jersey city', 230);
INSERT INTO `city` VALUES (135, 'Joliet', 'joliet', 230);
INSERT INTO `city` VALUES (136, 'Kansas', 'kansas', 230);
INSERT INTO `city` VALUES (137, 'Kansas City', 'kansas city', 230);
INSERT INTO `city` VALUES (138, 'Kent', 'kent', 230);
INSERT INTO `city` VALUES (139, 'Killeen', 'killeen', 230);
INSERT INTO `city` VALUES (140, 'Knoxville', 'knoxville', 230);
INSERT INTO `city` VALUES (141, 'Kyle', 'kyle', 230);
INSERT INTO `city` VALUES (142, 'Lafayette', 'lafayette', 230);
INSERT INTO `city` VALUES (143, 'Lakeland', 'lakeland', 230);
INSERT INTO `city` VALUES (144, 'Lakewood', 'lakewood', 230);
INSERT INTO `city` VALUES (145, 'Lancaster', 'lancaster', 230);
INSERT INTO `city` VALUES (146, 'Lancing', 'lancing', 230);
INSERT INTO `city` VALUES (147, 'Lansing', 'lansing', 230);
INSERT INTO `city` VALUES (148, 'Laredo', 'laredo', 230);
INSERT INTO `city` VALUES (149, 'Las Cruces', 'las cruces', 230);
INSERT INTO `city` VALUES (150, 'Las Vegas', 'las vegas', 230);
INSERT INTO `city` VALUES (151, 'League City', 'league city', 230);
INSERT INTO `city` VALUES (152, 'Lewisville', 'lewisville', 230);
INSERT INTO `city` VALUES (153, 'Lexington', 'lexington', 230);
INSERT INTO `city` VALUES (154, 'Lincoln', 'lincoln', 230);
INSERT INTO `city` VALUES (155, 'Little Rock', 'little rock', 230);
INSERT INTO `city` VALUES (156, 'Long Beach', 'long beach', 230);
INSERT INTO `city` VALUES (157, 'Los Angeles', 'los angeles', 230);
INSERT INTO `city` VALUES (158, 'Louisville', 'louisville', 230);
INSERT INTO `city` VALUES (159, 'Lowell', 'lowell', 230);
INSERT INTO `city` VALUES (160, 'Lubbock', 'lubbock', 230);
INSERT INTO `city` VALUES (161, 'Macon', 'macon', 230);
INSERT INTO `city` VALUES (162, 'Madison', 'madison', 230);
INSERT INTO `city` VALUES (163, 'Manchester', 'manchester', 230);
INSERT INTO `city` VALUES (164, 'Mcallen', 'mcallen', 230);
INSERT INTO `city` VALUES (165, 'Mckinney', 'mckinney', 230);
INSERT INTO `city` VALUES (166, 'Memphis', 'memphis', 230);
INSERT INTO `city` VALUES (167, 'Meridian', 'meridian', 230);
INSERT INTO `city` VALUES (168, 'Mesa', 'mesa', 230);
INSERT INTO `city` VALUES (169, 'Mesquite', 'mesquite', 230);
INSERT INTO `city` VALUES (170, 'Miami', 'miami', 230);
INSERT INTO `city` VALUES (171, 'Miami Beach', 'miami beach', 230);
INSERT INTO `city` VALUES (172, 'Michigan', 'michigan', 230);
INSERT INTO `city` VALUES (173, 'Midland', 'midland', 230);
INSERT INTO `city` VALUES (174, 'Milwaukee', 'milwaukee', 230);
INSERT INTO `city` VALUES (175, 'Miramar Beach', 'miramar beach', 230);
INSERT INTO `city` VALUES (176, 'Mobile', 'mobile', 230);
INSERT INTO `city` VALUES (177, 'Modesto', 'modesto', 230);
INSERT INTO `city` VALUES (178, 'Montara', 'montara', 230);
INSERT INTO `city` VALUES (179, 'Montgomery', 'montgomery', 230);
INSERT INTO `city` VALUES (180, 'Moreno Valley', 'moreno valley', 230);
INSERT INTO `city` VALUES (181, 'Morris', 'morris', 230);
INSERT INTO `city` VALUES (182, 'Murfreesboro', 'murfreesboro', 230);
INSERT INTO `city` VALUES (183, 'Murphy', 'murphy', 230);
INSERT INTO `city` VALUES (184, 'Murrieta', 'murrieta', 230);
INSERT INTO `city` VALUES (185, 'Naperville', 'naperville', 230);
INSERT INTO `city` VALUES (186, 'New Haven', 'new haven', 230);
INSERT INTO `city` VALUES (187, 'New Orleans', 'new orleans', 230);
INSERT INTO `city` VALUES (188, 'New Oxford', 'new oxford', 230);
INSERT INTO `city` VALUES (189, 'New York', 'new york', 230);
INSERT INTO `city` VALUES (190, 'Newark', 'newark', 230);
INSERT INTO `city` VALUES (191, 'Newport News', 'newport news', 230);
INSERT INTO `city` VALUES (192, 'Norfolk', 'norfolk', 230);
INSERT INTO `city` VALUES (193, 'Norman', 'norman', 230);
INSERT INTO `city` VALUES (194, 'North Charleston', 'north charleston', 230);
INSERT INTO `city` VALUES (195, 'North Las Vegas', 'north las vegas', 230);
INSERT INTO `city` VALUES (196, 'Norwalk', 'norwalk', 230);
INSERT INTO `city` VALUES (197, 'Olathe', 'olathe', 230);
INSERT INTO `city` VALUES (198, 'Omaha', 'omaha', 230);
INSERT INTO `city` VALUES (199, 'Ontario', 'ontario', 230);
INSERT INTO `city` VALUES (200, 'Orange', 'orange', 230);
INSERT INTO `city` VALUES (201, 'Orlando', 'orlando', 230);
INSERT INTO `city` VALUES (202, 'Overland Park', 'overland park', 230);
INSERT INTO `city` VALUES (203, 'Oxnard', 'oxnard', 230);
INSERT INTO `city` VALUES (204, 'Oakland', 'oakland', 230);
INSERT INTO `city` VALUES (205, 'Oceanside', 'oceanside', 230);
INSERT INTO `city` VALUES (206, 'Odessa', 'odessa', 230);
INSERT INTO `city` VALUES (207, 'Oklahoma City', 'oklahoma city', 230);
INSERT INTO `city` VALUES (208, 'Palm Bay', 'palm bay', 230);
INSERT INTO `city` VALUES (209, 'Palm Beach', 'palm beach', 230);
INSERT INTO `city` VALUES (210, 'Palmdale', 'palmdale', 230);
INSERT INTO `city` VALUES (211, 'Pasadena', 'pasadena', 230);
INSERT INTO `city` VALUES (212, 'Paterson', 'paterson', 230);
INSERT INTO `city` VALUES (213, 'Pearland', 'pearland', 230);
INSERT INTO `city` VALUES (214, 'Pembroke Pines', 'pembroke pines', 230);
INSERT INTO `city` VALUES (215, 'Peoria', 'peoria', 230);
INSERT INTO `city` VALUES (216, 'Philadelphia', 'philadelphia', 230);
INSERT INTO `city` VALUES (217, 'Phoenix', 'phoenix', 230);
INSERT INTO `city` VALUES (218, 'Pitsburg', 'pitsburg', 230);
INSERT INTO `city` VALUES (219, 'Plano', 'plano', 230);
INSERT INTO `city` VALUES (220, 'Pomona', 'pomona', 230);
INSERT INTO `city` VALUES (221, 'Pompano Beach', 'pompano beach', 230);
INSERT INTO `city` VALUES (222, 'Port Saint Lucie', 'port saint lucie', 230);
INSERT INTO `city` VALUES (223, 'Portland', 'portland', 230);
INSERT INTO `city` VALUES (224, 'Providence', 'providence', 230);
INSERT INTO `city` VALUES (225, 'Provo', 'provo', 230);
INSERT INTO `city` VALUES (226, 'Pueblo', 'pueblo', 230);
INSERT INTO `city` VALUES (227, 'Raleigh', 'raleigh', 230);
INSERT INTO `city` VALUES (228, 'Rancho Cucamonga', 'rancho cucamonga', 230);
INSERT INTO `city` VALUES (229, 'Reno', 'reno', 230);
INSERT INTO `city` VALUES (230, 'Renton', 'renton', 230);
INSERT INTO `city` VALUES (231, 'Rialto', 'rialto', 230);
INSERT INTO `city` VALUES (232, 'Richardson', 'richardson', 230);
INSERT INTO `city` VALUES (233, 'Richmond', 'richmond', 230);
INSERT INTO `city` VALUES (234, 'Riverside', 'riverside', 230);
INSERT INTO `city` VALUES (235, 'Rochester', 'rochester', 230);
INSERT INTO `city` VALUES (236, 'Rockford', 'rockford', 230);
INSERT INTO `city` VALUES (237, 'Roseville', 'roseville', 230);
INSERT INTO `city` VALUES (238, 'Round Rock', 'round rock', 230);
INSERT INTO `city` VALUES (239, 'Sacramento', 'sacramento', 230);
INSERT INTO `city` VALUES (240, 'Saint Paul', 'saint paul', 230);
INSERT INTO `city` VALUES (241, 'Salem', 'salem', 230);
INSERT INTO `city` VALUES (242, 'Salinas', 'salinas', 230);
INSERT INTO `city` VALUES (243, 'Salt Lake City', 'salt lake city', 230);
INSERT INTO `city` VALUES (244, 'San Andreas', 'san andreas', 230);
INSERT INTO `city` VALUES (245, 'San Angelo', 'san angelo', 230);
INSERT INTO `city` VALUES (246, 'San Antonio', 'san antonio', 230);
INSERT INTO `city` VALUES (247, 'San Bernardino', 'san bernardino', 230);
INSERT INTO `city` VALUES (248, 'San Diego', 'san diego', 230);
INSERT INTO `city` VALUES (249, 'San Francisco', 'san francisco', 230);
INSERT INTO `city` VALUES (250, 'San Jose', 'san jose', 230);
INSERT INTO `city` VALUES (251, 'San Mateo', 'san mateo', 230);
INSERT INTO `city` VALUES (252, 'Sandy Springs', 'sandy springs', 230);
INSERT INTO `city` VALUES (253, 'Santa Ana', 'santa ana', 230);
INSERT INTO `city` VALUES (254, 'Santa Clara', 'santa clara', 230);
INSERT INTO `city` VALUES (255, 'Santa Clarita', 'santa clarita', 230);
INSERT INTO `city` VALUES (256, 'Santa Maria', 'santa maria', 230);
INSERT INTO `city` VALUES (257, 'Santa Rosa', 'santa rosa', 230);
INSERT INTO `city` VALUES (258, 'Savannah', 'savannah', 230);
INSERT INTO `city` VALUES (259, 'Scottsdale', 'scottsdale', 230);
INSERT INTO `city` VALUES (260, 'Seattle', 'seattle', 230);
INSERT INTO `city` VALUES (261, 'Shreveport', 'shreveport', 230);
INSERT INTO `city` VALUES (262, 'Simi Valley', 'simi valley', 230);
INSERT INTO `city` VALUES (263, 'Sioux Falls', 'sioux falls', 230);
INSERT INTO `city` VALUES (264, 'South Bend', 'south bend', 230);
INSERT INTO `city` VALUES (265, 'Sparks', 'sparks', 230);
INSERT INTO `city` VALUES (266, 'Spokane', 'spokane', 230);
INSERT INTO `city` VALUES (267, 'Springfield', 'springfield', 230);
INSERT INTO `city` VALUES (268, 'Stanaford', 'stanaford', 230);
INSERT INTO `city` VALUES (269, 'Sterling Heights', 'sterling heights', 230);
INSERT INTO `city` VALUES (270, 'Stockton', 'stockton', 230);
INSERT INTO `city` VALUES (271, 'Sugar Land', 'sugar land', 230);
INSERT INTO `city` VALUES (272, 'Sunnyvale', 'sunnyvale', 230);
INSERT INTO `city` VALUES (273, 'Surprise', 'surprise', 230);
INSERT INTO `city` VALUES (274, 'Syracuse', 'syracuse', 230);
INSERT INTO `city` VALUES (275, 'Tacoma', 'tacoma', 230);
INSERT INTO `city` VALUES (276, 'Tallahassee', 'tallahassee', 230);
INSERT INTO `city` VALUES (277, 'Tampa', 'tampa', 230);
INSERT INTO `city` VALUES (278, 'Temecula', 'temecula', 230);
INSERT INTO `city` VALUES (279, 'Temple', 'temple', 230);
INSERT INTO `city` VALUES (280, 'Thornton', 'thornton', 230);
INSERT INTO `city` VALUES (281, 'Thousand Oaks', 'thousand oaks', 230);
INSERT INTO `city` VALUES (282, 'Toledo', 'toledo', 230);
INSERT INTO `city` VALUES (283, 'Topeka', 'topeka', 230);
INSERT INTO `city` VALUES (284, 'Toronto', 'toronto', 230);
INSERT INTO `city` VALUES (285, 'Torrance', 'torrance', 230);
INSERT INTO `city` VALUES (286, 'Tucson', 'tucson', 230);
INSERT INTO `city` VALUES (287, 'Tulsa', 'tulsa', 230);
INSERT INTO `city` VALUES (288, 'Tuscaloosa', 'tuscaloosa', 230);
INSERT INTO `city` VALUES (289, 'Tyler', 'tyler', 230);
INSERT INTO `city` VALUES (290, 'Vacaville', 'vacaville', 230);
INSERT INTO `city` VALUES (291, 'Vallejo', 'vallejo', 230);
INSERT INTO `city` VALUES (292, 'Vancouver', 'vancouver', 230);
INSERT INTO `city` VALUES (293, 'Ventura', 'ventura', 230);
INSERT INTO `city` VALUES (294, 'Victorville', 'victorville', 230);
INSERT INTO `city` VALUES (295, 'Virginia', 'virginia', 230);
INSERT INTO `city` VALUES (296, 'Virginia Beach', 'virginia beach', 230);
INSERT INTO `city` VALUES (297, 'Visalia', 'visalia', 230);
INSERT INTO `city` VALUES (298, 'Vista', 'vista', 230);
INSERT INTO `city` VALUES (299, 'Waco', 'waco', 230);
INSERT INTO `city` VALUES (300, 'Warren', 'warren', 230);
INSERT INTO `city` VALUES (301, 'Washington', 'washington', 230);
INSERT INTO `city` VALUES (302, 'Waterbury', 'waterbury', 230);
INSERT INTO `city` VALUES (303, 'West Covina', 'west covina', 230);
INSERT INTO `city` VALUES (304, 'West Jordan', 'west jordan', 230);
INSERT INTO `city` VALUES (305, 'West Palm Beach', 'west palm beach', 230);
INSERT INTO `city` VALUES (306, 'West Valley', 'west valley', 230);
INSERT INTO `city` VALUES (307, 'Westminster', 'westminster', 230);
INSERT INTO `city` VALUES (308, 'Wichita', 'wichita', 230);
INSERT INTO `city` VALUES (309, 'Wichita Falls', 'wichita falls', 230);
INSERT INTO `city` VALUES (310, 'Wilmington', 'wilmington', 230);
INSERT INTO `city` VALUES (311, 'Winston Salem', 'winston salem', 230);
INSERT INTO `city` VALUES (312, 'Woodbridge', 'woodbridge', 230);
INSERT INTO `city` VALUES (313, 'Worcester', 'worcester', 230);
INSERT INTO `city` VALUES (314, 'York', 'york', 230);

-- ----------------------------
-- Table structure for claimed_listing
-- ----------------------------
DROP TABLE IF EXISTS `claimed_listing`;
CREATE TABLE `claimed_listing`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `listing_id` int NULL DEFAULT NULL,
  `user_id` int NULL DEFAULT NULL,
  `full_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `additional_information` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of claimed_listing
-- ----------------------------
INSERT INTO `claimed_listing` VALUES (1, 2, 1, 'nmae', 'phne', 'addditional', 1);
INSERT INTO `claimed_listing` VALUES (2, 1, 1, 'claim name', 'claim phone', 'claim additional', 0);
INSERT INTO `claimed_listing` VALUES (3, 4, 3, 'Volo', '23232323', 'addddd', 1);

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `listing_id` int NULL DEFAULT NULL,
  `reply_to` int NULL DEFAULT NULL,
  `comment` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of comment
-- ----------------------------

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `code` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `dial_code` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `currency_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `currency_symbol` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `currency_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `code`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 242 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO `country` VALUES (1, 'Afghanistan', 'AF', '+93', 'Afghan afghani', '؋', 'AFN');
INSERT INTO `country` VALUES (2, 'Aland Islands', 'AX', '+358', '', '', '');
INSERT INTO `country` VALUES (3, 'Albania', 'AL', '+355', 'Albanian lek', 'L', 'ALL');
INSERT INTO `country` VALUES (4, 'Algeria', 'DZ', '+213', 'Algerian dinar', 'د.ج', 'DZD');
INSERT INTO `country` VALUES (5, 'AmericanSamoa', 'AS', '+1684', '', '', '');
INSERT INTO `country` VALUES (6, 'Andorra', 'AD', '+376', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (7, 'Angola', 'AO', '+244', 'Angolan kwanza', 'Kz', 'AOA');
INSERT INTO `country` VALUES (8, 'Anguilla', 'AI', '+1264', 'East Caribbean dolla', '$', 'XCD');
INSERT INTO `country` VALUES (9, 'Antarctica', 'AQ', '+672', '', '', '');
INSERT INTO `country` VALUES (10, 'Antigua and Barbuda', 'AG', '+1268', 'East Caribbean dolla', '$', 'XCD');
INSERT INTO `country` VALUES (11, 'Argentina', 'AR', '+54', 'Argentine peso', '$', 'ARS');
INSERT INTO `country` VALUES (12, 'Armenia', 'AM', '+374', 'Armenian dram', '', 'AMD');
INSERT INTO `country` VALUES (13, 'Aruba', 'AW', '+297', 'Aruban florin', 'ƒ', 'AWG');
INSERT INTO `country` VALUES (14, 'Australia', 'AU', '+61', 'Australian dollar', '$', 'AUD');
INSERT INTO `country` VALUES (15, 'Austria', 'AT', '+43', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (16, 'Azerbaijan', 'AZ', '+994', 'Azerbaijani manat', '', 'AZN');
INSERT INTO `country` VALUES (17, 'Bahamas', 'BS', '+1242', '', '', '');
INSERT INTO `country` VALUES (18, 'Bahrain', 'BH', '+973', 'Bahraini dinar', '.د.ب', 'BHD');
INSERT INTO `country` VALUES (19, 'Bangladesh', 'BD', '+880', 'Bangladeshi taka', '৳', 'BDT');
INSERT INTO `country` VALUES (20, 'Barbados', 'BB', '+1246', 'Barbadian dollar', '$', 'BBD');
INSERT INTO `country` VALUES (21, 'Belarus', 'BY', '+375', 'Belarusian ruble', 'Br', 'BYR');
INSERT INTO `country` VALUES (22, 'Belgium', 'BE', '+32', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (23, 'Belize', 'BZ', '+501', 'Belize dollar', '$', 'BZD');
INSERT INTO `country` VALUES (24, 'Benin', 'BJ', '+229', 'West African CFA fra', 'Fr', 'XOF');
INSERT INTO `country` VALUES (25, 'Bermuda', 'BM', '+1441', 'Bermudian dollar', '$', 'BMD');
INSERT INTO `country` VALUES (26, 'Bhutan', 'BT', '+975', 'Bhutanese ngultrum', 'Nu.', 'BTN');
INSERT INTO `country` VALUES (27, 'Bolivia, Plurination', 'BO', '+591', '', '', '');
INSERT INTO `country` VALUES (28, 'Bosnia and Herzegovi', 'BA', '+387', '', '', '');
INSERT INTO `country` VALUES (29, 'Botswana', 'BW', '+267', 'Botswana pula', 'P', 'BWP');
INSERT INTO `country` VALUES (30, 'Brazil', 'BR', '+55', 'Brazilian real', 'R$', 'BRL');
INSERT INTO `country` VALUES (31, 'British Indian Ocean', 'IO', '+246', '', '', '');
INSERT INTO `country` VALUES (32, 'Brunei Darussalam', 'BN', '+673', '', '', '');
INSERT INTO `country` VALUES (33, 'Bulgaria', 'BG', '+359', 'Bulgarian lev', 'лв', 'BGN');
INSERT INTO `country` VALUES (34, 'Burkina Faso', 'BF', '+226', 'West African CFA fra', 'Fr', 'XOF');
INSERT INTO `country` VALUES (35, 'Burundi', 'BI', '+257', 'Burundian franc', 'Fr', 'BIF');
INSERT INTO `country` VALUES (36, 'Cambodia', 'KH', '+855', 'Cambodian riel', '៛', 'KHR');
INSERT INTO `country` VALUES (37, 'Cameroon', 'CM', '+237', 'Central African CFA ', 'Fr', 'XAF');
INSERT INTO `country` VALUES (38, 'Canada', 'CA', '+1', 'Canadian dollar', '$', 'CAD');
INSERT INTO `country` VALUES (39, 'Cape Verde', 'CV', '+238', 'Cape Verdean escudo', 'Esc or $', 'CVE');
INSERT INTO `country` VALUES (40, 'Cayman Islands', 'KY', '+ 345', 'Cayman Islands dolla', '$', 'KYD');
INSERT INTO `country` VALUES (41, 'Central African Repu', 'CF', '+236', '', '', '');
INSERT INTO `country` VALUES (42, 'Chad', 'TD', '+235', 'Central African CFA ', 'Fr', 'XAF');
INSERT INTO `country` VALUES (43, 'Chile', 'CL', '+56', 'Chilean peso', '$', 'CLP');
INSERT INTO `country` VALUES (44, 'China', 'CN', '+86', 'Chinese yuan', '¥ or 元', 'CNY');
INSERT INTO `country` VALUES (45, 'Christmas Island', 'CX', '+61', '', '', '');
INSERT INTO `country` VALUES (46, 'Cocos (Keeling) Isla', 'CC', '+61', '', '', '');
INSERT INTO `country` VALUES (47, 'Colombia', 'CO', '+57', 'Colombian peso', '$', 'COP');
INSERT INTO `country` VALUES (48, 'Comoros', 'KM', '+269', 'Comorian franc', 'Fr', 'KMF');
INSERT INTO `country` VALUES (49, 'Congo', 'CG', '+242', '', '', '');
INSERT INTO `country` VALUES (50, 'Congo, The Democrati', 'CD', '+243', '', '', '');
INSERT INTO `country` VALUES (51, 'Cook Islands', 'CK', '+682', 'New Zealand dollar', '$', 'NZD');
INSERT INTO `country` VALUES (52, 'Costa Rica', 'CR', '+506', 'Costa Rican colón', '₡', 'CRC');
INSERT INTO `country` VALUES (53, 'Cote d\'Ivoire', 'CI', '+225', 'West African CFA fra', 'Fr', 'XOF');
INSERT INTO `country` VALUES (54, 'Croatia', 'HR', '+385', 'Croatian kuna', 'kn', 'HRK');
INSERT INTO `country` VALUES (55, 'Cuba', 'CU', '+53', 'Cuban convertible pe', '$', 'CUC');
INSERT INTO `country` VALUES (56, 'Cyprus', 'CY', '+357', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (57, 'Czech Republic', 'CZ', '+420', 'Czech koruna', 'Kč', 'CZK');
INSERT INTO `country` VALUES (58, 'Denmark', 'DK', '+45', 'Danish krone', 'kr', 'DKK');
INSERT INTO `country` VALUES (59, 'Djibouti', 'DJ', '+253', 'Djiboutian franc', 'Fr', 'DJF');
INSERT INTO `country` VALUES (60, 'Dominica', 'DM', '+1767', 'East Caribbean dolla', '$', 'XCD');
INSERT INTO `country` VALUES (61, 'Dominican Republic', 'DO', '+1849', 'Dominican peso', '$', 'DOP');
INSERT INTO `country` VALUES (62, 'Ecuador', 'EC', '+593', 'United States dollar', '$', 'USD');
INSERT INTO `country` VALUES (63, 'Egypt', 'EG', '+20', 'Egyptian pound', '£ or ج.م', 'EGP');
INSERT INTO `country` VALUES (64, 'El Salvador', 'SV', '+503', 'United States dollar', '$', 'USD');
INSERT INTO `country` VALUES (65, 'Equatorial Guinea', 'GQ', '+240', 'Central African CFA ', 'Fr', 'XAF');
INSERT INTO `country` VALUES (66, 'Eritrea', 'ER', '+291', 'Eritrean nakfa', 'Nfk', 'ERN');
INSERT INTO `country` VALUES (67, 'Estonia', 'EE', '+372', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (68, 'Ethiopia', 'ET', '+251', 'Ethiopian birr', 'Br', 'ETB');
INSERT INTO `country` VALUES (69, 'Falkland Islands (Ma', 'FK', '+500', '', '', '');
INSERT INTO `country` VALUES (70, 'Faroe Islands', 'FO', '+298', 'Danish krone', 'kr', 'DKK');
INSERT INTO `country` VALUES (71, 'Fiji', 'FJ', '+679', 'Fijian dollar', '$', 'FJD');
INSERT INTO `country` VALUES (72, 'Finland', 'FI', '+358', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (73, 'France', 'FR', '+33', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (74, 'French Guiana', 'GF', '+594', '', '', '');
INSERT INTO `country` VALUES (75, 'French Polynesia', 'PF', '+689', 'CFP franc', 'Fr', 'XPF');
INSERT INTO `country` VALUES (76, 'Gabon', 'GA', '+241', 'Central African CFA ', 'Fr', 'XAF');
INSERT INTO `country` VALUES (77, 'Gambia', 'GM', '+220', '', '', '');
INSERT INTO `country` VALUES (78, 'Georgia', 'GE', '+995', 'Georgian lari', 'ლ', 'GEL');
INSERT INTO `country` VALUES (79, 'Germany', 'DE', '+49', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (80, 'Ghana', 'GH', '+233', 'Ghana cedi', '₵', 'GHS');
INSERT INTO `country` VALUES (81, 'Gibraltar', 'GI', '+350', 'Gibraltar pound', '£', 'GIP');
INSERT INTO `country` VALUES (82, 'Greece', 'GR', '+30', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (83, 'Greenland', 'GL', '+299', '', '', '');
INSERT INTO `country` VALUES (84, 'Grenada', 'GD', '+1473', 'East Caribbean dolla', '$', 'XCD');
INSERT INTO `country` VALUES (85, 'Guadeloupe', 'GP', '+590', '', '', '');
INSERT INTO `country` VALUES (86, 'Guam', 'GU', '+1671', '', '', '');
INSERT INTO `country` VALUES (87, 'Guatemala', 'GT', '+502', 'Guatemalan quetzal', 'Q', 'GTQ');
INSERT INTO `country` VALUES (88, 'Guernsey', 'GG', '+44', 'British pound', '£', 'GBP');
INSERT INTO `country` VALUES (89, 'Guinea', 'GN', '+224', 'Guinean franc', 'Fr', 'GNF');
INSERT INTO `country` VALUES (90, 'Guinea-Bissau', 'GW', '+245', 'West African CFA fra', 'Fr', 'XOF');
INSERT INTO `country` VALUES (91, 'Guyana', 'GY', '+595', 'Guyanese dollar', '$', 'GYD');
INSERT INTO `country` VALUES (92, 'Haiti', 'HT', '+509', 'Haitian gourde', 'G', 'HTG');
INSERT INTO `country` VALUES (93, 'Holy See (Vatican Ci', 'VA', '+379', '', '', '');
INSERT INTO `country` VALUES (94, 'Honduras', 'HN', '+504', 'Honduran lempira', 'L', 'HNL');
INSERT INTO `country` VALUES (95, 'Hong Kong', 'HK', '+852', 'Hong Kong dollar', '$', 'HKD');
INSERT INTO `country` VALUES (96, 'Hungary', 'HU', '+36', 'Hungarian forint', 'Ft', 'HUF');
INSERT INTO `country` VALUES (97, 'Iceland', 'IS', '+354', 'Icelandic króna', 'kr', 'ISK');
INSERT INTO `country` VALUES (98, 'India', 'IN', '+91', 'Indian rupee', '₹', 'INR');
INSERT INTO `country` VALUES (99, 'Indonesia', 'ID', '+62', 'Indonesian rupiah', 'Rp', 'IDR');
INSERT INTO `country` VALUES (100, 'Iran, Islamic Republ', 'IR', '+98', '', '', '');
INSERT INTO `country` VALUES (101, 'Iraq', 'IQ', '+964', 'Iraqi dinar', 'ع.د', 'IQD');
INSERT INTO `country` VALUES (102, 'Ireland', 'IE', '+353', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (103, 'Isle of Man', 'IM', '+44', 'British pound', '£', 'GBP');
INSERT INTO `country` VALUES (104, 'Israel', 'IL', '+972', 'Israeli new shekel', '₪', 'ILS');
INSERT INTO `country` VALUES (105, 'Italy', 'IT', '+39', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (106, 'Jamaica', 'JM', '+1876', 'Jamaican dollar', '$', 'JMD');
INSERT INTO `country` VALUES (107, 'Japan', 'JP', '+81', 'Japanese yen', '¥', 'JPY');
INSERT INTO `country` VALUES (108, 'Jersey', 'JE', '+44', 'British pound', '£', 'GBP');
INSERT INTO `country` VALUES (109, 'Jordan', 'JO', '+962', 'Jordanian dinar', 'د.ا', 'JOD');
INSERT INTO `country` VALUES (110, 'Kazakhstan', 'KZ', '+7 7', 'Kazakhstani tenge', '', 'KZT');
INSERT INTO `country` VALUES (111, 'Kenya', 'KE', '+254', 'Kenyan shilling', 'Sh', 'KES');
INSERT INTO `country` VALUES (112, 'Kiribati', 'KI', '+686', 'Australian dollar', '$', 'AUD');
INSERT INTO `country` VALUES (113, 'Korea, Democratic Pe', 'KP', '+850', '', '', '');
INSERT INTO `country` VALUES (114, 'Korea, Republic of S', 'KR', '+82', '', '', '');
INSERT INTO `country` VALUES (115, 'Kuwait', 'KW', '+965', 'Kuwaiti dinar', 'د.ك', 'KWD');
INSERT INTO `country` VALUES (116, 'Kyrgyzstan', 'KG', '+996', 'Kyrgyzstani som', 'лв', 'KGS');
INSERT INTO `country` VALUES (117, 'Laos', 'LA', '+856', 'Lao kip', '₭', 'LAK');
INSERT INTO `country` VALUES (118, 'Latvia', 'LV', '+371', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (119, 'Lebanon', 'LB', '+961', 'Lebanese pound', 'ل.ل', 'LBP');
INSERT INTO `country` VALUES (120, 'Lesotho', 'LS', '+266', 'Lesotho loti', 'L', 'LSL');
INSERT INTO `country` VALUES (121, 'Liberia', 'LR', '+231', 'Liberian dollar', '$', 'LRD');
INSERT INTO `country` VALUES (122, 'Libyan Arab Jamahiri', 'LY', '+218', '', '', '');
INSERT INTO `country` VALUES (123, 'Liechtenstein', 'LI', '+423', 'Swiss franc', 'Fr', 'CHF');
INSERT INTO `country` VALUES (124, 'Lithuania', 'LT', '+370', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (125, 'Luxembourg', 'LU', '+352', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (126, 'Macao', 'MO', '+853', '', '', '');
INSERT INTO `country` VALUES (127, 'Macedonia', 'MK', '+389', '', '', '');
INSERT INTO `country` VALUES (128, 'Madagascar', 'MG', '+261', 'Malagasy ariary', 'Ar', 'MGA');
INSERT INTO `country` VALUES (129, 'Malawi', 'MW', '+265', 'Malawian kwacha', 'MK', 'MWK');
INSERT INTO `country` VALUES (130, 'Malaysia', 'MY', '+60', 'Malaysian ringgit', 'RM', 'MYR');
INSERT INTO `country` VALUES (131, 'Maldives', 'MV', '+960', 'Maldivian rufiyaa', '.ރ', 'MVR');
INSERT INTO `country` VALUES (132, 'Mali', 'ML', '+223', 'West African CFA fra', 'Fr', 'XOF');
INSERT INTO `country` VALUES (133, 'Malta', 'MT', '+356', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (134, 'Marshall Islands', 'MH', '+692', 'United States dollar', '$', 'USD');
INSERT INTO `country` VALUES (135, 'Martinique', 'MQ', '+596', '', '', '');
INSERT INTO `country` VALUES (136, 'Mauritania', 'MR', '+222', 'Mauritanian ouguiya', 'UM', 'MRO');
INSERT INTO `country` VALUES (137, 'Mauritius', 'MU', '+230', 'Mauritian rupee', '₨', 'MUR');
INSERT INTO `country` VALUES (138, 'Mayotte', 'YT', '+262', '', '', '');
INSERT INTO `country` VALUES (139, 'Mexico', 'MX', '+52', 'Mexican peso', '$', 'MXN');
INSERT INTO `country` VALUES (140, 'Micronesia, Federate', 'FM', '+691', '', '', '');
INSERT INTO `country` VALUES (141, 'Moldova', 'MD', '+373', 'Moldovan leu', 'L', 'MDL');
INSERT INTO `country` VALUES (142, 'Monaco', 'MC', '+377', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (143, 'Mongolia', 'MN', '+976', 'Mongolian tögrög', '₮', 'MNT');
INSERT INTO `country` VALUES (144, 'Montenegro', 'ME', '+382', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (145, 'Montserrat', 'MS', '+1664', 'East Caribbean dolla', '$', 'XCD');
INSERT INTO `country` VALUES (146, 'Morocco', 'MA', '+212', 'Moroccan dirham', 'د.م.', 'MAD');
INSERT INTO `country` VALUES (147, 'Mozambique', 'MZ', '+258', 'Mozambican metical', 'MT', 'MZN');
INSERT INTO `country` VALUES (148, 'Myanmar', 'MM', '+95', 'Burmese kyat', 'Ks', 'MMK');
INSERT INTO `country` VALUES (149, 'Namibia', 'NA', '+264', 'Namibian dollar', '$', 'NAD');
INSERT INTO `country` VALUES (150, 'Nauru', 'NR', '+674', 'Australian dollar', '$', 'AUD');
INSERT INTO `country` VALUES (151, 'Nepal', 'NP', '+977', 'Nepalese rupee', '₨', 'NPR');
INSERT INTO `country` VALUES (152, 'Netherlands', 'NL', '+31', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (153, 'Netherlands Antilles', 'AN', '+599', '', '', '');
INSERT INTO `country` VALUES (154, 'New Caledonia', 'NC', '+687', 'CFP franc', 'Fr', 'XPF');
INSERT INTO `country` VALUES (155, 'New Zealand', 'NZ', '+64', 'New Zealand dollar', '$', 'NZD');
INSERT INTO `country` VALUES (156, 'Nicaragua', 'NI', '+505', 'Nicaraguan córdoba', 'C$', 'NIO');
INSERT INTO `country` VALUES (157, 'Niger', 'NE', '+227', 'West African CFA fra', 'Fr', 'XOF');
INSERT INTO `country` VALUES (158, 'Nigeria', 'NG', '+234', 'Nigerian naira', '₦', 'NGN');
INSERT INTO `country` VALUES (159, 'Niue', 'NU', '+683', 'New Zealand dollar', '$', 'NZD');
INSERT INTO `country` VALUES (160, 'Norfolk Island', 'NF', '+672', '', '', '');
INSERT INTO `country` VALUES (161, 'Northern Mariana Isl', 'MP', '+1670', '', '', '');
INSERT INTO `country` VALUES (162, 'Norway', 'NO', '+47', 'Norwegian krone', 'kr', 'NOK');
INSERT INTO `country` VALUES (163, 'Oman', 'OM', '+968', 'Omani rial', 'ر.ع.', 'OMR');
INSERT INTO `country` VALUES (164, 'Pakistan', 'PK', '+92', 'Pakistani rupee', '₨', 'PKR');
INSERT INTO `country` VALUES (165, 'Palau', 'PW', '+680', 'Palauan dollar', '$', '');
INSERT INTO `country` VALUES (166, 'Palestinian Territor', 'PS', '+970', '', '', '');
INSERT INTO `country` VALUES (167, 'Panama', 'PA', '+507', 'Panamanian balboa', 'B/.', 'PAB');
INSERT INTO `country` VALUES (168, 'Papua New Guinea', 'PG', '+675', 'Papua New Guinean ki', 'K', 'PGK');
INSERT INTO `country` VALUES (169, 'Paraguay', 'PY', '+595', 'Paraguayan guaraní', '₲', 'PYG');
INSERT INTO `country` VALUES (170, 'Peru', 'PE', '+51', 'Peruvian nuevo sol', 'S/.', 'PEN');
INSERT INTO `country` VALUES (171, 'Philippines', 'PH', '+63', 'Philippine peso', '₱', 'PHP');
INSERT INTO `country` VALUES (172, 'Pitcairn', 'PN', '+872', '', '', '');
INSERT INTO `country` VALUES (173, 'Poland', 'PL', '+48', 'Polish z?oty', 'zł', 'PLN');
INSERT INTO `country` VALUES (174, 'Portugal', 'PT', '+351', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (175, 'Puerto Rico', 'PR', '+1939', '', '', '');
INSERT INTO `country` VALUES (176, 'Qatar', 'QA', '+974', 'Qatari riyal', 'ر.ق', 'QAR');
INSERT INTO `country` VALUES (177, 'Romania', 'RO', '+40', 'Romanian leu', 'lei', 'RON');
INSERT INTO `country` VALUES (178, 'Russia', 'RU', '+7', 'Russian ruble', '', 'RUB');
INSERT INTO `country` VALUES (179, 'Rwanda', 'RW', '+250', 'Rwandan franc', 'Fr', 'RWF');
INSERT INTO `country` VALUES (180, 'Reunion', 'RE', '+262', '', '', '');
INSERT INTO `country` VALUES (181, 'Saint Barthelemy', 'BL', '+590', '', '', '');
INSERT INTO `country` VALUES (182, 'Saint Helena, Ascens', 'SH', '+290', '', '', '');
INSERT INTO `country` VALUES (183, 'Saint Kitts and Nevi', 'KN', '+1869', '', '', '');
INSERT INTO `country` VALUES (184, 'Saint Lucia', 'LC', '+1758', 'East Caribbean dolla', '$', 'XCD');
INSERT INTO `country` VALUES (185, 'Saint Martin', 'MF', '+590', '', '', '');
INSERT INTO `country` VALUES (186, 'Saint Pierre and Miq', 'PM', '+508', '', '', '');
INSERT INTO `country` VALUES (187, 'Saint Vincent and th', 'VC', '+1784', '', '', '');
INSERT INTO `country` VALUES (188, 'Samoa', 'WS', '+685', 'Samoan t?l?', 'T', 'WST');
INSERT INTO `country` VALUES (189, 'San Marino', 'SM', '+378', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (190, 'Sao Tome and Princip', 'ST', '+239', '', '', '');
INSERT INTO `country` VALUES (191, 'Saudi Arabia', 'SA', '+966', 'Saudi riyal', 'ر.س', 'SAR');
INSERT INTO `country` VALUES (192, 'Senegal', 'SN', '+221', 'West African CFA fra', 'Fr', 'XOF');
INSERT INTO `country` VALUES (193, 'Serbia', 'RS', '+381', 'Serbian dinar', 'дин. or din.', 'RSD');
INSERT INTO `country` VALUES (194, 'Seychelles', 'SC', '+248', 'Seychellois rupee', '₨', 'SCR');
INSERT INTO `country` VALUES (195, 'Sierra Leone', 'SL', '+232', 'Sierra Leonean leone', 'Le', 'SLL');
INSERT INTO `country` VALUES (196, 'Singapore', 'SG', '+65', 'Brunei dollar', '$', 'BND');
INSERT INTO `country` VALUES (197, 'Slovakia', 'SK', '+421', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (198, 'Slovenia', 'SI', '+386', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (199, 'Solomon Islands', 'SB', '+677', 'Solomon Islands doll', '$', 'SBD');
INSERT INTO `country` VALUES (200, 'Somalia', 'SO', '+252', 'Somali shilling', 'Sh', 'SOS');
INSERT INTO `country` VALUES (201, 'South Africa', 'ZA', '+27', 'South African rand', 'R', 'ZAR');
INSERT INTO `country` VALUES (202, 'South Georgia and th', 'GS', '+500', '', '', '');
INSERT INTO `country` VALUES (203, 'Spain', 'ES', '+34', 'Euro', '€', 'EUR');
INSERT INTO `country` VALUES (204, 'Sri Lanka', 'LK', '+94', 'Sri Lankan rupee', 'Rs or රු', 'LKR');
INSERT INTO `country` VALUES (205, 'Sudan', 'SD', '+249', 'Sudanese pound', 'ج.س.', 'SDG');
INSERT INTO `country` VALUES (206, 'Suriname', 'SR', '+597', 'Surinamese dollar', '$', 'SRD');
INSERT INTO `country` VALUES (207, 'Svalbard and Jan May', 'SJ', '+47', '', '', '');
INSERT INTO `country` VALUES (208, 'Swaziland', 'SZ', '+268', 'Swazi lilangeni', 'L', 'SZL');
INSERT INTO `country` VALUES (209, 'Sweden', 'SE', '+46', 'Swedish krona', 'kr', 'SEK');
INSERT INTO `country` VALUES (210, 'Switzerland', 'CH', '+41', 'Swiss franc', 'Fr', 'CHF');
INSERT INTO `country` VALUES (211, 'Syrian Arab Republic', 'SY', '+963', '', '', '');
INSERT INTO `country` VALUES (212, 'Taiwan', 'TW', '+886', 'New Taiwan dollar', '$', 'TWD');
INSERT INTO `country` VALUES (213, 'Tajikistan', 'TJ', '+992', 'Tajikistani somoni', 'ЅМ', 'TJS');
INSERT INTO `country` VALUES (214, 'Tanzania, United Rep', 'TZ', '+255', '', '', '');
INSERT INTO `country` VALUES (215, 'Thailand', 'TH', '+66', 'Thai baht', '฿', 'THB');
INSERT INTO `country` VALUES (216, 'Timor-Leste', 'TL', '+670', '', '', '');
INSERT INTO `country` VALUES (217, 'Togo', 'TG', '+228', 'West African CFA fra', 'Fr', 'XOF');
INSERT INTO `country` VALUES (218, 'Tokelau', 'TK', '+690', '', '', '');
INSERT INTO `country` VALUES (219, 'Tonga', 'TO', '+676', 'Tongan pa?anga', 'T$', 'TOP');
INSERT INTO `country` VALUES (220, 'Trinidad and Tobago', 'TT', '+1868', 'Trinidad and Tobago ', '$', 'TTD');
INSERT INTO `country` VALUES (221, 'Tunisia', 'TN', '+216', 'Tunisian dinar', 'د.ت', 'TND');
INSERT INTO `country` VALUES (222, 'Turkey', 'TR', '+90', 'Turkish lira', '', 'TRY');
INSERT INTO `country` VALUES (223, 'Turkmenistan', 'TM', '+993', 'Turkmenistan manat', 'm', 'TMT');
INSERT INTO `country` VALUES (224, 'Turks and Caicos Isl', 'TC', '+1649', '', '', '');
INSERT INTO `country` VALUES (225, 'Tuvalu', 'TV', '+688', 'Australian dollar', '$', 'AUD');
INSERT INTO `country` VALUES (226, 'Uganda', 'UG', '+256', 'Ugandan shilling', 'Sh', 'UGX');
INSERT INTO `country` VALUES (227, 'Ukraine', 'UA', '+380', 'Ukrainian hryvnia', '₴', 'UAH');
INSERT INTO `country` VALUES (228, 'United Arab Emirates', 'AE', '+971', 'United Arab Emirates', 'د.إ', 'AED');
INSERT INTO `country` VALUES (229, 'United Kingdom', 'GB', '+44', 'British pound', '£', 'GBP');
INSERT INTO `country` VALUES (230, 'United States', 'US', '+1', 'United States dollar', '$', 'USD');
INSERT INTO `country` VALUES (231, 'Uruguay', 'UY', '+598', 'Uruguayan peso', '$', 'UYU');
INSERT INTO `country` VALUES (232, 'Uzbekistan', 'UZ', '+998', 'Uzbekistani som', '', 'UZS');
INSERT INTO `country` VALUES (233, 'Vanuatu', 'VU', '+678', 'Vanuatu vatu', 'Vt', 'VUV');
INSERT INTO `country` VALUES (234, 'Venezuela, Bolivaria', 'VE', '+58', '', '', '');
INSERT INTO `country` VALUES (235, 'Vietnam', 'VN', '+84', 'Vietnamese ??ng', '₫', 'VND');
INSERT INTO `country` VALUES (236, 'Virgin Islands, Brit', 'VG', '+1284', '', '', '');
INSERT INTO `country` VALUES (237, 'Virgin Islands, U.S.', 'VI', '+1340', '', '', '');
INSERT INTO `country` VALUES (238, 'Wallis and Futuna', 'WF', '+681', 'CFP franc', 'Fr', 'XPF');
INSERT INTO `country` VALUES (239, 'Yemen', 'YE', '+967', 'Yemeni rial', '﷼', 'YER');
INSERT INTO `country` VALUES (240, 'Zambia', 'ZM', '+260', 'Zambian kwacha', 'ZK', 'ZMW');
INSERT INTO `country` VALUES (241, 'Zimbabwe', 'ZW', '+263', 'Botswana pula', 'P', 'BWP');

-- ----------------------------
-- Table structure for currency
-- ----------------------------
DROP TABLE IF EXISTS `currency`;
CREATE TABLE `currency`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `symbol` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `paypal_supported` int NOT NULL DEFAULT 0,
  `stripe_supported` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 113 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of currency
-- ----------------------------
INSERT INTO `currency` VALUES (1, 'Leke', 'ALL', 'Lek', 0, 1);
INSERT INTO `currency` VALUES (2, 'Dollars', 'USD', '$', 1, 1);
INSERT INTO `currency` VALUES (3, 'Afghanis', 'AFN', '؋', 0, 1);
INSERT INTO `currency` VALUES (4, 'Pesos', 'ARS', '$', 0, 1);
INSERT INTO `currency` VALUES (5, 'Guilders', 'AWG', 'ƒ', 0, 1);
INSERT INTO `currency` VALUES (6, 'Dollars', 'AUD', '$', 1, 1);
INSERT INTO `currency` VALUES (7, 'New Manats', 'AZN', 'ман', 0, 1);
INSERT INTO `currency` VALUES (8, 'Dollars', 'BSD', '$', 0, 1);
INSERT INTO `currency` VALUES (9, 'Dollars', 'BBD', '$', 0, 1);
INSERT INTO `currency` VALUES (10, 'Rubles', 'BYR', 'p.', 0, 0);
INSERT INTO `currency` VALUES (11, 'Euro', 'EUR', '€', 1, 1);
INSERT INTO `currency` VALUES (12, 'Dollars', 'BZD', 'BZ$', 0, 1);
INSERT INTO `currency` VALUES (13, 'Dollars', 'BMD', '$', 0, 1);
INSERT INTO `currency` VALUES (14, 'Bolivianos', 'BOB', '$b', 0, 1);
INSERT INTO `currency` VALUES (15, 'Convertible Marka', 'BAM', 'KM', 0, 1);
INSERT INTO `currency` VALUES (16, 'Pula', 'BWP', 'P', 0, 1);
INSERT INTO `currency` VALUES (17, 'Leva', 'BGN', 'лв', 0, 1);
INSERT INTO `currency` VALUES (18, 'Reais', 'BRL', 'R$', 1, 1);
INSERT INTO `currency` VALUES (19, 'Pounds', 'GBP', '£', 1, 1);
INSERT INTO `currency` VALUES (20, 'Dollars', 'BND', '$', 0, 1);
INSERT INTO `currency` VALUES (21, 'Riels', 'KHR', '៛', 0, 1);
INSERT INTO `currency` VALUES (22, 'Dollars', 'CAD', '$', 1, 1);
INSERT INTO `currency` VALUES (23, 'Dollars', 'KYD', '$', 0, 1);
INSERT INTO `currency` VALUES (24, 'Pesos', 'CLP', '$', 0, 1);
INSERT INTO `currency` VALUES (25, 'Yuan Renminbi', 'CNY', '¥', 0, 1);
INSERT INTO `currency` VALUES (26, 'Pesos', 'COP', '$', 0, 1);
INSERT INTO `currency` VALUES (27, 'Colón', 'CRC', '₡', 0, 1);
INSERT INTO `currency` VALUES (28, 'Kuna', 'HRK', 'kn', 0, 1);
INSERT INTO `currency` VALUES (29, 'Pesos', 'CUP', '₱', 0, 0);
INSERT INTO `currency` VALUES (30, 'Koruny', 'CZK', 'Kč', 1, 1);
INSERT INTO `currency` VALUES (31, 'Kroner', 'DKK', 'kr', 1, 1);
INSERT INTO `currency` VALUES (32, 'Pesos', 'DOP ', 'RD$', 0, 1);
INSERT INTO `currency` VALUES (33, 'Dollars', 'XCD', '$', 0, 1);
INSERT INTO `currency` VALUES (34, 'Pounds', 'EGP', '£', 0, 1);
INSERT INTO `currency` VALUES (35, 'Colones', 'SVC', '$', 0, 0);
INSERT INTO `currency` VALUES (36, 'Pounds', 'FKP', '£', 0, 1);
INSERT INTO `currency` VALUES (37, 'Dollars', 'FJD', '$', 0, 1);
INSERT INTO `currency` VALUES (38, 'Cedis', 'GHC', '¢', 0, 0);
INSERT INTO `currency` VALUES (39, 'Pounds', 'GIP', '£', 0, 1);
INSERT INTO `currency` VALUES (40, 'Quetzales', 'GTQ', 'Q', 0, 1);
INSERT INTO `currency` VALUES (41, 'Pounds', 'GGP', '£', 0, 0);
INSERT INTO `currency` VALUES (42, 'Dollars', 'GYD', '$', 0, 1);
INSERT INTO `currency` VALUES (43, 'Lempiras', 'HNL', 'L', 0, 1);
INSERT INTO `currency` VALUES (44, 'Dollars', 'HKD', '$', 1, 1);
INSERT INTO `currency` VALUES (45, 'Forint', 'HUF', 'Ft', 1, 1);
INSERT INTO `currency` VALUES (46, 'Kronur', 'ISK', 'kr', 0, 1);
INSERT INTO `currency` VALUES (47, 'Rupees', 'INR', 'Rs', 1, 1);
INSERT INTO `currency` VALUES (48, 'Rupiahs', 'IDR', 'Rp', 0, 1);
INSERT INTO `currency` VALUES (49, 'Rials', 'IRR', '﷼', 0, 0);
INSERT INTO `currency` VALUES (50, 'Pounds', 'IMP', '£', 0, 0);
INSERT INTO `currency` VALUES (51, 'New Shekels', 'ILS', '₪', 1, 1);
INSERT INTO `currency` VALUES (52, 'Dollars', 'JMD', 'J$', 0, 1);
INSERT INTO `currency` VALUES (53, 'Yen', 'JPY', '¥', 1, 1);
INSERT INTO `currency` VALUES (54, 'Pounds', 'JEP', '£', 0, 0);
INSERT INTO `currency` VALUES (55, 'Tenge', 'KZT', 'лв', 0, 1);
INSERT INTO `currency` VALUES (56, 'Won', 'KPW', '₩', 0, 0);
INSERT INTO `currency` VALUES (57, 'Won', 'KRW', '₩', 0, 1);
INSERT INTO `currency` VALUES (58, 'Soms', 'KGS', 'лв', 0, 1);
INSERT INTO `currency` VALUES (59, 'Kips', 'LAK', '₭', 0, 1);
INSERT INTO `currency` VALUES (60, 'Lati', 'LVL', 'Ls', 0, 0);
INSERT INTO `currency` VALUES (61, 'Pounds', 'LBP', '£', 0, 1);
INSERT INTO `currency` VALUES (62, 'Dollars', 'LRD', '$', 0, 1);
INSERT INTO `currency` VALUES (63, 'Switzerland Francs', 'CHF', 'CHF', 1, 1);
INSERT INTO `currency` VALUES (64, 'Litai', 'LTL', 'Lt', 0, 0);
INSERT INTO `currency` VALUES (65, 'Denars', 'MKD', 'ден', 0, 1);
INSERT INTO `currency` VALUES (66, 'Ringgits', 'MYR', 'RM', 1, 1);
INSERT INTO `currency` VALUES (67, 'Rupees', 'MUR', '₨', 0, 1);
INSERT INTO `currency` VALUES (68, 'Pesos', 'MXN', '$', 1, 1);
INSERT INTO `currency` VALUES (69, 'Tugriks', 'MNT', '₮', 0, 1);
INSERT INTO `currency` VALUES (70, 'Meticais', 'MZN', 'MT', 0, 1);
INSERT INTO `currency` VALUES (71, 'Dollars', 'NAD', '$', 0, 1);
INSERT INTO `currency` VALUES (72, 'Rupees', 'NPR', '₨', 0, 1);
INSERT INTO `currency` VALUES (73, 'Guilders', 'ANG', 'ƒ', 0, 1);
INSERT INTO `currency` VALUES (74, 'Dollars', 'NZD', '$', 1, 1);
INSERT INTO `currency` VALUES (75, 'Cordobas', 'NIO', 'C$', 0, 1);
INSERT INTO `currency` VALUES (76, 'Nairas', 'NGN', '₦', 0, 1);
INSERT INTO `currency` VALUES (77, 'Krone', 'NOK', 'kr', 1, 1);
INSERT INTO `currency` VALUES (78, 'Rials', 'OMR', '﷼', 0, 0);
INSERT INTO `currency` VALUES (79, 'Rupees', 'PKR', '₨', 0, 1);
INSERT INTO `currency` VALUES (80, 'Balboa', 'PAB', 'B/.', 0, 1);
INSERT INTO `currency` VALUES (81, 'Guarani', 'PYG', 'Gs', 0, 1);
INSERT INTO `currency` VALUES (82, 'Nuevos Soles', 'PEN', 'S/.', 0, 1);
INSERT INTO `currency` VALUES (83, 'Pesos', 'PHP', 'Php', 1, 1);
INSERT INTO `currency` VALUES (84, 'Zlotych', 'PLN', 'zł', 1, 1);
INSERT INTO `currency` VALUES (85, 'Rials', 'QAR', '﷼', 0, 1);
INSERT INTO `currency` VALUES (86, 'New Lei', 'RON', 'lei', 0, 1);
INSERT INTO `currency` VALUES (87, 'Rubles', 'RUB', 'руб', 0, 1);
INSERT INTO `currency` VALUES (88, 'Pounds', 'SHP', '£', 0, 1);
INSERT INTO `currency` VALUES (89, 'Riyals', 'SAR', '﷼', 0, 1);
INSERT INTO `currency` VALUES (90, 'Dinars', 'RSD', 'Дин.', 0, 1);
INSERT INTO `currency` VALUES (91, 'Rupees', 'SCR', '₨', 0, 1);
INSERT INTO `currency` VALUES (92, 'Dollars', 'SGD', '$', 1, 1);
INSERT INTO `currency` VALUES (93, 'Dollars', 'SBD', '$', 0, 1);
INSERT INTO `currency` VALUES (94, 'Shillings', 'SOS', 'S', 0, 1);
INSERT INTO `currency` VALUES (95, 'Rand', 'ZAR', 'R', 0, 1);
INSERT INTO `currency` VALUES (96, 'Rupees', 'LKR', '₨', 0, 1);
INSERT INTO `currency` VALUES (97, 'Kronor', 'SEK', 'kr', 1, 1);
INSERT INTO `currency` VALUES (98, 'Dollars', 'SRD', '$', 0, 1);
INSERT INTO `currency` VALUES (99, 'Pounds', 'SYP', '£', 0, 0);
INSERT INTO `currency` VALUES (100, 'New Dollars', 'TWD', 'NT$', 1, 1);
INSERT INTO `currency` VALUES (101, 'Baht', 'THB', '฿', 1, 1);
INSERT INTO `currency` VALUES (102, 'Dollars', 'TTD', 'TT$', 0, 1);
INSERT INTO `currency` VALUES (103, 'Lira', 'TRY', 'TL', 0, 1);
INSERT INTO `currency` VALUES (104, 'Liras', 'TRL', '£', 0, 0);
INSERT INTO `currency` VALUES (105, 'Dollars', 'TVD', '$', 0, 0);
INSERT INTO `currency` VALUES (106, 'Hryvnia', 'UAH', '₴', 0, 1);
INSERT INTO `currency` VALUES (107, 'Pesos', 'UYU', '$U', 0, 1);
INSERT INTO `currency` VALUES (108, 'Sums', 'UZS', 'лв', 0, 1);
INSERT INTO `currency` VALUES (109, 'Bolivares Fuertes', 'VEF', 'Bs', 0, 0);
INSERT INTO `currency` VALUES (110, 'Dong', 'VND', '₫', 0, 1);
INSERT INTO `currency` VALUES (111, 'Rials', 'YER', '﷼', 0, 1);
INSERT INTO `currency` VALUES (112, 'Zimbabwe Dollars', 'ZWD', 'Z$', 0, 0);

-- ----------------------------
-- Table structure for facebook_messenger
-- ----------------------------
DROP TABLE IF EXISTS `facebook_messenger`;
CREATE TABLE `facebook_messenger`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `listing_id` int NULL DEFAULT NULL,
  `page_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `color` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `logged_in_greeting` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of facebook_messenger
-- ----------------------------

-- ----------------------------
-- Table structure for food_menu
-- ----------------------------
DROP TABLE IF EXISTS `food_menu`;
CREATE TABLE `food_menu`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `listing_id` int NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `items` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `photo` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of food_menu
-- ----------------------------
INSERT INTO `food_menu` VALUES (1, 3, 'menu 1', '34', 'meat,potatoes,bread', 'd83b7689f02509ea1f0e4ab0bdb507d0.jpg');
INSERT INTO `food_menu` VALUES (2, 3, 'menu 2', '67', 'cake', '2013e0b85a78ea22dd03c4f00b061712.jpg');

-- ----------------------------
-- Table structure for frontend_settings
-- ----------------------------
DROP TABLE IF EXISTS `frontend_settings`;
CREATE TABLE `frontend_settings`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of frontend_settings
-- ----------------------------
INSERT INTO `frontend_settings` VALUES (1, 'banner_title', 'Atlas Business Directory Listing');
INSERT INTO `frontend_settings` VALUES (2, 'banner_sub_title', 'Subtitle Of Atlas Directory Listing');
INSERT INTO `frontend_settings` VALUES (3, 'about_us', '<p>About us</p>\r\n');
INSERT INTO `frontend_settings` VALUES (4, 'terms_and_condition', '<p>Terms and conditions</p>\r\n');
INSERT INTO `frontend_settings` VALUES (5, 'privacy_policy', '<p>Privacy Poilicy</p>\r\n');
INSERT INTO `frontend_settings` VALUES (6, 'social_links', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\",\"google\":\"\",\"instagram\":\"\",\"pinterest\":\"\"}');
INSERT INTO `frontend_settings` VALUES (7, 'slogan', 'Find your local places, you love most to roam around.');
INSERT INTO `frontend_settings` VALUES (8, 'faq', '<p>Faq</p>\r\n');
INSERT INTO `frontend_settings` VALUES (9, 'cookie_note', 'This Website Uses Cookies To Personalize Content And Analyse Traffic In Order To Offer You A Better Experience.');
INSERT INTO `frontend_settings` VALUES (10, 'cookie_status', '1');
INSERT INTO `frontend_settings` VALUES (11, 'cookie_policy', '<h1>Cookie policy</h1>\r\n\r\n<ol>\r\n	<li>Cookies are small text files that can be used by websites to make a user&#39;s experience more efficient.</li>\r\n	<li>The law states that we can store cookies on your device if they are strictly necessary for the operation of this site. For all other types of cookies we need your permission.</li>\r\n	<li>This site uses different types of cookies. Some cookies are placed by third party services that appear on our pages.</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n');

-- ----------------------------
-- Table structure for hotel_room_specification
-- ----------------------------
DROP TABLE IF EXISTS `hotel_room_specification`;
CREATE TABLE `hotel_room_specification`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `listing_id` int NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `amenities` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `photo` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of hotel_room_specification
-- ----------------------------
INSERT INTO `hotel_room_specification` VALUES (1, 2, 'room 1', 'Room description 1', '45', 'Room description ', '0d75303cfb3e35992095e8eb59c83fea.jpg');
INSERT INTO `hotel_room_specification` VALUES (2, 2, 'Room 2', 'Room description', '80', 'Room description', '8e6d6269d0f9ffb552b6bd96ef6fb253.jpg');

-- ----------------------------
-- Table structure for inventory
-- ----------------------------
DROP TABLE IF EXISTS `inventory`;
CREATE TABLE `inventory`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `listing_id` int NULL DEFAULT NULL,
  `category_id` int NULL DEFAULT NULL,
  `details` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `availability` int NULL DEFAULT NULL,
  `thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of inventory
-- ----------------------------

-- ----------------------------
-- Table structure for inventory_category
-- ----------------------------
DROP TABLE IF EXISTS `inventory_category`;
CREATE TABLE `inventory_category`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `listing_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of inventory_category
-- ----------------------------

-- ----------------------------
-- Table structure for job_cv
-- ----------------------------
DROP TABLE IF EXISTS `job_cv`;
CREATE TABLE `job_cv`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `listing_id` int NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `cover_letter` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `cv` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `optional_attachment` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of job_cv
-- ----------------------------
INSERT INTO `job_cv` VALUES (1, 6, 'admin@gmail.com', 'name first', 'name last', '123456789', '<p>Cover letter&nbsp;Cover letter</p>\r\n', 'my_cv.docx_87ceea9693.docx', 'my_op_att.docx_187d26743d.docx', '2020-12-09 03:44:43');
INSERT INTO `job_cv` VALUES (2, 6, 'newname@gmail.com', 'hj name', 's lasttt', '123006789', '<p>Cover letter&nbsp;Cover letter</p>\r\n', 'my_cv.docx_27159bcd08.docx', 'my_op_att.docx_4e55bac266.docx', '2020-12-09 03:45:59');
INSERT INTO `job_cv` VALUES (3, 6, 'emmm@gg.gg', 'name first', 'name last', '87980002', '', 'my_cv.docx_62b250a57b.docx', 'my_op_att.docx_b0be693eb9.docx', '2020-12-09 03:48:41');

-- ----------------------------
-- Table structure for job_deadline
-- ----------------------------
DROP TABLE IF EXISTS `job_deadline`;
CREATE TABLE `job_deadline`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `listing_id` int NULL DEFAULT NULL,
  `date` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of job_deadline
-- ----------------------------
INSERT INTO `job_deadline` VALUES (1, 25, '2020-12-29');
INSERT INTO `job_deadline` VALUES (2, 6, '2020-12-29');
INSERT INTO `job_deadline` VALUES (3, 22, '2020-12-29');
INSERT INTO `job_deadline` VALUES (4, 23, '2020-12-29');
INSERT INTO `job_deadline` VALUES (5, 24, '2020-12-29');

-- ----------------------------
-- Table structure for job_details
-- ----------------------------
DROP TABLE IF EXISTS `job_details`;
CREATE TABLE `job_details`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `listing_id` int NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `employment_term` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `job_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `job_responsibilities` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `required_qualifications` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `variant` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `photo` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of job_details
-- ----------------------------
INSERT INTO `job_details` VALUES (1, 6, 'Front-end developer', 'Permanent', 'Full time', 'ZOOM GRAPHICS is looking for a creative HTML5 / CSS3 / Javascript developer, ready for the opportunity to take a part in development of various PIXEL PEREFCT web applications, e-commerce platforms and other web-based solutions. Responsibilities will include the translation of the UI/UX design wireframes to actual code that will produce visual elements of the application and their behaviour. The selected candidate will develop, maintain and support new or existing software products while applying the best practices of software development. ', 'Design and develop new functionality for our software on all commonly used platforms using C# and JavaScript,\r\nDesign and develop new features for our software on all commonly used platforms using C++ ', 'A university degree in the field of information technology or a related subject or relevant work experience\r\n5+ years of professional experience in the field of software development ', '10', 'Architecture Design,Database architecture,Design patterns,C++,OOP', '2d92ee56e0f2b1338171239aeb919368.jpg');
INSERT INTO `job_details` VALUES (3, 22, 'Front-end developer', 'Permanent', 'Training', 'ZOOM GRAPHICS is looking for a creative HTML5 / CSS3 / Javascript developer, ready for the opportunity to take a part in development of various PIXEL PEREFCT web applications, e-commerce platforms and other web-based solutions. Responsibilities will include the translation of the UI/UX design wireframes to actual code that will produce visual elements of the application and their behaviour. The selected candidate will develop, maintain and support new or existing software products while applying the best practices of software development. ', 'Design and develop new functionality for our software on all commonly used platforms using C# and JavaScript,\r\nDesign and develop new features for our software on all commonly used platforms using C++ ', 'A university degree in the field of information technology or a related subject or relevant work experience\r\n5+ years of professional experience in the field of software development ', '10', 'Architecture Design,Database architecture,Design patterns,C++,OOP', 'a062828921b42657687449305700f788.jpg');
INSERT INTO `job_details` VALUES (5, 23, 'Back-end developer', 'Freelance', 'Full time', 'ZOOM GRAPHICS is looking for a creative HTML5 / CSS3 / Javascript developer, ready for the opportunity to take a part in development of various PIXEL PEREFCT web applications, e-commerce platforms and other web-based solutions. Responsibilities will include the translation of the UI/UX design wireframes to actual code that will produce visual elements of the application and their behaviour. The selected candidate will develop, maintain and support new or existing software products while applying the best practices of software development. ', 'Design and develop new functionality for our software on all commonly used platforms using C# and JavaScript,\r\nDesign and develop new features for our software on all commonly used platforms using C++ ', 'A university degree in the field of information technology or a related subject or relevant work experience\r\n5+ years of professional experience in the field of software development ', '10', 'Architecture Design,Database architecture,Design patterns,C++,OOP', '70705cab860023f050b4e077b2ec600a.jpg');
INSERT INTO `job_details` VALUES (7, 24, 'HR Specialist', 'Permanent', 'Part time', 'We are looking for a scrum enthusiast to join us as a Scrum Master and ensure the team lives agile values and principles. ', 'To do Data entry;\r\nTo track CRF/DCF;', 'Have at least a Bachelor’s degree in clinical/pharmacy/biological/mathematical sciences or in any related field;\r\nAre detail-oriented;', '9', 'OOP,C++,C#,Architecture Design,hr', '5ec423d4ae78bce5e44df8a288439b7e.jpg');
INSERT INTO `job_details` VALUES (9, 25, 'Scrum Master (m/f/d) IoT', 'Internship', 'Part time', 'We are looking for a scrum enthusiast to join us as a Scrum Master and ensure the team lives agile values and principles. ', 'To do Data entry;\r\nTo track CRF/DCF;', 'Have at least a Bachelor’s degree in clinical/pharmacy/biological/mathematical sciences or in any related field;\r\nAre detail-oriented;', '9', 'OOP,C++,C#,Architecture Design', '859273310d17f921c8a7da1f05dffb38.jpg');

-- ----------------------------
-- Table structure for listing
-- ----------------------------
DROP TABLE IF EXISTS `listing`;
CREATE TABLE `listing`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `categories` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `amenities` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `photos` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `video_url` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `video_provider` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `tags` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `website` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `social` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `user_id` int NULL DEFAULT NULL,
  `latitude` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `longitude` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `country_id` int NULL DEFAULT NULL,
  `city_id` int NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `listing_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `listing_thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `listing_cover` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `seo_meta_tags` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `meta_description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `date_added` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `date_modified` int NULL DEFAULT NULL,
  `is_featured` int NOT NULL DEFAULT 0,
  `google_analytics_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of listing
-- ----------------------------
INSERT INTO `listing` VALUES (2, 'acb5441e1bceaea7a5d28e675d6a1041', 'Park Plaza Westminster Bridge London', 'Located on the South Bank of the Thames, Park Plaza Westminster Bridge London is set opposite the Houses of Parliament and Big Ben, on the South Bank. It is less than a 5-minute walk from the London Eye, the Aquarium, restaurants and theatres.\r\n\r\nThere is a fully equipped 24-hour gym, a 15 m swimming pool with a sauna and steam room, and the Mandara Spa, offering a range of luxury treatments.\r\n\r\nThe large, contemporary air-conditioned rooms have modern amenities, including a flat-screen TV, large desk, minibar and safe.\r\n\r\nThe hotel features the modern, award-winning Brasserie Joël and Ichi Sushi &amp; Sashimi, which specialises in sushi and sashimi. The 1WB Lounge &amp; Patisserie serves traditional afternoon tea and all-day tapas. The illy Caffè serves Italian coffee and pastries. Primo Bar offers live evening entertainment every night.\r\n\r\nThe hotel is within a short walk from Westminster and Waterloo tube stations, offering links to the top attractions of the city.\r\n\r\nThis is our guests&#039; favourite part of London, according to independent reviews.\r\n\r\nWe speak your language!', '[\"2\"]', '[]', '[\"3b75c3c9091d076d67cfaaf8bd9ccfc9.jpg\",\"c07784511dfb16b04606acf5dade8474.jpg\",\"d23e917740f42d2fd95f18fd562f702c.jpg\",\"7064798c14889726cac9fe90daa4970d.jpg\",\"f607941b24ce112983d72a544d4db01b.jpg\",\"39d8a941c38fce367195f99aa90a8610.jpg\",\"17fff58f51aa5d7cadbfecffe8e856ed.jpg\",\"782ee8cae9a2921055b5eb0ef08e69d7.jpg\"]', '', 'youtube', NULL, 'asss', 'volodya.avetisyan@gmail.com', '+37493123122', 'Mutant X', '{\"facebook\":\"fb\",\"twitter\":\"tw\",\"linkedin\":\"li\"}', 1, '40.209419', '44.483606', 229, 157, 'active', 'hotel', 'ee9ae50f1f804d4279cfd6629098e99c.jpg', 'e65b550093c53c94593d97320c44bf69.jpg', '', '', '1599548400', 1606464000, 0, '');
INSERT INTO `listing` VALUES (3, '2248c5caf5caa347b417c346306934a1', 'Restaurant Le LOFT', 'French, European, Healthy, Vegetarian Friendly, Vegan Options', '[\"3\"]', '[]', '[\"67e20c53cc2e7f14daa9659366e63c2a.jpg\",\"6f6439ca65095dd94a0fec3e268b7b1e.jpg\",\"c0b1cfa7bf764721f0c4ee7b9b376424.jpg\",\"248a275e5cb10b5317f39b4f9496622f.jpg\",\"b23ab9de3c54a227c2e811fd390ac199.jpg\",\"1669c78f937c862bf6e27936876a333a.jpg\"]', '', 'youtube', NULL, 'asxxxxx', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '40.209419', '44.483606', 230, 157, 'active', 'restaurant', 'dc391c61626637f9beef9e5631526304.jpg', '64b85aae25098ff99ed5b0a0cdf1b9e4.jpg', '', '', '1599548400', 1606464000, 0, '');
INSERT INTO `listing` VALUES (4, '647998eea613cc5611e9272b1d7d7fd0', 'MANAN Beauty Salon', 'Manan Beauty Salon lovingly opens its doors to everyone.', '[\"4\"]', '[]', '[\"a53681b78c8ebedd8d96ddddf7cb6cf5.jpg\",\"dc7c61642c35018e5698a8056869c636.jpg\",\"b53f79b2c95540f50f2c99c8078aab37.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 3, '40.209419', '44.483606', 230, 157, 'active', 'beauty', '9935df8a8ff8ac6b72f35088ffb30751.jpg', 'a4da8e04cabb33f5aa9e5efa6b4618ac.jpg', '', '', '1600412400', 1606464000, 0, '');
INSERT INTO `listing` VALUES (6, '8f442fe49e1c01dfc8f77d1992d05255', 'Job new Title', 'Addevice is a custom mobile app development company delivering solutions to startup and enterprise clients. Over 6 years of our history, we’ve successfully delivered a lot of projects and helped our clients and brands establish a strong online presence.', '[\"5\",\"25\",\"27\"]', '[]', '[]', 'https://www.youtube.com/watch?v=YT_63UntRJE&amp;t=260s', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '34.102', '-118.344', 230, 157, 'active', 'job', 'fd5faf200f4dee1600a61c9d7c9e25eb.jpg', '24852398b90b4883b93778c958075f32.jpg', '', '', '1600326000', 1609660800, 0, '');
INSERT INTO `listing` VALUES (7, 'a6851ef102643286dfa7afa0766f9cba', '1200 California St Unit 15A, San Francisco, CA, 94109', 'For the first time in 30 years, this rare view corner residence is available for sale! With Golden Gate Bridge, Bay and City views, the residence boasts floor-to-ceiling windows that basks the home with abundant natural light. Step outside onto the private balcony to breathe fresh air and enjoy and relax. Last remodeled 1987, this residence awaits your buyer&#039;s personal signature! Located at the intersection of Pacific Heights, Cow Hollow, Russian Hill, and Nob Hill, Jackson Towers has a 99 walk score and is a well-run HOA. Its amenities include an updated lobby, elevator bank, and an enormous shared rooftop with 360 classic San Francisco views.', '[\"7\",\"21\"]', '[]', '[\"8ee1ce082d7856ac97adae27ef7ab4d7.jpg\",\"348a27d4319072b2b929a93135406be4.jpg\",\"67be0a00d379d672de7f03a9186157b1.jpg\",\"1d65697a525f5410711a05b6bb85cc4b.jpg\",\"8fd3dddd3465172c5319cc58342ef5d1.jpg\",\"fcbaa39a6e0c5c83ba12750113183960.jpg\",\"887f362dc45b14fe049d7606858bce3e.jpg\",\"bf80fb418e68df307630fa15bb8a321b.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '212121212', 230, 157, 'active', 'real-estate', '756fb17495d355eb102b254fd8e4b158.jpg', '99980aa364346c532ce3ec0896690ebd.jpg', '', '', '1600412400', 1606464000, 0, '');
INSERT INTO `listing` VALUES (8, '7dde04620de06595d1b5884e918cd2a6', 'Certified 2019 Nissan Sentra SRs', '', '[\"6\",\"18\"]', '[]', '[\"6594310f92c868ceface3bac101b4692.jpg\",\"394d673226fa47ab6c09dfecfb548923.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 0, 'a', 'a', 230, 157, 'active', 'car', 'da4561c292f7fdfbfcfe34e97e3d92a6.jpg', '9d742dca542e5cd2d6612ca17dc7658e.jpg', '', '', '1600412400', 1606464000, 0, '');
INSERT INTO `listing` VALUES (10, 'a7caa4af00caf81478b5ce2b7f7bc9f6', '2020 Nissan Kicks SV', 'Recent Arrival! CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags, Electronic Stability Control, Exterior Parking Camera Rear, Four wheel independent suspension, Front anti-roll bar, Front Bucket Seats, Front Center Armrest, Front dual zone A/C, Front reading lights, Fully automatic headlights, Garage door transmitter: HomeLink, Heated door mirrors, Heated front seats, Heated Front Sport Bucket Seats, Illuminated entry, Leather Shift Knob, Leather steering wheel, Leatherette-Trimmed Interior, Low tire pressure warning, Memory seat, Occupant sensing airbag, Outside temperature display, Overhead airbag, Overhead console, Panic alarm, Passenger door bin, Passenger vanity mirror, Power door mirrors, Power driver seat, Power Liftgate, Power moonroof, Power passenger seat, Power steering, Power windows, Radio data system, Radio: Acura Premium Audio System, Rear anti-roll bar, Rear seat center armrest, Rear window defroster, Rear window wiper, Remote keyless entry, Security system, Speed control, Speed-sensing steering, Speed-Sensitive Wipers, Split folding rear seat, Spoiler, Steering wheel mounted audio controls, Tachometer, Telescoping steering wheel, Tilt steering wheel, Traction control, Trip computer, Turn signal indicator mirrors, Variably inte Come experience the first and only Acura dealership in the city of Chicago! Conveniently located off the Kennedy expressway at the Division exit! Our facility is equipped with 4 themed customer waiting areas, a full service cafe, and a relaxing walkway along the Chicago river! Come experience the McGrath difference!', '[\"6\",\"18\"]', '[]', '[\"b906909b8c480ebdbb17e3fc6b93547c.jpg\",\"f3f2c1b4d14e9ea6e8f8c61895f94297.jpg\",\"6af205469c22df3f861d0446b510d5a7.jpg\",\"9da3fde1abc1342642d5ad8267e58355.jpg\",\"48c100252ef4640ff70eea79c432f5d5.jpg\",\"e71b0cd1e781d8fa2952ff7b33a19889.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '2121212121', 230, 51, 'active', 'car', '59945324a4c6a9b63ce379587aa9630a.jpg', 'b7aafa408d92b91d1483e71352e98821.jpg', '', '', '1606464000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (11, '5b179e04d3c6f2ad079d7bd5a47d5818', '2020 Chevrolet Trax LS', 'Recent Arrival! CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags, Electronic Stability Control, Exterior Parking Camera Rear, Four wheel independent suspension, Front anti-roll bar, Front Bucket Seats, Front Center Armrest, Front dual zone A/C, Front reading lights, Fully automatic headlights, Garage door transmitter: HomeLink, Heated door mirrors, Heated front seats, Heated Front Sport Bucket Seats, Illuminated entry, Leather Shift Knob, Leather steering wheel, Leatherette-Trimmed Interior, Low tire pressure warning, Memory seat, Occupant sensing airbag, Outside temperature display, Overhead airbag, Overhead console, Panic alarm, Passenger door bin, Passenger vanity mirror, Power door mirrors, Power driver seat, Power Liftgate, Power moonroof, Power passenger seat, Power steering, Power windows, Radio data system, Radio: Acura Premium Audio System, Rear anti-roll bar, Rear seat center armrest, Rear window defroster, Rear window wiper, Remote keyless entry, Security system, Speed control, Speed-sensing steering, Speed-Sensitive Wipers, Split folding rear seat, Spoiler, Steering wheel mounted audio controls, Tachometer, Telescoping steering wheel, Tilt steering wheel, Traction control, Trip computer, Turn signal indicator mirrors, Variably inte Come experience the first and only Acura dealership in the city of Chicago! Conveniently located off the Kennedy expressway at the Division exit! Our facility is equipped with 4 themed customer waiting areas, a full service cafe, and a relaxing walkway along the Chicago river! Come experience the McGrath difference!', '[\"6\",\"12\"]', '[]', '[\"667b7ff885e68e667e7093c8691406ea.jpg\",\"e8d855c2cfa82177d8f0fd27ab763588.jpg\",\"779d2014d35d412634e0a008d5748b57.jpg\",\"f456631e2554b4c3f3e9e700bb57affe.jpg\",\"757f57adaa53b4a8b8ce0f77986bc45d.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '2121212121', 230, 29, 'active', 'car', '4cfc468d49500fd1c5c8681a6ec4a3ed.jpg', 'f2a4d992f684db544b757d04e5612819.jpg', '', '', '1606464000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (12, '3430f482234578b1edb10b74712e7dbc', '2021 Chevrolet Spark LS', 'Recent Arrival! CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags, Electronic Stability Control, Exterior Parking Camera Rear, Four wheel independent suspension, Front anti-roll bar, Front Bucket Seats, Front Center Armrest, Front dual zone A/C, Front reading lights, Fully automatic headlights, Garage door transmitter: HomeLink, Heated door mirrors, Heated front seats, Heated Front Sport Bucket Seats, Illuminated entry, Leather Shift Knob, Leather steering wheel, Leatherette-Trimmed Interior, Low tire pressure warning, Memory seat, Occupant sensing airbag, Outside temperature display, Overhead airbag, Overhead console, Panic alarm, Passenger door bin, Passenger vanity mirror, Power door mirrors, Power driver seat, Power Liftgate, Power moonroof, Power passenger seat, Power steering, Power windows, Radio data system, Radio: Acura Premium Audio System, Rear anti-roll bar, Rear seat center armrest, Rear window defroster, Rear window wiper, Remote keyless entry, Security system, Speed control, Speed-sensing steering, Speed-Sensitive Wipers, Split folding rear seat, Spoiler, Steering wheel mounted audio controls, Tachometer, Telescoping steering wheel, Tilt steering wheel, Traction control, Trip computer, Turn signal indicator mirrors, Variably inte Come experience the first and only Acura dealership in the city of Chicago! Conveniently located off the Kennedy expressway at the Division exit! Our facility is equipped with 4 themed customer waiting areas, a full service cafe, and a relaxing walkway along the Chicago river! Come experience the McGrath difference!', '[\"6\",\"12\"]', '[]', '[\"b680f5fce9b9ff25ec4e4f5ccf39323a.jpg\",\"6084bb7a82079076461c4f80a8a00656.jpg\",\"4fb3abe6397bef40d517e9f9238f2e38.jpg\",\"4ff834e943f68103536cf617c04a734e.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '2121212121', 230, 29, 'active', 'car', '6d45fcb32d8883837598e60e7666b896.jpg', '5a5afd52f289ef6c1b1a8d2411cd7a8d.jpg', '', '', '1606464000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (13, '0ea8158a871a1cd2f383180435d9a2a7', '2021 Chevrolet Malibu RS', 'Recent Arrival! CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags, Electronic Stability Control, Exterior Parking Camera Rear, Four wheel independent suspension, Front anti-roll bar, Front Bucket Seats, Front Center Armrest, Front dual zone A/C, Front reading lights, Fully automatic headlights, Garage door transmitter: HomeLink, Heated door mirrors, Heated front seats, Heated Front Sport Bucket Seats, Illuminated entry, Leather Shift Knob, Leather steering wheel, Leatherette-Trimmed Interior, Low tire pressure warning, Memory seat, Occupant sensing airbag, Outside temperature display, Overhead airbag, Overhead console, Panic alarm, Passenger door bin, Passenger vanity mirror, Power door mirrors, Power driver seat, Power Liftgate, Power moonroof, Power passenger seat, Power steering, Power windows, Radio data system, Radio: Acura Premium Audio System, Rear anti-roll bar, Rear seat center armrest, Rear window defroster, Rear window wiper, Remote keyless entry, Security system, Speed control, Speed-sensing steering, Speed-Sensitive Wipers, Split folding rear seat, Spoiler, Steering wheel mounted audio controls, Tachometer, Telescoping steering wheel, Tilt steering wheel, Traction control, Trip computer, Turn signal indicator mirrors, Variably inte Come experience the first and only Acura dealership in the city of Chicago! Conveniently located off the Kennedy expressway at the Division exit! Our facility is equipped with 4 themed customer waiting areas, a full service cafe, and a relaxing walkway along the Chicago river! Come experience the McGrath difference!', '[\"6\",\"12\"]', '[]', '[\"b12a56c3656493ce25621da118002e80.jpg\",\"b1fb6482fccfb185dbaeb25a79a1fd75.jpg\",\"6a0312637183ae1d66afca49be2b3d5f.jpg\",\"c8e405c2d7996b401bd17afb792be607.jpg\",\"cc861b81e414efafb2fa85de10d5db16.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '2121212121', 230, 53, 'active', 'car', 'ce6e204333b01d74057469b4720c2989.jpg', 'ca93ab5cbd02d75a653328fe1bd1e802.jpg', '', '', '1606464000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (14, '35c0c2087f5675e9e879024f8f509792', '2020 BMW i3 120Ah w/Range Extender', 'Recent Arrival! CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags, Electronic Stability Control, Exterior Parking Camera Rear, Four wheel independent suspension, Front anti-roll bar, Front Bucket Seats, Front Center Armrest, Front dual zone A/C, Front reading lights, Fully automatic headlights, Garage door transmitter: HomeLink, Heated door mirrors, Heated front seats, Heated Front Sport Bucket Seats, Illuminated entry, Leather Shift Knob, Leather steering wheel, Leatherette-Trimmed Interior, Low tire pressure warning, Memory seat, Occupant sensing airbag, Outside temperature display, Overhead airbag, Overhead console, Panic alarm, Passenger door bin, Passenger vanity mirror, Power door mirrors, Power driver seat, Power Liftgate, Power moonroof, Power passenger seat, Power steering, Power windows, Radio data system, Radio: Acura Premium Audio System, Rear anti-roll bar, Rear seat center armrest, Rear window defroster, Rear window wiper, Remote keyless entry, Security system, Speed control, Speed-sensing steering, Speed-Sensitive Wipers, Split folding rear seat, Spoiler, Steering wheel mounted audio controls, Tachometer, Telescoping steering wheel, Tilt steering wheel, Traction control, Trip computer, Turn signal indicator mirrors, Variably inte Come experience the first and only Acura dealership in the city of Chicago! Conveniently located off the Kennedy expressway at the Division exit! Our facility is equipped with 4 themed customer waiting areas, a full service cafe, and a relaxing walkway along the Chicago river! Come experience the McGrath difference!', '[\"6\",\"11\"]', '[]', '[\"db7f4da12910714b4fc7b9dc262b5c8f.jpg\",\"d172172849b1996f515875d44f4403aa.jpg\",\"1600ceecf004279b518d01e7065b9d37.jpg\",\"52033bab87898ca7196719d2a2b0ea00.jpg\",\"09ac6bb1a0854feb7717f58a054e70b4.jpg\",\"9c7c6098d29f6b944fcb277c348d7a26.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '2121212121', 230, 51, 'active', 'car', '8160e0712d1695396ef69ff8f5ba08b3.jpg', '8ece1f02614b9b3be224224b91c7045f.jpg', '', '', '1606464000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (15, 'c480f5f6bcec00a6120a6619a37ff43f', '2019 BMW X3 xDrive30i', 'Recent Arrival! CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags, Electronic Stability Control, Exterior Parking Camera Rear, Four wheel independent suspension, Front anti-roll bar, Front Bucket Seats, Front Center Armrest, Front dual zone A/C, Front reading lights, Fully automatic headlights, Garage door transmitter: HomeLink, Heated door mirrors, Heated front seats, Heated Front Sport Bucket Seats, Illuminated entry, Leather Shift Knob, Leather steering wheel, Leatherette-Trimmed Interior, Low tire pressure warning, Memory seat, Occupant sensing airbag, Outside temperature display, Overhead airbag, Overhead console, Panic alarm, Passenger door bin, Passenger vanity mirror, Power door mirrors, Power driver seat, Power Liftgate, Power moonroof, Power passenger seat, Power steering, Power windows, Radio data system, Radio: Acura Premium Audio System, Rear anti-roll bar, Rear seat center armrest, Rear window defroster, Rear window wiper, Remote keyless entry, Security system, Speed control, Speed-sensing steering, Speed-Sensitive Wipers, Split folding rear seat, Spoiler, Steering wheel mounted audio controls, Tachometer, Telescoping steering wheel, Tilt steering wheel, Traction control, Trip computer, Turn signal indicator mirrors, Variably inte Come experience the first and only Acura dealership in the city of Chicago! Conveniently located off the Kennedy expressway at the Division exit! Our facility is equipped with 4 themed customer waiting areas, a full service cafe, and a relaxing walkway along the Chicago river! Come experience the McGrath difference!', '[\"6\",\"11\"]', '[]', '[\"f6ff44a0ee57d6289ff43c0da9ee6780.jpg\",\"d07774e2fec74780608fb5ae5311963e.jpg\",\"6f99dfc57aaa9fcbdd3cc44c247bbbff.jpg\",\"8f9e7ba5ef81400180e2c16144afbd7d.jpg\",\"7d917818b67abe06fd6abcdcdc5687f8.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '2121212121', 230, 69, 'active', 'car', 'a9c26091b01a3eb0874e6041ec1c632e.jpg', '8aa5481d99d93e524db6db862aa80618.jpg', '', '', '1606464000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (16, '1f3200adc4e38830d962eb674d2eb782', '2012 BMW 750 Li xDrive', 'Recent Arrival! CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags, Electronic Stability Control, Exterior Parking Camera Rear, Four wheel independent suspension, Front anti-roll bar, Front Bucket Seats, Front Center Armrest, Front dual zone A/C, Front reading lights, Fully automatic headlights, Garage door transmitter: HomeLink, Heated door mirrors, Heated front seats, Heated Front Sport Bucket Seats, Illuminated entry, Leather Shift Knob, Leather steering wheel, Leatherette-Trimmed Interior, Low tire pressure warning, Memory seat, Occupant sensing airbag, Outside temperature display, Overhead airbag, Overhead console, Panic alarm, Passenger door bin, Passenger vanity mirror, Power door mirrors, Power driver seat, Power Liftgate, Power moonroof, Power passenger seat, Power steering, Power windows, Radio data system, Radio: Acura Premium Audio System, Rear anti-roll bar, Rear seat center armrest, Rear window defroster, Rear window wiper, Remote keyless entry, Security system, Speed control, Speed-sensing steering, Speed-Sensitive Wipers, Split folding rear seat, Spoiler, Steering wheel mounted audio controls, Tachometer, Telescoping steering wheel, Tilt steering wheel, Traction control, Trip computer, Turn signal indicator mirrors, Variably inte Come experience the first and only Acura dealership in the city of Chicago! Conveniently located off the Kennedy expressway at the Division exit! Our facility is equipped with 4 themed customer waiting areas, a full service cafe, and a relaxing walkway along the Chicago river! Come experience the McGrath difference!', '[\"6\",\"11\"]', '[]', '[\"817c29b631974f7d412e1101351fa7be.jpg\",\"446d9c5879c061b5bea22aaf6752f91e.jpg\",\"ba81650ebea2c9b5de18df63e1c9f605.jpg\",\"ba8183b782bd990f58d5477d6e6ea1e9.jpg\",\"9b54a1f682af1d20fee7ff104490d666.jpg\",\"109a3089eb1d67acedefa5cc54f60c7c.jpg\",\"63f28df5a3d7b0422a8be93ea38c31de.jpg\",\"004a1ed9a80751fc38bf9258b2b0915f.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '2121212121', 230, 136, 'active', 'car', '51fe443169ec49e3ad675e6203788927.jpg', 'a77074c92cc4fa2e5f72c158de7a04c0.jpg', '', '', '1606464000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (17, 'e036966e9740414f1bcb1894becbaa10', '2015 Acura RDX Technology Package', 'Recent Arrival! CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags, Electronic Stability Control, Exterior Parking Camera Rear, Four wheel independent suspension, Front anti-roll bar, Front Bucket Seats, Front Center Armrest, Front dual zone A/C, Front reading lights, Fully automatic headlights, Garage door transmitter: HomeLink, Heated door mirrors, Heated front seats, Heated Front Sport Bucket Seats, Illuminated entry, Leather Shift Knob, Leather steering wheel, Leatherette-Trimmed Interior, Low tire pressure warning, Memory seat, Occupant sensing airbag, Outside temperature display, Overhead airbag, Overhead console, Panic alarm, Passenger door bin, Passenger vanity mirror, Power door mirrors, Power driver seat, Power Liftgate, Power moonroof, Power passenger seat, Power steering, Power windows, Radio data system, Radio: Acura Premium Audio System, Rear anti-roll bar, Rear seat center armrest, Rear window defroster, Rear window wiper, Remote keyless entry, Security system, Speed control, Speed-sensing steering, Speed-Sensitive Wipers, Split folding rear seat, Spoiler, Steering wheel mounted audio controls, Tachometer, Telescoping steering wheel, Tilt steering wheel, Traction control, Trip computer, Turn signal indicator mirrors, Variably inte Come experience the first and only Acura dealership in the city of Chicago! Conveniently located off the Kennedy expressway at the Division exit! Our facility is equipped with 4 themed customer waiting areas, a full service cafe, and a relaxing walkway along the Chicago river! Come experience the McGrath difference!', '[\"6\",\"10\"]', '[]', '[\"b09251d625bf392b22d98718331298db.jpg\",\"e97c8a970f12d447c14ba0919cbcd10e.jpg\",\"205e67dddf3b0d94e0c362043a66287b.jpg\",\"3a45eb89efb1d7f02b9f509d8ccafd72.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '2121212121', 230, 189, 'active', 'car', '59a3efc0e57f47daefde9196d86152f3.jpg', '40f1814f5bb2236f10d81529bb9be0a6.jpg', '', '', '1606464000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (18, '9c5091a71c36ef3f5b423921fcbfa119', '2018 Acura RLX Technology Package', 'Recent Arrival! CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags, Electronic Stability Control, Exterior Parking Camera Rear, Four wheel independent suspension, Front anti-roll bar, Front Bucket Seats, Front Center Armrest, Front dual zone A/C, Front reading lights, Fully automatic headlights, Garage door transmitter: HomeLink, Heated door mirrors, Heated front seats, Heated Front Sport Bucket Seats, Illuminated entry, Leather Shift Knob, Leather steering wheel, Leatherette-Trimmed Interior, Low tire pressure warning, Memory seat, Occupant sensing airbag, Outside temperature display, Overhead airbag, Overhead console, Panic alarm, Passenger door bin, Passenger vanity mirror, Power door mirrors, Power driver seat, Power Liftgate, Power moonroof, Power passenger seat, Power steering, Power windows, Radio data system, Radio: Acura Premium Audio System, Rear anti-roll bar, Rear seat center armrest, Rear window defroster, Rear window wiper, Remote keyless entry, Security system, Speed control, Speed-sensing steering, Speed-Sensitive Wipers, Split folding rear seat, Spoiler, Steering wheel mounted audio controls, Tachometer, Telescoping steering wheel, Tilt steering wheel, Traction control, Trip computer, Turn signal indicator mirrors, Variably inte Come experience the first and only Acura dealership in the city of Chicago! Conveniently located off the Kennedy expressway at the Division exit! Our facility is equipped with 4 themed customer waiting areas, a full service cafe, and a relaxing walkway along the Chicago river! Come experience the McGrath difference!', '[\"6\",\"10\"]', '[]', '[\"07e6ad6defa6f257d43fe6089193685e.jpg\",\"1776c33d65895bf444ba4ee65cd2e7b5.jpg\",\"0f26380cac7bca79f37dd989eeaaee13.jpg\",\"af4c37737dc56162c3afd1f1a9eff532.jpg\",\"75246176cd9bcc0a27250a59f2db0209.jpg\",\"4acd549781b990375b81cd81c855e192.jpg\",\"3cb8826dce9a9752667646d2679fd151.jpg\",\"af748d1409b6939aad5c430287bcb141.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '2121212121', 230, 150, 'active', 'car', '977218d73448adb697b76da42eed4139.jpg', '21fd34d3a3bdf806524c49b17d7ac279.jpg', '', '', '1606464000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (19, '86b310c6c798cca59b8a4e610d972cbd', '2018 Acura TLX FWD', 'Recent Arrival! CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags, Electronic Stability Control, Exterior Parking Camera Rear, Four wheel independent suspension, Front anti-roll bar, Front Bucket Seats, Front Center Armrest, Front dual zone A/C, Front reading lights, Fully automatic headlights, Garage door transmitter: HomeLink, Heated door mirrors, Heated front seats, Heated Front Sport Bucket Seats, Illuminated entry, Leather Shift Knob, Leather steering wheel, Leatherette-Trimmed Interior, Low tire pressure warning, Memory seat, Occupant sensing airbag, Outside temperature display, Overhead airbag, Overhead console, Panic alarm, Passenger door bin, Passenger vanity mirror, Power door mirrors, Power driver seat, Power Liftgate, Power moonroof, Power passenger seat, Power steering, Power windows, Radio data system, Radio: Acura Premium Audio System, Rear anti-roll bar, Rear seat center armrest, Rear window defroster, Rear window wiper, Remote keyless entry, Security system, Speed control, Speed-sensing steering, Speed-Sensitive Wipers, Split folding rear seat, Spoiler, Steering wheel mounted audio controls, Tachometer, Telescoping steering wheel, Tilt steering wheel, Traction control, Trip computer, Turn signal indicator mirrors, Variably inte Come experience the first and only Acura dealership in the city of Chicago! Conveniently located off the Kennedy expressway at the Division exit! Our facility is equipped with 4 themed customer waiting areas, a full service cafe, and a relaxing walkway along the Chicago river! Come experience the McGrath difference!', '[\"6\",\"10\"]', '[]', '[\"f47ec96f9bbcef5b64df5b8d47463a42.jpg\",\"ec560cf0abec878b619ca015b2eeb9f2.jpg\",\"2acb3f4690dc93f6e66e3edf238837cd.jpg\",\"89d6199364424a2498380213272bbda7.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '2121212121', 230, 1, 'active', 'car', 'a69947ff588f2fd595ce3f0558419fb3.jpg', '1b2f6822c861dc06f943597c802ba3cb.jpg', '', '', '1606464000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (20, 'ef5c405a96a40b7ac248a1037259bb34', '2019 Acura RDX A-Spec', 'Recent Arrival! CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags, Electronic Stability Control, Exterior Parking Camera Rear, Four wheel independent suspension, Front anti-roll bar, Front Bucket Seats, Front Center Armrest, Front dual zone A/C, Front reading lights, Fully automatic headlights, Garage door transmitter: HomeLink, Heated door mirrors, Heated front seats, Heated Front Sport Bucket Seats, Illuminated entry, Leather Shift Knob, Leather steering wheel, Leatherette-Trimmed Interior, Low tire pressure warning, Memory seat, Occupant sensing airbag, Outside temperature display, Overhead airbag, Overhead console, Panic alarm, Passenger door bin, Passenger vanity mirror, Power door mirrors, Power driver seat, Power Liftgate, Power moonroof, Power passenger seat, Power steering, Power windows, Radio data system, Radio: Acura Premium Audio System, Rear anti-roll bar, Rear seat center armrest, Rear window defroster, Rear window wiper, Remote keyless entry, Security system, Speed control, Speed-sensing steering, Speed-Sensitive Wipers, Split folding rear seat, Spoiler, Steering wheel mounted audio controls, Tachometer, Telescoping steering wheel, Tilt steering wheel, Traction control, Trip computer, Turn signal indicator mirrors, Variably inte Come experience the first and only Acura dealership in the city of Chicago! Conveniently located off the Kennedy expressway at the Division exit! Our facility is equipped with 4 themed customer waiting areas, a full service cafe, and a relaxing walkway along the Chicago river! Come experience the McGrath difference!', '[\"6\",\"10\"]', '[]', '[\"0a950840acb50773a92257e9a9127727.jpg\",\"a42a1e82b0875644fda64305105cf8b1.jpg\",\"21c70dbe34c531f097f785d025bbb43e.jpg\",\"760976d64ecddfa05aa4b75803e8629f.jpg\",\"010f209f20358f4b9a1319f85b045eb1.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '2121212121', 230, 157, 'active', 'car', '6ee8ae4caab434a4fcff651020b03054.jpg', '04c92564fc5e37b861afe0889764ad93.jpg', '', '', '1606464000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (21, '0fcb5d6228db4f7ecc1792aa398e8515', '2018 Acura RDX Base', 'Recent Arrival! CARFAX One-Owner. Clean CARFAX. 2018 Acura RDX Lunar Silver Metallic 4D Sport Utility AWD 3.5L V6 SOHC i-VTEC 24V **LOCAL TRADE**, **ACCIDENT FREE CARFAX**, **LEATHER **, **SUNROOF**, **ONE OWNER**, **SERVICE RECORDS AVAILABLE**, **DEALER MAINTAINED**, **AWD**, **LOW MILES**, 4D Sport Utility, 3.5L V6 SOHC i-VTEC 24V, 6-Speed Automatic, AWD, Lunar Silver Metallic, Ebony w/Leatherette-Trimmed Interior, 4.25 Axle Ratio, 4-Wheel Disc Brakes, 7 Speakers, ABS brakes, Air Conditioning, Alloy wheels, AM/FM radio: SiriusXM, Anti-whiplash front head restraints, Auto-dimming Rear-View mirror, Automatic temperature control, Brake assist, Bumpers: body-color, CD player, Delay-off headlights, Driver door bin, Driver vanity mirror, Dual front impact airbags, Dual front side impact airbags, Electronic Stability Control, Exterior Parking Camera Rear, Four wheel independent suspension, Front anti-roll bar, Front Bucket Seats, Front Center Armrest, Front dual zone A/C, Front reading lights, Fully automatic headlights, Garage door transmitter: HomeLink, Heated door mirrors, Heated front seats, Heated Front Sport Bucket Seats, Illuminated entry, Leather Shift Knob, Leather steering wheel, Leatherette-Trimmed Interior, Low tire pressure warning, Memory seat, Occupant sensing airbag, Outside temperature display, Overhead airbag, Overhead console, Panic alarm, Passenger door bin, Passenger vanity mirror, Power door mirrors, Power driver seat, Power Liftgate, Power moonroof, Power passenger seat, Power steering, Power windows, Radio data system, Radio: Acura Premium Audio System, Rear anti-roll bar, Rear seat center armrest, Rear window defroster, Rear window wiper, Remote keyless entry, Security system, Speed control, Speed-sensing steering, Speed-Sensitive Wipers, Split folding rear seat, Spoiler, Steering wheel mounted audio controls, Tachometer, Telescoping steering wheel, Tilt steering wheel, Traction control, Trip computer, Turn signal indicator mirrors, Variably inte Come experience the first and only Acura dealership in the city of Chicago! Conveniently located off the Kennedy expressway at the Division exit! Our facility is equipped with 4 themed customer waiting areas, a full service cafe, and a relaxing walkway along the Chicago river! Come experience the McGrath difference!', '[\"6\",\"10\"]', '[]', '[\"9ffd6a21d80e2d6b9160548d6c1afda0.jpg\",\"23a8175b988f45e34473af29f406c542.jpg\",\"3480ae2a455486716ac3ed81279c50b9.jpg\",\"86cf84b27c0848fa1bf7e96475600a8e.jpg\",\"f924cc8cc1650e072917322e808712c1.jpg\",\"8a27a73eb017af36d9a88722454eea2e.jpg\",\"8b70d97ed04d60fdfd97c1e8e5edacdf.jpg\",\"a49f917de807ab1e5fab99b55d2e039b.jpg\",\"e78c7fa838b1b471a4399604e12cc23d.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '2121212121', 230, 51, 'active', 'car', '9663092920d5766831e45d0ea3ab99a7.jpg', '4f28c16a4c33d9c685d5ac5e3117ce67.jpg', '', '', '1606464000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (22, 'bad90ec14d5aed6dfe91b3c2026978c7', 'Webb Fontaine Holding LL', 'Adjarabet is the most successful, safe and innovative gaming portal in Caucasus Region. Adjarabet offers to its customers fast payouts, secure transactions and excellent customer service.', '[\"5\",\"25\",\"46\",\"49\"]', '[]', '[]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '34.102', '-118.344', 230, 157, 'active', 'job', '0942ecbd73666fce0425727422fd98de.jpg', 'b55ca9f570a03973e77e52e721a2d70a.jpg', '', '', '1600326000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (23, '3ffaa2384b360cc8a41501947d7c13d1', 'Ameriabank CJSC', 'Addevice is a custom mobile app development company delivering solutions to startup and enterprise clients. Over 6 years of our history, we’ve successfully delivered a lot of projects and helped our clients and brands establish a strong online presence.', '[\"5\",\"28\"]', '[]', '[]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '34.102', '-118.344', 230, 157, 'active', 'job', '7e3e5ed2927b42afd027ae2940a88bc5.jpg', '260b22e80e4b165110e98e131ef0e0b4.jpg', '', '', '1600326000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (24, 'b2513e4e10a702eb06aa55af425220ee', 'Magnus', 'Adjarabet is the most successful, safe and innovative gaming portal in Caucasus Region. Adjarabet offers to its customers fast payouts, secure transactions and excellent customer service.', '[\"5\",\"26\",\"52\"]', '[]', '[]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '34.102', '-118.344', 230, 157, 'active', 'job', 'dd0b9f0a1661af01acbd240fcec5854a.jpg', '28254f14d994fd895b648df94a46513a.jpg', '', '', '1600326000', 1606464000, 0, '');
INSERT INTO `listing` VALUES (25, 'a9ed3e30b735d8f837a9e7cedfd463ac', 'Willing &amp; Able Operations LLC', 'Addevice is a custom mobile app development company delivering solutions to startup and enterprise clients. Over 6 years of our history, we’ve successfully delivered a lot of projects and helped our clients and brands establish a strong online presence.', '[\"5\",\"25\",\"29\"]', '[]', '[]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '34.102', '-118.344', 230, 157, 'active', 'job', '58150336ccd1d17fb10e495e5bb67f0c.jpg', '1c1d27b6accea126dff266b981fd14a8.jpg', '', '', '1600326000', 1609660800, 0, '');
INSERT INTO `listing` VALUES (26, 'a8e70c37f7311e37b80788e5f5a0f519', '176 Collins St, San Francisco, CA, 94118', 'For the first time in 30 years, this rare view corner residence is available for sale! With Golden Gate Bridge, Bay and City views, the residence boasts floor-to-ceiling windows that basks the home with abundant natural light. Step outside onto the private balcony to breathe fresh air and enjoy and relax. Last remodeled 1987, this residence awaits your buyer&#039;s personal signature! Located at the intersection of Pacific Heights, Cow Hollow, Russian Hill, and Nob Hill, Jackson Towers has a 99 walk score and is a well-run HOA. Its amenities include an updated lobby, elevator bank, and an enormous shared rooftop with 360 classic San Francisco views.', '[\"7\",\"21\"]', '[]', '[\"8a984e3a5e5992b50c8e7311814303bd.jpg\",\"1bd4612892e40ca018a7b0d18000a208.jpg\",\"2ac9f3a82a52fe07a5e18a96b3263791.jpg\",\"b004b742e4448e90006bfe08fa9cea0b.jpg\",\"1c432f7345c1d704da9b0d207a6f34f4.jpg\",\"7369278cca944518931b7724bddb3b0e.jpg\",\"f90c4fb22d57da2f88d63646babf19af.jpg\",\"8f18c30afcee9e91c0229d6bb79ccb11.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '212121212', 230, 249, 'active', 'real-estate', '284608b40e298a880aa47754ffb8d425.jpg', '754861c713ef3771d5f55a208aa339dc.jpg', '', '', '1600412400', 1606464000, 0, '');
INSERT INTO `listing` VALUES (27, 'eb81c8fcd8b776683b90016a983c6bc2', '718 Long Bridge St Apt 1203,', 'For the first time in 30 years, this rare view corner residence is available for sale! With Golden Gate Bridge, Bay and City views, the residence boasts floor-to-ceiling windows that basks the home with abundant natural light. Step outside onto the private balcony to breathe fresh air and enjoy and relax. Last remodeled 1987, this residence awaits your buyer&#039;s personal signature! Located at the intersection of Pacific Heights, Cow Hollow, Russian Hill, and Nob Hill, Jackson Towers has a 99 walk score and is a well-run HOA. Its amenities include an updated lobby, elevator bank, and an enormous shared rooftop with 360 classic San Francisco views.', '[\"7\",\"22\"]', '[]', '[\"f9b1955bbdd751bf0ebfd7c22bdcb029.jpg\",\"e72c75153a282d831cbe465513cc45b4.jpg\",\"7beea46563eec08f16fa1d56b7ac1026.jpg\",\"c243e0c6dc04383d8e214d1889a4ea8d.jpg\",\"a275d50cd3e823e24bbe80b6b5acbc22.jpg\",\"2d206d79098cd3b7dc0938e66fe73f4e.jpg\",\"24a220bfce620bcebe0454a7bef0bdc4.jpg\",\"e0bf63d9e04e09452f5ed39877d1856f.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '212121212', 230, 249, 'active', 'real-estate', '6629b961326fd6eec9f2e370e8bc998d.jpg', '5f7a0ba44ccc98cb66b23009bfc8ba6f.jpg', '', '', '1600412400', 1606464000, 0, '');
INSERT INTO `listing` VALUES (28, 'b1e540467318597774f5fbc3c23bd820', '25-27 Moulton St, San Francisco, CA, 94123', 'For the first time in 30 years, this rare view corner residence is available for sale! With Golden Gate Bridge, Bay and City views, the residence boasts floor-to-ceiling windows that basks the home with abundant natural light. Step outside onto the private balcony to breathe fresh air and enjoy and relax. Last remodeled 1987, this residence awaits your buyer&#039;s personal signature! Located at the intersection of Pacific Heights, Cow Hollow, Russian Hill, and Nob Hill, Jackson Towers has a 99 walk score and is a well-run HOA. Its amenities include an updated lobby, elevator bank, and an enormous shared rooftop with 360 classic San Francisco views.', '[\"7\",\"20\"]', '[]', '[\"aae32795e073b57ed4d3af485cb84952.jpg\",\"2ec83f81a02b1a54fa85f756e32ae028.jpg\",\"8042512815045ce4e8b891405ddac077.jpg\",\"d45c7d785ce29364bb67b2252311e7e9.jpg\",\"35c8b66f079b56fa768d20a2f9645589.jpg\",\"6eb2c36ba0f2f671732b81f5d0fbc449.jpg\",\"2d9af736781bf6c1a78f3447335c448a.jpg\",\"c316e1ff07dc0b9ca1172639ac591de5.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '212121212', 230, 244, 'active', 'real-estate', '750db36cabee59ea7d6da3d19867a8fa.jpg', '84170188fc416d4beaebf5e59dccbb4e.jpg', '', '', '1600412400', 1606464000, 0, '');
INSERT INTO `listing` VALUES (29, 'df15a97f88ec37e233f3191d28e517ab', '77 E 56th St, Brooklyn, NY, 11203', 'For the first time in 30 years, this rare view corner residence is available for sale! With Golden Gate Bridge, Bay and City views, the residence boasts floor-to-ceiling windows that basks the home with abundant natural light. Step outside onto the private balcony to breathe fresh air and enjoy and relax. Last remodeled 1987, this residence awaits your buyer&#039;s personal signature! Located at the intersection of Pacific Heights, Cow Hollow, Russian Hill, and Nob Hill, Jackson Towers has a 99 walk score and is a well-run HOA. Its amenities include an updated lobby, elevator bank, and an enormous shared rooftop with 360 classic San Francisco views.', '[\"7\",\"23\"]', '[]', '[\"8adfeb68ce250140ab1686def882fe20.jpg\",\"196a6673a5e6e5f69aef53ea6dc71f13.jpg\",\"031d806ac59b8d15f18b65cda2578cff.jpg\",\"bec7fcd57fa24b8dcaa890c79c915c6d.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '212121212', 230, 189, 'active', 'real-estate', 'eaad65c75cee7e418a5593f323ae9b08.jpg', '0d66aecb945a369b811038ea8b43f0f2.jpg', '', '', '1600412400', 1606464000, 0, '');
INSERT INTO `listing` VALUES (30, '949318929dab0f350f9026741fb3b7d0', '3041 9th Ave, Los Angeles, CA, 90018', 'For the first time in 30 years, this rare view corner residence is available for sale! With Golden Gate Bridge, Bay and City views, the residence boasts floor-to-ceiling windows that basks the home with abundant natural light. Step outside onto the private balcony to breathe fresh air and enjoy and relax. Last remodeled 1987, this residence awaits your buyer&#039;s personal signature! Located at the intersection of Pacific Heights, Cow Hollow, Russian Hill, and Nob Hill, Jackson Towers has a 99 walk score and is a well-run HOA. Its amenities include an updated lobby, elevator bank, and an enormous shared rooftop with 360 classic San Francisco views.', '[\"7\",\"21\"]', '[]', '[\"a8000cdfccaf847649739763c7c2c14a.jpg\",\"fb48b8387fbcaaa98f9ba8b642545c3c.jpg\",\"3605d91c5b9d8dc26e362ba7c04ac4fe.jpg\",\"c2daca1c3b2db5049723ceac5df6aaa4.jpg\",\"0792ae448e27358c9c28773351167730.jpg\",\"d9cba676fbb3b0cf675673924c723265.jpg\",\"3ebc40ce5708794cf11df1a2cb63bdb9.jpg\",\"94ee8db98263b9dbf9428de2224ba727.jpg\"]', '', 'youtube', NULL, '', '', '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 1, '12121', '212121212', 230, 157, 'active', 'real-estate', 'a54fb457fe119bd7e68877e014fc3dd5.jpg', '977aa39006eae456774356183b7364ae.jpg', '', '', '1600412400', 1606464000, 0, '');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int NULL DEFAULT NULL,
  `listing_id` int NULL DEFAULT NULL,
  `listing_owner_id` int NULL DEFAULT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `delivery_address` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `delivery_contact` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `note` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `delivery_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `payment_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `total_amount` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `order_placed_at` int NULL DEFAULT NULL,
  `order_delivered_at` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for order_details
-- ----------------------------
DROP TABLE IF EXISTS `order_details`;
CREATE TABLE `order_details`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `inventory_id` int NULL DEFAULT NULL,
  `quantity` int NULL DEFAULT NULL,
  `total_amount` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of order_details
-- ----------------------------

-- ----------------------------
-- Table structure for package
-- ----------------------------
DROP TABLE IF EXISTS `package`;
CREATE TABLE `package`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `price` int NULL DEFAULT NULL,
  `validity` int NOT NULL DEFAULT 0 COMMENT 'validity should be in days',
  `ability_to_add_video` int NOT NULL DEFAULT 0,
  `ability_to_add_contact_form` int NOT NULL DEFAULT 0,
  `number_of_photos` int NOT NULL DEFAULT 0,
  `number_of_tags` int NOT NULL DEFAULT 0,
  `number_of_categories` int NOT NULL DEFAULT 0,
  `is_recommended` int NOT NULL DEFAULT 0,
  `package_type` int NOT NULL DEFAULT 0,
  `number_of_listings` int NOT NULL DEFAULT 0,
  `featured` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of package
-- ----------------------------
INSERT INTO `package` VALUES (1, 'free', 0, 30, 0, 0, 5, 8, 5, 0, 0, 3, 0);
INSERT INTO `package` VALUES (2, 'premium', 10, 60, 1, 1, 20, 25, 20, 1, 1, 10, 1);
INSERT INTO `package` VALUES (3, 'ultimate', 20, 120, 1, 1, 30, 30, 30, 0, 1, 25, 1);

-- ----------------------------
-- Table structure for package_purchased_history
-- ----------------------------
DROP TABLE IF EXISTS `package_purchased_history`;
CREATE TABLE `package_purchased_history`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `package_id` int NULL DEFAULT NULL,
  `user_id` int NULL DEFAULT NULL,
  `expired_date` int NULL DEFAULT NULL,
  `amount_paid` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `purchase_date` int NULL DEFAULT NULL,
  `payment_method` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of package_purchased_history
-- ----------------------------
INSERT INTO `package_purchased_history` VALUES (1, 1, 3, 1612252800, '0', 1609660800, 'free');

-- ----------------------------
-- Table structure for product_details
-- ----------------------------
DROP TABLE IF EXISTS `product_details`;
CREATE TABLE `product_details`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `listing_id` int NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `variant` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `photo` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product_details
-- ----------------------------
INSERT INTO `product_details` VALUES (1, 1, 'prduct 1', 'Variants 1,Variants 2,Variants 3', '101', '12b7d5df8e470df1567e1956aa0f0fff.jpg');
INSERT INTO `product_details` VALUES (2, 1, 'prduct 2', 'Variant 4', '205', '4a6891456bb69ed86a12387158591627.jpg');

-- ----------------------------
-- Table structure for real_estate_details
-- ----------------------------
DROP TABLE IF EXISTS `real_estate_details`;
CREATE TABLE `real_estate_details`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `listing_id` int NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `property_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `style` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `year_built` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `variant` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `photo` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of real_estate_details
-- ----------------------------
INSERT INTO `real_estate_details` VALUES (1, 7, '1200 California St Unit 15A, San Francisco, CA, 94109', 'Architecturally significant + gracious Edwardian home with stunning curb appeal. Classic high ceilings with an expansive ideal floor plan. The 2nd floor is the consummate configuration for life as a family; 3 spacious bedrooms/ 2 baths are found on this floor. A stately main level with large formal living and dining rooms that open to the ultimate renovated, well-appointed kitchen. At the rear is a light infused den lined with built-in bookshelves. Walk through the den to a huge deck. Deck can accommodate a large outdoor table plus a lounge area. This floor is completed by a powder room. On the garage level is a very large finished bonus room. Ideal for a playroom, fitness room, media room or guest quarters. Grassy garden on ground level. 3-car parking (!) with internal access. Prime Inner Sunset location. Sixth Ave is one of the highly desirable Inner Sunset blocks (no power lines overhead!). Steps from Irving Street, Golden Gate Park, Sutro Trails, UCSF, N-Judah and lots more!', '2,395,000', 'Multi-Family Home', 'Spanish/med', '1938', 'home,house,real estate', '19c8ed5e840e3ed9818e3580f12ba5e2.jpg');
INSERT INTO `real_estate_details` VALUES (2, 26, '176 Collins St, San Francisco, CA, 94118', 'Architecturally significant + gracious Edwardian home with stunning curb appeal. Classic high ceilings with an expansive ideal floor plan. The 2nd floor is the consummate configuration for life as a family; 3 spacious bedrooms/ 2 baths are found on this floor. A stately main level with large formal living and dining rooms that open to the ultimate renovated, well-appointed kitchen. At the rear is a light infused den lined with built-in bookshelves. Walk through the den to a huge deck. Deck can accommodate a large outdoor table plus a lounge area. This floor is completed by a powder room. On the garage level is a very large finished bonus room. Ideal for a playroom, fitness room, media room or guest quarters. Grassy garden on ground level. 3-car parking (!) with internal access. Prime Inner Sunset location. Sixth Ave is one of the highly desirable Inner Sunset blocks (no power lines overhead!). Steps from Irving Street, Golden Gate Park, Sutro Trails, UCSF, N-Judah and lots more!', '1,590,000', 'Multi-Family Home', 'Spanish/med', '1948', 'home,house,real estate', '329dc9ed7fd7af2cd8f52dc07d54c713.jpg');
INSERT INTO `real_estate_details` VALUES (3, 27, '718 Long Bridge St Apt 1203,', 'Architecturally significant + gracious Edwardian home with stunning curb appeal. Classic high ceilings with an expansive ideal floor plan. The 2nd floor is the consummate configuration for life as a family; 3 spacious bedrooms/ 2 baths are found on this floor. A stately main level with large formal living and dining rooms that open to the ultimate renovated, well-appointed kitchen. At the rear is a light infused den lined with built-in bookshelves. Walk through the den to a huge deck. Deck can accommodate a large outdoor table plus a lounge area. This floor is completed by a powder room. On the garage level is a very large finished bonus room. Ideal for a playroom, fitness room, media room or guest quarters. Grassy garden on ground level. 3-car parking (!) with internal access. Prime Inner Sunset location. Sixth Ave is one of the highly desirable Inner Sunset blocks (no power lines overhead!). Steps from Irving Street, Golden Gate Park, Sutro Trails, UCSF, N-Judah and lots more!', '2,225,000', 'Multi-Family Home', 'Spanish/med', '1950', 'home,house,real estate', '52d89436b24942a46e62198610289966.jpg');
INSERT INTO `real_estate_details` VALUES (4, 28, '25-27 Moulton St, San Francisco, CA, 94123', 'Architecturally significant + gracious Edwardian home with stunning curb appeal. Classic high ceilings with an expansive ideal floor plan. The 2nd floor is the consummate configuration for life as a family; 3 spacious bedrooms/ 2 baths are found on this floor. A stately main level with large formal living and dining rooms that open to the ultimate renovated, well-appointed kitchen. At the rear is a light infused den lined with built-in bookshelves. Walk through the den to a huge deck. Deck can accommodate a large outdoor table plus a lounge area. This floor is completed by a powder room. On the garage level is a very large finished bonus room. Ideal for a playroom, fitness room, media room or guest quarters. Grassy garden on ground level. 3-car parking (!) with internal access. Prime Inner Sunset location. Sixth Ave is one of the highly desirable Inner Sunset blocks (no power lines overhead!). Steps from Irving Street, Golden Gate Park, Sutro Trails, UCSF, N-Judah and lots more!', '1,195,000', 'Multi-Family Home', 'Spanish/med', '1960', 'home,house,real estate', 'eb3ee7dc046e006ddd2265bca87dc61f.jpg');
INSERT INTO `real_estate_details` VALUES (5, 29, '77 E 56th St, Brooklyn, NY, 11203', 'Architecturally significant + gracious Edwardian home with stunning curb appeal. Classic high ceilings with an expansive ideal floor plan. The 2nd floor is the consummate configuration for life as a family; 3 spacious bedrooms/ 2 baths are found on this floor. A stately main level with large formal living and dining rooms that open to the ultimate renovated, well-appointed kitchen. At the rear is a light infused den lined with built-in bookshelves. Walk through the den to a huge deck. Deck can accommodate a large outdoor table plus a lounge area. This floor is completed by a powder room. On the garage level is a very large finished bonus room. Ideal for a playroom, fitness room, media room or guest quarters. Grassy garden on ground level. 3-car parking (!) with internal access. Prime Inner Sunset location. Sixth Ave is one of the highly desirable Inner Sunset blocks (no power lines overhead!). Steps from Irving Street, Golden Gate Park, Sutro Trails, UCSF, N-Judah and lots more!', '1,400,000', 'Multi-Family Home', 'Spanish/med', '1961', 'home,house,real estate', '84935a6a785ff089f725e7d592123662.jpg');
INSERT INTO `real_estate_details` VALUES (6, 30, '3041 9th Ave, Los Angeles, CA, 90018', 'Architecturally significant + gracious Edwardian home with stunning curb appeal. Classic high ceilings with an expansive ideal floor plan. The 2nd floor is the consummate configuration for life as a family; 3 spacious bedrooms/ 2 baths are found on this floor. A stately main level with large formal living and dining rooms that open to the ultimate renovated, well-appointed kitchen. At the rear is a light infused den lined with built-in bookshelves. Walk through the den to a huge deck. Deck can accommodate a large outdoor table plus a lounge area. This floor is completed by a powder room. On the garage level is a very large finished bonus room. Ideal for a playroom, fitness room, media room or guest quarters. Grassy garden on ground level. 3-car parking (!) with internal access. Prime Inner Sunset location. Sixth Ave is one of the highly desirable Inner Sunset blocks (no power lines overhead!). Steps from Irving Street, Golden Gate Park, Sutro Trails, UCSF, N-Judah and lots more!', '2,500,000', 'Multi-Family Home', 'Spanish/med', '1970', 'home,house,real estate', 'eb14a95018e7a9b283cb413464402b10.jpg');

-- ----------------------------
-- Table structure for reported_listing
-- ----------------------------
DROP TABLE IF EXISTS `reported_listing`;
CREATE TABLE `reported_listing`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `listing_id` int NOT NULL DEFAULT 0,
  `reporter_id` int NOT NULL DEFAULT 0,
  `report` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `status` int NOT NULL DEFAULT 0,
  `date_added` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of reported_listing
-- ----------------------------

-- ----------------------------
-- Table structure for review
-- ----------------------------
DROP TABLE IF EXISTS `review`;
CREATE TABLE `review`  (
  `review_id` int NOT NULL AUTO_INCREMENT,
  `listing_id` int NULL DEFAULT NULL,
  `reviewer_id` int NULL DEFAULT NULL,
  `review_comment` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `review_rating` int NULL DEFAULT NULL,
  `timestamp` int NULL DEFAULT NULL,
  PRIMARY KEY (`review_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of review
-- ----------------------------
INSERT INTO `review` VALUES (1, 6, 3, 'review my', 2, 1601103600);
INSERT INTO `review` VALUES (2, 6, 3, 'review my ', 3, 1601103600);

-- ----------------------------
-- Table structure for review_wise_quality
-- ----------------------------
DROP TABLE IF EXISTS `review_wise_quality`;
CREATE TABLE `review_wise_quality`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `rating_from` float NULL DEFAULT NULL,
  `rating_to` float NULL DEFAULT NULL,
  `quality` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of review_wise_quality
-- ----------------------------
INSERT INTO `review_wise_quality` VALUES (1, 1, 1.5, 'Bad!!');
INSERT INTO `review_wise_quality` VALUES (2, 1.6, 2.8, 'Not Bad');
INSERT INTO `review_wise_quality` VALUES (3, 2.9, 3.4, 'So So');
INSERT INTO `review_wise_quality` VALUES (4, 3.5, 4.5, 'Good');
INSERT INTO `review_wise_quality` VALUES (5, 4.6, 5, 'Awesome');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, 'Admin');
INSERT INTO `role` VALUES (2, 'User');

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES (1, 'website_title', 'Armlocal');
INSERT INTO `settings` VALUES (2, 'system_title', 'ATLAS Directory Listing CMS');
INSERT INTO `settings` VALUES (4, 'system_email', 'creativeitem@example.com');
INSERT INTO `settings` VALUES (5, 'address', 'New York');
INSERT INTO `settings` VALUES (6, 'phone', '1234567890');
INSERT INTO `settings` VALUES (7, 'vat_percentage', NULL);
INSERT INTO `settings` VALUES (8, 'country_id', '230');
INSERT INTO `settings` VALUES (9, 'text_align', NULL);
INSERT INTO `settings` VALUES (10, 'currency_position', 'left');
INSERT INTO `settings` VALUES (11, 'language', 'english');
INSERT INTO `settings` VALUES (12, 'purchase_code', 'a06a8c56-26ab-4caa-94ea-802a5939d328');
INSERT INTO `settings` VALUES (13, 'timezone', 'America/Los_Angeles');
INSERT INTO `settings` VALUES (14, 'paypal', '[{\"active\":\"1\",\"mode\":\"sandbox\",\"sandbox_client_id\":\"AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R\",\"production_client_id\":\"1234\"}]');
INSERT INTO `settings` VALUES (15, 'stripe', '[{\"active\":\"1\",\"testmode\":\"on\",\"public_key\":\"pk_test_c6VvBEbwHFdulFZ62q1IQrar\",\"secret_key\":\"sk_test_9IMkiM6Ykxr1LCe2dJ3PgaxS\",\"public_live_key\":\"pk_live_xxxxxxxxxxxxxxxxxxxxxxxx\",\"secret_live_key\":\"sk_live_xxxxxxxxxxxxxxxxxxxxxxxx\"}]');
INSERT INTO `settings` VALUES (16, 'recaptcha_site_key', '6LfV-p4UAAAAANQN9JwL1vDTH7D1FFEpqOkCr3HU');
INSERT INTO `settings` VALUES (17, 'recapthca_secret_key', '6LfV-p4UAAAAALgG78gd9FeeNoFr4i2n4km86lyc');
INSERT INTO `settings` VALUES (18, 'system_currency', 'USD');
INSERT INTO `settings` VALUES (19, 'paypal_currency', 'USD');
INSERT INTO `settings` VALUES (20, 'stripe_currency', 'USD');
INSERT INTO `settings` VALUES (21, 'youtube_api_key', '');
INSERT INTO `settings` VALUES (22, 'vimeo_api_key', '');
INSERT INTO `settings` VALUES (23, 'protocol', 'smtp');
INSERT INTO `settings` VALUES (24, 'smtp_host', 'ssl://smtp.googlemail.com');
INSERT INTO `settings` VALUES (25, 'smtp_port', '465');
INSERT INTO `settings` VALUES (26, 'smtp_user', 'admin@example.com');
INSERT INTO `settings` VALUES (27, 'smtp_pass', 'xxxxxxxxxx');
INSERT INTO `settings` VALUES (28, 'social_links', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}');
INSERT INTO `settings` VALUES (29, 'about', '');
INSERT INTO `settings` VALUES (30, 'term_and_condition', '');
INSERT INTO `settings` VALUES (31, 'privacy_policy', '');
INSERT INTO `settings` VALUES (32, 'faq', '');
INSERT INTO `settings` VALUES (35, 'footer_text', 'Creativeitem');
INSERT INTO `settings` VALUES (36, 'footer_link', 'http://creativeitem.com/');
INSERT INTO `settings` VALUES (37, 'version', '2.5');
INSERT INTO `settings` VALUES (38, 'meta_keyword', 'business');
INSERT INTO `settings` VALUES (39, 'meta_description', 'Atlas business directory listing');
INSERT INTO `settings` VALUES (40, 'map_access_token', 'pk.eyJ1IjoiYXJtbG9jYWwiLCJhIjoiY2tpYzhiaXpsMDUycjJ2bncyemYzZWpqeSJ9.Rv6E4xFyXjOGdNvSqxDcyw');
INSERT INTO `settings` VALUES (41, 'max_zoom_level', '18');
INSERT INTO `settings` VALUES (42, 'min_zoom_listings_page', '2');
INSERT INTO `settings` VALUES (43, 'min_zoom_directory_page', '2');
INSERT INTO `settings` VALUES (44, 'default_location', '34.05559220391327, -118.24964520664366');
INSERT INTO `settings` VALUES (48, 'recaptcha_sitekey', 'RECAPTCHA_SITEKEY');
INSERT INTO `settings` VALUES (49, 'recaptcha_secretkey', 'RECAPTCHA_SECRETKEY');

-- ----------------------------
-- Table structure for shopping_cart
-- ----------------------------
DROP TABLE IF EXISTS `shopping_cart`;
CREATE TABLE `shopping_cart`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `listing_id` int NULL DEFAULT NULL,
  `inventory_id` int NULL DEFAULT NULL,
  `quantity` int NULL DEFAULT NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of shopping_cart
-- ----------------------------

-- ----------------------------
-- Table structure for time_configuration
-- ----------------------------
DROP TABLE IF EXISTS `time_configuration`;
CREATE TABLE `time_configuration`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `listing_id` int NULL DEFAULT NULL,
  `saturday` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `sunday` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `monday` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `tuesday` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `wednesday` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `thursday` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `friday` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of time_configuration
-- ----------------------------
INSERT INTO `time_configuration` VALUES (1, 1, 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed');
INSERT INTO `time_configuration` VALUES (2, 2, '0-5', '3-5', 'closed-closed', '4-5', 'closed-closed', 'closed-closed', 'closed-closed');
INSERT INTO `time_configuration` VALUES (3, 3, 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', '4-23');
INSERT INTO `time_configuration` VALUES (4, 4, '0-1', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed');
INSERT INTO `time_configuration` VALUES (5, 5, 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed');
INSERT INTO `time_configuration` VALUES (6, 6, '-', '-', '-', '-', '-', '-', '-');
INSERT INTO `time_configuration` VALUES (7, 7, 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed');
INSERT INTO `time_configuration` VALUES (8, 8, '0-0', '0-0', '0-0', '0-0', '0-0', '0-0', '0-0');
INSERT INTO `time_configuration` VALUES (9, 9, 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed');
INSERT INTO `time_configuration` VALUES (10, 9, 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed');
INSERT INTO `time_configuration` VALUES (11, 10, 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed', 'closed-closed');
INSERT INTO `time_configuration` VALUES (12, 0, '-', '-', '-', '-', '-', '-', '-');
INSERT INTO `time_configuration` VALUES (13, 0, '-', '-', '-', '-', '-', '-', '-');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `website` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `social` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `about` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `role_id` int NULL DEFAULT NULL,
  `wishlists` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `verification_code` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `is_verified` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'Admin', 'admin@gmail.com', NULL, '', '', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 'd033e22ae348aeb5660fc2140aec35850c4da997', 1, '[\"6\"]', NULL, 1);
INSERT INTO `user` VALUES (2, 'Volodya', 'signup@gmail.com', 'Address', '222222', NULL, '{\"facebook\":null,\"twitter\":null,\"linkedin\":null}', NULL, '4e3d91444d2d47e7ed5711fdce33fc7928a8a7f9', 2, '[]', '804bd4d24e77334ab4b1fa32ccc4e9e9', 1);
INSERT INTO `user` VALUES (3, 'Business Man 1', 'user@gmail.com', 'addd', 'ph', 'Web', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', 'Ab', '12dea96fec20593566ab75692c9949596833adc9', 2, '[]', 'b5d30f6d5dc9cb23120a3d09d653f4d1', 1);

SET FOREIGN_KEY_CHECKS = 1;
