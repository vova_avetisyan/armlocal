<?php
$countries  = $this->db->get('country')->result_array();
//$categories = $this->db->get('category')->result_array();
$categories = $this->crud_model->get_sub_categories($category_id)->result_array();
?>
<div class="row">
    <div class="col-sm-4">
        <div class="tile-title tile-primary">
            <div class="icon">
                <i class="glyphicon glyphicon-lock"></i>
            </div>
            <div class="title">
                <h3>
                    <?php
                    $user_id = $this->session->userdata('user_id');
                    $this->db->where('user_id', $user_id);
                    $this->db->order_by('id', 'desc')->limit(1);
                    $package_id = $this->db->get_where('package_purchased_history')->row('package_id');
                    $total_listing = $this->db->get_where('package', array('id' => $package_id))->row('number_of_listings');
                    echo $total_listing;
                    ?>
                </h3>
                <p>
                    <?php echo get_phrase('total_capacity'); ?>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="tile-title tile-red">
            <div class="icon">
                <i class="glyphicon glyphicon-link"></i>
            </div>
            <div class="title">
                <h3>
                    <?php
                    $submited_listing = 0;
                    $submited_listings = $this->db->get_where('listing', array('user_id'=> $user_id))->result_array();
                    foreach($submited_listings as $row){
                        $submited_listing++;
                    }
                    echo $submited_listing;
                    ?>
                </h3>
                <p>
                    <?php echo get_phrase('already_submited'); ?>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="tile-title tile-blue">
            <div class="icon">
                <i class="entypo-paper-plane"></i>
            </div>
            <div class="title">
                <h3>
                    <?php
                    if($total_listing < $submited_listing){
                        echo 0;
                    }else{
                        echo $free_space = $total_listing - $submited_listing;
                    }
                    ?>
                </h3>
                <p>
                    <?php echo get_phrase('free_space'); ?>
                </p>
            </div>
        </div>
    </div>
</div>

<?php if($total_listing > $submited_listing){  ?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">
					<?php echo get_phrase('add_listing_form'); ?>
				</div>
			</div>
			<div class="panel-body">
				<form action="<?php echo site_url('user/listings/add?type=real-estate'); ?>" method="post" enctype="multipart/form-data" role="form" class="form-horizontal form-groups-bordered listing_add_form">
                    <input type="hidden" name="total_listing" value="<?php echo $total_listing; ?>">
                    <input type="hidden" name="submited_listing" value="<?php echo $submited_listing; ?>">

                    <div class="col-md-12">
                        <ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
                            <li class="active">
                                <a href="#eighth" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-cog"></i></span>
                                    <span class="hidden-xs"><?php echo get_phrase('type'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="#first" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-home"></i></span>
                                    <span class="hidden-xs"><?php echo get_phrase('basic'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="#second" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-location"></i></span>
                                    <span class="hidden-xs"><?php echo get_phrase('location'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="#fourth" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-video"></i></span>
                                    <span class="hidden-xs"><?php echo get_phrase('media'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="#sixth" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-cog"></i></span>
                                    <span class="hidden-xs"><?php echo get_phrase('schedule'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="#seventh" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-link"></i></span>
                                    <span class="hidden-xs"><?php echo get_phrase('contact'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="#ninth" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-check"></i></span>
                                    <span class="hidden-xs"><?php echo get_phrase('finish'); ?></span>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="eighth">
                                <?php include 'add_listing_type_real_estate.php'; ?>

                                <?php echo prev_next(false); ?>
                            </div>

                            <div class="tab-pane" id="first">
                                <?php include 'add_listing_basic_real_estate.php'; ?>

                                <?php echo prev_next(); ?>
                            </div>

                            <div class="tab-pane" id="second">
                                <?php include 'add_listing_location.php'; ?>

                                <?php echo prev_next(); ?>
                            </div>

                            <div class="tab-pane" id="third">
                                <?php include 'add_listing_amenity.php'; ?>

                                <?php echo prev_next(); ?>
                            </div>

                            <div class="tab-pane" id="fourth">
                                <?php include 'add_listing_media.php'; ?>

                                <?php echo prev_next(); ?>
                            </div>

                            <div class="tab-pane" id="sixth">
                                <?php include 'add_listing_schedule.php'; ?>

                                <?php echo prev_next(); ?>
                            </div>

                            <div class="tab-pane" id="seventh">
                                <?php include 'add_listing_contact.php'; ?>

                                <?php echo prev_next(); ?>
                            </div>

                            <div class="tab-pane" id="ninth">
                                <?php include 'add_listing_finish.php'; ?>
                            </div>
                        </div>
				    </div>
				</form>
			</div>
		</div>
	</div><!-- end col-->
</div>
<?php }elseif($total_listing < $submited_listing){ ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <?php echo get_phrase('oops'); ?>
                    </div>
                </div>
                <div class="panel-body text-center">
                    <img src="<?php echo base_url('assets/frontend/images/file-searching.svg'); ?>" height="90" alt="File not found Image">

                    <h4 class="text-uppercase text-danger mt-3"><?php echo get_phrase('listing').' '.$total_listing.' / '.$submited_listing; ?></h4>
                    <h5 class="mt-3 text-danger ">The list ability of your current package is <?php echo $total_listing ?>, so all your listings will not be displayed on the home page. Buy a upper level package, to show on all the listing home pages.</h5>

                    <a class="btn btn-primary mt-3" href="<?php echo site_url('user/packages'); ?>"><i class="mdi mdi-reply"></i><?php echo get_phrase('purchase_package'); ?></a></a>
                </div>
            </div>
        </div><!-- end col-->
    </div>
<?php }else{ ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <?php echo get_phrase('oops'); ?>
                    </div>
                </div>
                <div class="panel-body text-center">
                    <img src="<?php echo base_url('assets/frontend/images/file-searching.svg'); ?>" height="90" alt="File not found Image">

                    <h4 class="text-uppercase text-danger mt-3"><?php echo get_phrase('listing').' '.$total_listing.' / '.$submited_listing; ?></h4>
                    <p class="text-muted mt-3">There is no free space add to the listing, for that you have to buy a upper level package.</p>

                    <a class="btn btn-primary mt-3" href="<?php echo site_url('user/packages'); ?>"><i class="mdi mdi-reply"></i><?php echo get_phrase('purchase_package'); ?></a></a>
                </div>
            </div>
        </div><!-- end col-->
    </div>
<?php } ?>

<script type="text/javascript">
function getCityList(country_id) {
	$.ajax({
		type : 'POST',
		url : '<?php echo site_url('home/get_city_list_by_country_id'); ?>',
		data : {country_id : country_id},
		success : function(response) {
			$('#city_id').html(response);
		}
	});
}
var blank_category = $('#blank_category_field').html();
var blank_photo_uploader = $('#blank_photo_uploader').html();
var blank_real_estate_div = $('#blank_real_estate_div').html();
var listing_type_value = $('.listing-type-radio').val();

$(document).ready(function() {
	$('#blank_category_field').hide();
	$('#blank_photo_uploader').hide();
    $('#blank_real_estate_div').hide();
    showListingTypeForm('real_estate');
});

function appendRealEstate() {
    jQuery('#real_estate_div').append(blank_real_estate_div);
    let selector = jQuery('#real_estate_div .real_estate_div');

    let rand = Math.random().toString(36).slice(3);

    $(selector[selector.length - 1]).find('label.btn').attr('for', 'real-estate-image-' + rand );
    $(selector[selector.length - 1]).find('input.image-upload').attr('id', 'real-estate-image-' + rand );
    $(".bootstrap-tag-input").tagsinput('items');
    initImagePreviewer();
}

function removeRealEstate(elem) {
    jQuery(elem).closest('.real_estate_div').remove();
    $(".bootstrap-tag-input").tagsinput('items');
}

function appendCategory() {
	jQuery('#category_area').append(blank_category);
}

function removeCategory(categoryElem) {
	jQuery(categoryElem).closest('.appendedCategoryFields').remove();
}

function appendPhotoUploader() {
	jQuery('#photos_area').append(blank_photo_uploader);
}

function removePhotoUploader(photoElem) {
	jQuery(photoElem).closest('.appendedPhotoUploader').remove();
}

function showListingTypeForm(listing_type) {
	listing_type_value = listing_type;

    $('#real_estate_parent_div').show();
}

// This fucntion checks the minimul required fields of listing form
function checkMinimumFieldRequired() {
	var title = $('#title').val();
	var defaultCategory = $('#category_default').val();
	var latitude = $('#latitude').val();
	var longitude = $('#longitude').val();
	if (title === "" || defaultCategory === "" || latitude === "" || longitude === "") {
		error_notify('<?php echo get_phrase('listing_title').', '.get_phrase('listing_category').', '.get_phrase('latitude').', '.get_phrase('longitude').' '.get_phrase('can_not_be_empty'); ?>');
	}else {
		$('.listing_add_form').submit();
	}
}
</script>

<script>
	function service_time(){
		var starting_time = $('#starting_time').val();
		var ending_time = $('#starting_time').val();
		if(starting_time != '' && ending_time != ''){
			$("#ending_time").attr("min", starting_time);
			$("#ending_time").attr("max", "24:00");
		}
	}

	// function service_time_min(){
	// 	var ending_time   = $('#ending_time').val();
	// 	if(ending_time != ''){
	// 		$("#ending_time").attr("min", "1");
	// 		$("#ending_time").attr("max", ending_time);
	// 	}
	// }
</script>