<div id="job_parent_div" style="display: none; padding-top: 10px;">
  <div id = "job_div">
    <div class="job_div">
      <div class="row">
        <div class="col-lg-10">
          <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-body">
              <div class="collapse show" style="padding-top: 10px;">
                <div class="row no-margin">

                    <!-- Media -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo get_phrase('listing_thumbnail'); ?> <br/> <small>(460 X 306)</small> </label>
                        <div class="col-sm-7">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;" data-trigger="fileinput">
                                    <img src="<?php echo base_url('uploads/placeholder.png'); ?>" alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                <div>
                                    <span class="btn btn-white btn-file">
                                      <span class="fileinput-new"><?php echo get_phrase('select_image'); ?></span>
                                      <span class="fileinput-exists"><?php echo get_phrase('change'); ?></span>
                                      <input type="file" name="listing_thumbnail" accept="image/*">
                                    </span>
                                    <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput"><?php echo get_phrase('remove'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo get_phrase('listing_cover'); ?> <br/> <small>(1600 X 600)</small> </label>
                        <div class="col-sm-7">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;" data-trigger="fileinput">
                                    <img src="<?php echo base_url('uploads/placeholder.png'); ?>" alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                <div>
                                    <span class="btn btn-white btn-file">
                                      <span class="fileinput-new"><?php echo get_phrase('select_image'); ?></span>
                                      <span class="fileinput-exists"><?php echo get_phrase('change'); ?></span>
                                      <input type="file" name="listing_cover" accept="image/*">
                                    </span>
                                    <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput"><?php echo get_phrase('remove'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Form -->
                  <div class="col-lg-9">
                      <div class="form-group">
                          <label for="title"><?php echo get_phrase('title'); ?></label>
                          <input type="text" name="title" class="form-control" id="title" required>
                      </div>

                      <div class="form-group">
                          <label for="employment_term"><?php echo get_phrase('employment_term'); ?></label>
                          <select name="employment_term[]" class="selectboxit">
                              <option value="Permanent">Permanent</option>
                              <option value="Temporary">Temporary</option>
                              <option value="Freelance">Freelance</option>
                              <option value="Contract">Contract</option>
                              <option value="Internship">Internship</option>
                              <option value="Other">Other</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="job_type"><?php echo get_phrase('job_type'); ?></label>
                          <select name="job_type[]" class="selectboxit">
                              <option value="Full time">Full time</option>
                              <option value="Part time">Part time</option>
                              <option value="Training">Training</option>
                              <option value="Fixed term contract">Fixed term contract</option>
                              <option value="Tender">Tender</option>
                              <option value="Internship">Internship</option>
                              <option value="Remote">Remote</option>
                              <option value="Other">Other</option>
                          </select>
                      </div>
                  </div>

                    <!-- Deadline -->
                    <div class="col-lg-5">
                        <div class="form-group job_deadline">
                            <label style="margin-bottom: 10px"><?php echo get_phrase('deadline'); ?></label>
                            <input type="date" name="deadline" value="" class="form-control">
                        </div>
                    </div>

                    <!-- Category -->
                    <div class="col-lg-9">
                        <div class="form-group">
                            <label for="category"> <?php echo get_phrase('job_category'); ?></label>
                            <div id="category_area">
                                <select name="categories[]" style="display: none;">
                                    <option value="<?php echo $category_id; ?>"></option>
                                </select>

                                <div class="row">
                                    <div class="col-sm-7">
                                        <select class="form-control select2" name="categories[]" id = "category_default" required>
                                            <option value=""><?php echo get_phrase('select_category'); ?></option>
                                            <?php foreach ($categories as $category): ?>
                                                <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="button" class="btn btn-primary btn-sm" style="margin-top: 5px; float: right;" name="button" onclick="appendCategory()"> <i class="fa fa-plus"></i> </button>
                                    </div>
                                </div>
                            </div>

                            <div id="blank_category_field">
                                <div class="row appendedCategoryFields" style="margin-top: 10px;">
                                    <div class="col-sm-7 pr-0">
                                        <select class="form-control" name="categories[]">
                                            <option value=""><?php echo get_phrase('select_category'); ?></option>
                                            <?php foreach ($categories as $category): ?>
                                                <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="button" class="btn btn-danger btn-sm" style="margin-top: 5px; float: right;" name="button" onclick="removeCategory(this)"> <i class="fa fa-minus"></i> </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-9">
                      <div class="form-group">
                          <label for="job_description"><?php echo get_phrase('description'); ?></label>
                          <textarea name="job_description[]" class="form-control ckeditor" rows="10"></textarea>
                      </div>
                      <div class="form-group">
                          <label for="job_responsibilities"><?php echo get_phrase('job_responsibilities'); ?></label>
                          <textarea name="job_responsibilities[]" class="form-control ckeditor" rows="10"></textarea>
                      </div>
                      <div class="form-group">
                          <label for="required_qualifications"><?php echo get_phrase('required_qualifications'); ?></label>
                          <textarea name="required_qualifications[]" class="form-control ckeditor" rows="10"></textarea>
                      </div>
                      <div class="form-group">
                          <label for="job_price"><?php echo get_phrase('salary').' ('.currency_code_and_symbol().')'; ?></label>
                          <input type="text" name="job_price[]" class="form-control" />
                      </div>

                    <div class="form-group">
                      <label for="job_variants"><?php echo get_phrase('skills'); ?> <small>(<?php echo get_phrase('press_Enter_after_entering_every_skill'); ?>)</small></label>
                      <input type="text" class="form-control bootstrap-tag-input" name="job_variants[]" data-role="tagsinput"/>
                    </div>

                      <!-- About company, featured -->
                      <div class="form-group">
                          <label for="description"><?php echo get_phrase('about_company'); ?> <small>(<?php echo get_phrase('about_field_is_optional'); ?>)</small></label>
                          <textarea name="description" class="form-control ckeditor" id="description" rows="8" cols="80"></textarea>
                      </div>
                      <div class="form-group">
                          <label for="featured_type"><?php echo get_phrase('featured_type'); ?></label>
                          <?php $user_id = $this->session->userdata('user_id'); ?>
                          <?php $package_id = has_package($user_id, 'package_id'); ?>
                          <?php $featured_status = $this->db->get_where('package', array('id' => $package_id['package_id']))->row('featured'); ?>
                          <select name="is_featured" id = "featured_type" class="selectboxit" <?php if($featured_status != 1) echo 'disabled'; ?> required>
                              <option value=""><?php echo get_phrase('select_featured_type'); ?></option>
                              <option value="1"><?php echo get_phrase('featured'); ?></option>
                              <option value="0"<?php if($featured_status != 1) echo 'selected'; ?>><?php echo get_phrase('none_featured'); ?></option>
                          </select>
                      </div>

                      <!-- Video -->
                      <hr>
                      <div class="form-group">
                          <label for="video_provider" class="col-sm-3 control-label"><?php echo get_phrase('video_provider'); ?></label>
                          <div class="col-sm-9">
                              <select name="video_provider" id = "video_provider" class="selectboxit" required>
                                  <option value="youtube">YouTube</option>
                                  <option value="vimeo">Vimeo</option>
                                  <option value="html5">HTML5</option>
                              </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="video_url" class="col-sm-3 control-label"><?php echo get_phrase('video_url'); ?></label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control" name="video_url" id="video_url" placeholder="<?php echo get_phrase('video_url'); ?>" required>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-4" style="display: none">
                    <div class="wrapper-image-preview">
                      <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                          <label for="job-image-1" class="btn"> <i class="entypo-camera"></i> <?php echo get_phrase('upload_job_image'); ?> <small>(200 X 200) </small> </label>
                          <input id="job-image-1" style="visibility:hidden;" type="file" class="image-upload" name="job_image[]" accept="image/*">
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row text-center" style="display: none">
    <button type="button" class="btn btn-primary" name="button" onclick="appendJob()"> <i class="mdi mdi-plus"></i> <?php echo get_phrase('add_new_job'); ?></button>
  </div>
</div>

<div id = "blank_job_div" style="display: none">
  <div class="job_div">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <div class="panel panel-primary" data-collapsed="0">
          <div class="panel-body">
            <div class="collapse show" style="padding-top: 10px;">
              <div class="row no-margin">
                <div class="col-lg-8">
                  <div class="form-group">
                      <label for="job_name"><?php echo get_phrase('name'); ?></label>
                      <input type="text" name="job_name[]" class="form-control" />
                  </div>
                    <div class="form-group">
                        <label for="employment_term"><?php echo get_phrase('employment_term'); ?></label>
                        <input type="text" name="employment_term[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="job_type"><?php echo get_phrase('job_type'); ?></label>
                        <input type="text" name="job_type[]" class="form-control" />
                    </div>
                  <div class="form-group">
                      <label for="job_description"><?php echo get_phrase('description'); ?></label>
                      <textarea name="job_description[]" class="form-control" rows="5"></textarea>
                  </div>
                    <div class="form-group">
                        <label for="job_responsibilities"><?php echo get_phrase('job_responsibilities'); ?></label>
                        <textarea name="job_responsibilities[]" class="form-control" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="required_qualifications"><?php echo get_phrase('required_qualifications'); ?></label>
                        <textarea name="required_qualifications[]" class="form-control" rows="5"></textarea>
                    </div>
                  <div class="form-group">
                      <label for="job_price"><?php echo get_phrase('salary').' ('.currency_code_and_symbol().')'; ?></label>
                      <input type="text" name="job_price[]" class="form-control" />
                  </div>

                  <div class="form-group">
                      <label for="job_variants"><?php echo get_phrase('skills'); ?> <small>(<?php echo get_phrase('press_Enter_after_entering_every_skill'); ?>)</small></label>
                      <input type="text" class="form-control bootstrap-tag-input" name="job_variants[]" data-role="tagsinput"/>
                  </div>
                </div>
                <div class="col-lg-4" style="display: none">
                  <div class="wrapper-image-preview">
                    <div class="box">
                      <div class="js--image-preview"></div>
                      <div class="upload-options">
                        <label for="" class="btn"> <i class="entypo-camera"></i> <?php echo get_phrase('upload_job_image'); ?> <small>(200 X 200) </small> </label>
                        <input id="" style="visibility:hidden;" type="file" class="image-upload" name="job_image[]" accept="image/*">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
