<?php
$listing_details = $this->crud_model->get_listings($listing_id)->row_array();
$job_deadline_details = $this->crud_model->get_job_deadline_by_listing_id($listing_id)->row_array();$social_links = json_decode($listing_details['social'], true);
$countries  = $this->db->get('country')->result_array();
//$categories = $this->db->get('category')->result_array();
$categories = $this->crud_model->get_sub_categories($category_id)->result_array();
$listing_amenities = json_decode($listing_details['amenities'], false);
$listing_categories = json_decode($listing_details['categories'], false);
?>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">
					<?php echo get_phrase('update').': '.$listing_details['name']; ?>
				</div>
			</div>
			<div class="panel-body">
				<form action="<?php echo site_url('user/listings/edit/'.$listing_id . '?type=job'); ?>" method="post" enctype="multipart/form-data" role="form" class="form-horizontal form-groups-bordered listing_edit_form">
				<div class="col-md-12">
					<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
                        <li class="active">
                            <a href="#eighth" data-toggle="tab">
                                <span class="visible-xs"><i class="entypo-cog"></i></span>
                                <span class="hidden-xs"><?php echo get_phrase('type'); ?></span>
                            </a>
                        </li>
						<li>
							<a href="#second" data-toggle="tab">
								<span class="visible-xs"><i class="entypo-user"></i></span>
								<span class="hidden-xs"><?php echo get_phrase('location'); ?></span>
							</a>
						</li>
						<li>
							<a href="#seventh" data-toggle="tab">
								<span class="visible-xs"><i class="entypo-cog"></i></span>
								<span class="hidden-xs"><?php echo get_phrase('contact'); ?></span>
							</a>
						</li>
                        <li>
                            <a href="#ninth" data-toggle="tab">
                                <span class="visible-xs"><i class="entypo-check"></i></span>
                                <span class="hidden-xs"><?php echo get_phrase('finish'); ?></span>
                            </a>
                        </li>
					</ul>

					<div class="tab-content">
                        <div class="tab-pane active" id="eighth">
                            <?php include 'edit_listing_type_job.php'; ?>

                            <?php echo prev_next(false); ?>
                        </div>

						<div class="tab-pane" id="second">
							<?php include 'edit_listing_location.php'; ?>

                            <?php echo prev_next(); ?>
						</div>

						<div class="tab-pane" id="seventh">
							<?php include 'edit_listing_contact.php'; ?>

                            <?php echo prev_next(); ?>
						</div>

                        <div class="tab-pane" id="ninth">
                          <?php include 'edit_listing_finish.php'; ?>
                        </div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div><!-- end col-->
</div>
  <script type="text/javascript">
  function getCityList(country_id) {
    $.ajax({
      type : 'POST',
      url : '<?php echo site_url('home/get_city_list_by_country_id'); ?>',
      data : {country_id : country_id},
      success : function(response) {
        $('#city_id').html(response);
      }
    });
  }
  var blank_category = $('#blank_category_field').html();
  var blank_photo_uploader = $('#blank_photo_uploader').html();
  var blank_job_div = $('#blank_job_div').html();
  var listing_type_value = $('.listing-type-radio').val();

  $(document).ready(function() {
    $('#blank_category_field').hide();
    $('#blank_photo_uploader').hide();
    $('#blank_job_div').hide();
    showListingTypeForm('<?php echo $listing_details['listing_type']; ?>');
  });

  function appendJob() {
      jQuery('#job_div').append(blank_job_div);
      let selector = jQuery('#job_div .job_div');

      let rand = Math.random().toString(36).slice(3);

      $(selector[selector.length - 1]).find('label.btn').attr('for', 'job-image-' + rand );
      $(selector[selector.length - 1]).find('input.image-upload').attr('id', 'job-image-' + rand );
      $(".bootstrap-tag-input").tagsinput('items');
      initImagePreviewer();
  }

  function removeJob(elem) {
      jQuery(elem).closest('.job_div').remove();
      $(".bootstrap-tag-input").tagsinput('items');
      removeFromDatabase('job', elem.id);
  }

  function appendCategory() {
    jQuery('#category_area').append(blank_category);
  }

  function removeCategory(categoryElem) {
    jQuery(categoryElem).closest('.appendedCategoryFields').remove();
  }

  function appendPhotoUploader() {
    jQuery('#photos_area').append(blank_photo_uploader);
  }

  function removePhotoUploader(photoElem) {
    jQuery(photoElem).closest('.appendedPhotoUploader').remove();
  }

  function showListingTypeForm(listing_type) {
  	listing_type_value = listing_type;

      $('#job_parent_div').show();
  }

  function removeFromDatabase(type, id) {
    $.ajax({
      type : 'POST',
      url : '<?php echo site_url('user/remove_listing_inner_feature'); ?>',
      data : {type : type, id : id},
      success : function(response) {
        success_notify('<?php echo get_phrase('removed_successfully'); ?>');
      }
    });
  }

  // This function checks the minimul required fields of listing form
  function checkMinimumFieldRequired() {
  	var title = $('#title').val();
  	var defaultCategory = $('#category_default').val();
  	if (title === "" || defaultCategory === "") {
  		error_notify('<?php echo get_phrase('listing_title').', '.get_phrase('listing_category').', '.get_phrase('can_not_be_empty'); ?>');
  	}else {
  		$('.listing_edit_form').submit();
  	}
  }
</script>