<div class="row" style="display: none">
  <div class="col-sm-offset-3 col-sm-3">
      <div class="col-lg-12">
          <div class="custom-control custom-radio">
              <input type="radio" id="job" name="listing_type" class="custom-control-input listing-type-radio" value="job" onclick="showListingTypeForm('job')" checked = "checked">
              <label class="custom-control-label" for="job"><i class="fa fa-hotel" style="color: #636363;"></i> <?php echo get_phrase('job'); ?></label>
          </div>
      </div>
  </div>
</div>

<?php include 'job_form_for_editing.php'; ?>
