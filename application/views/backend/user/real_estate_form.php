<div id="real_estate_parent_div" style="display: none; padding-top: 10px;">
  <div id = "real_estate_div">
    <div class="real_estate_div">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-body">
              <div class="collapse show" style="padding-top: 10px;">
                <div class="row no-margin">
                    <div class="col-lg-12">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="real_estate_name"><?php echo get_phrase('name'); ?></label>
                                <input type="text" name="real_estate_name[]" class="form-control" />
                            </div>

                            <div class="form-group">
                                <label for="status"><?php echo get_phrase('status'); ?></label>
                                <select name="status[]" class="selectboxit">
                                    <option value="for_sale">For Sale</option>
                                    <option value="waiting">Waiting</option>
                                    <option value="sold">Sold</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="real_estate_price"><?php echo get_phrase('price').' ('.currency_code_and_symbol().')'; ?></label>
                                <input type="number" name="real_estate_price[]" class="form-control" />
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="wrapper-image-preview">
                                <div class="box">
                                    <div class="js--image-preview"></div>
                                    <div class="upload-options">
                                        <label for="real-estate-image-1" class="btn"> <i class="entypo-camera"></i> <?php echo get_phrase('upload_real_estate_image'); ?> <small>(200 X 200) </small> </label>
                                        <input id="real-estate-image-1" style="visibility:hidden;" type="file" class="image-upload" name="real_estate_image[]" accept="image/*">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="real_estate_description"><?php echo get_phrase('description'); ?></label>
                            <textarea name="real_estate_description[]" class="form-control ckeditor" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <div class="form-group">
                                <label for="bed"><?php echo get_phrase('bed'); ?></label>
                                <input type="text" name="bed[]" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group col-lg-6 pull-right">
                            <div class="form-group">
                                <label for="bath"><?php echo get_phrase('bath'); ?></label>
                                <input type="text" name="bath[]" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <div class="form-group">
                                <label for="sqft"><?php echo get_phrase('sqft'); ?></label>
                                <input type="text" name="sqft[]" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group col-lg-6 pull-right">
                            <div class="form-group">
                                <label for="sqft_lot"><?php echo get_phrase('sqft_lot'); ?></label>
                                <input type="text" name="sqft_lot[]" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <div class="form-group">
                                <label for="property_type"><?php echo get_phrase('property_type'); ?></label>
                                <input type="text" name="property_type[]" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group col-lg-6 pull-right">
                            <div class="form-group">
                                <label for="style"><?php echo get_phrase('style'); ?></label>
                                <input type="text" name="style[]" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <div class="form-group">
                                <label for="year_built"><?php echo get_phrase('year_built'); ?></label>
                                <input type="text" name="year_built[]" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                          <label for="real_estate_variants"><?php echo get_phrase('tags'); ?> <small>(<?php echo get_phrase('press_Enter_after_entering_every_tag'); ?>)</small></label>
                          <input type="text" class="form-control bootstrap-tag-input" name="real_estate_variants[]" data-role="tagsinput"/>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row text-center" style="display: none">
    <button type="button" class="btn btn-primary" name="button" onclick="appendRealEstate()"> <i class="mdi mdi-plus"></i> <?php echo get_phrase('add_new_real_estate'); ?></button>
  </div>
</div>

<div id = "blank_real_estate_div">
  <div class="real_estate_div">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <div class="panel panel-primary" data-collapsed="0">
          <div class="panel-body">
            <h5 class="card-title mb-0"><?php echo get_phrase('real_estate'); ?>
              <button type="button" class="btn btn-danger btn-sm btn-rounded alignToTitleOnPreview" name="button" onclick="removeRealEstate(this)"><?php echo get_phrase('remove_this_real_estate'); ?></button>
            </h5>
            <div class="collapse show" style="padding-top: 10px;">
              <div class="row no-margin">
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="real_estate_name"><?php echo get_phrase('name'); ?></label>
                        <input type="text" name="real_estate_name[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="status"><?php echo get_phrase('status'); ?></label>
                        <select name="status[]" class="selectboxit">
                            <option value="for_sale">For Sale</option>
                            <option value="waiting">Waiting</option>
                            <option value="sold">Sold</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="real_estate_description"><?php echo get_phrase('description'); ?></label>
                        <textarea name="real_estate_description[]" class="form-control" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="real_estate_price"><?php echo get_phrase('price').' ('.currency_code_and_symbol().')'; ?></label>
                        <input type="text" name="real_estate_price[]" class="form-control" />
                    </div>
                    <div class="form-group col-lg-6">
                        <div class="form-group">
                            <label for="bed"><?php echo get_phrase('bed'); ?></label>
                            <input type="text" name="bed[]" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-lg-6 pull-right">
                        <div class="form-group">
                            <label for="bath"><?php echo get_phrase('bath'); ?></label>
                            <input type="text" name="bath[]" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sqft"><?php echo get_phrase('sqft'); ?></label>
                        <input type="text" name="sqft[]" class="form-control" />
                    </div>
                    <div class="form-group col-lg-6 pull-right">
                        <div class="form-group">
                            <label for="sqft_lot"><?php echo get_phrase('sqft_lot'); ?></label>
                            <input type="text" name="sqft_lot[]" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="property_type"><?php echo get_phrase('property_type'); ?></label>
                        <input type="text" name="property_type[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="style"><?php echo get_phrase('style'); ?></label>
                        <input type="text" name="style[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="year_built"><?php echo get_phrase('year_built'); ?></label>
                        <input type="text" name="year_built[]" class="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="real_estate_variants"><?php echo get_phrase('tags'); ?> <small>(<?php echo get_phrase('press_Enter_after_entering_every_tag'); ?>)</small></label>
                        <input type="text" class="form-control bootstrap-tag-input" name="real_estate_variants[]" data-role="tagsinput"/>
                    </div>
                </div>
                <div class="col-lg-4">
                  <div class="wrapper-image-preview">
                    <div class="box">
                      <div class="js--image-preview"></div>
                      <div class="upload-options">
                        <label for="" class="btn"> <i class="entypo-camera"></i> <?php echo get_phrase('upload_real_estate_image'); ?> <small>(200 X 200) </small> </label>
                        <input id="" style="visibility:hidden;" type="file" class="image-upload" name="real_estate_image[]" accept="image/*">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
