<div id="car_parent_div" style="display: none; padding-top: 10px;">
  <div id = "car_div">
    <div class="car_div">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-body">
              <div class="collapse show" style="padding-top: 10px;">
                <div class="row no-margin">
                    <div class="col-lg-12">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="car_name"><?php echo get_phrase('name'); ?></label>
                                <input type="text" name="car_name[]" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="new_used"><?php echo get_phrase('new/used'); ?></label>
                                <select name="new_used[]" class="selectboxit">
                                    <option value="new">new</option>
                                    <option value="used">used</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="mileage"><?php echo get_phrase('mileage'); ?></label>
                                <input type="number" name="mileage[]" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="car_price"><?php echo get_phrase('price').' ('.currency_code_and_symbol().')'; ?></label>
                                <input type="number" name="car_price[]" class="form-control" />
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="wrapper-image-preview">
                                <div class="box">
                                    <div class="js--image-preview"></div>
                                    <div class="upload-options">
                                        <label for="car-image-1" class="btn"> <i class="entypo-camera"></i> <?php echo get_phrase('upload_car_image'); ?> <small>(200 X 200) </small> </label>
                                        <input id="car-image-1" style="visibility:hidden;" type="file" class="image-upload" name="car_image[]" accept="image/*">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="car_description"><?php echo get_phrase('description'); ?></label>
                            <textarea name="car_description[]" class="form-control ckeditor" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label for="fuel_type"><?php echo get_phrase('fuel_type'); ?></label>
                            <select name="fuel_type[]" class="selectboxit">
                                <option value="Gasoline">Gasoline</option>
                                <option value="Diesel">Diesel</option>
                                <option value="Biodiesel">Biodiesel</option>
                                <option value="Ethanol">Ethanol</option>
                                <option value="Electric">Electric</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-6 pull-right">
                            <label for="city_mpg"><?php echo get_phrase('city_mpg'); ?></label>
                            <input type="text" name="city_mpg[]" class="form-control" />
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label for="highway_mpg"><?php echo get_phrase('highway_mpg'); ?></label>
                            <input type="text" name="highway_mpg[]" class="form-control" />
                        </div>
                        <div class="form-group col-lg-6 pull-right">
                            <label for="drivetrain"><?php echo get_phrase('drivetrain'); ?></label>
                            <input type="text" name="drivetrain[]" class="form-control" />
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label for="engine"><?php echo get_phrase('engine'); ?></label>
                            <input type="text" name="engine[]" class="form-control" />
                        </div>
                        <div class="form-group col-lg-6 pull-right">
                            <label for="exterior_color"><?php echo get_phrase('exterior_color'); ?></label>
                            <input type="text" name="exterior_color[]" class="form-control" />
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label for="interior_color"><?php echo get_phrase('interior_color'); ?></label>
                            <input type="text" name="interior_color[]" class="form-control" />
                        </div>
                        <div class="form-group col-lg-6 pull-right">
                            <label for="stock"><?php echo get_phrase('stock'); ?></label>
                            <input type="text" name="stock[]" class="form-control" />
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label for="transmission"><?php echo get_phrase('transmission'); ?></label>
                            <input type="text" name="transmission[]" class="form-control" />
                        </div>
                        <div class="form-group col-lg-6 pull-right">
                            <label for="vin"><?php echo get_phrase('vin'); ?></label>
                            <input type="text" name="vin[]" class="form-control" />
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-12">
                            <label for="category"> <?php echo get_phrase('category'); ?></label>
                            <div id="category_area">
                                <select name="categories[]" style="display: none;">
                                    <option value="<?php echo $category_id; ?>"></option>
                                </select>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <select class="form-control" name="categories[]" id="category_default" required>
                                            <option value=""><?php echo get_phrase('select_category'); ?></option>
							                <?php foreach ($categories as $category): ?>
                                                <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
							                <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-primary btn-sm" style="margin-top: 2px; float: right;" name="button" onclick="appendCategory()"> <i class="fa fa-plus"></i> </button>
                                    </div>
                                </div>
                            </div>

                            <div id="blank_category_field">
                                <div class="row appendedCategoryFields" style="margin-top: 10px;">
                                    <div class="col-sm-6 pr-0">
                                        <select class="form-control" name="categories[]">
                                            <option value=""><?php echo get_phrase('select_category'); ?></option>
							                <?php foreach ($categories as $category): ?>
                                                <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
							                <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-danger btn-sm" style="margin-top: 2px; float: right;" name="button" onclick="removeCategory(this)"> <i class="fa fa-minus"></i> </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="car_variants"><?php echo get_phrase('tags'); ?> <small>(<?php echo get_phrase('press_Enter_after_entering_every_tag'); ?>)</small></label>
                            <input type="text" class="form-control bootstrap-tag-input" name="car_variants[]" data-role="tagsinput"/>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row text-center" style="display: none">
    <button type="button" class="btn btn-primary" name="button" onclick="appendCar()"> <i class="mdi mdi-plus"></i> <?php echo get_phrase('add_new_car'); ?></button>
  </div>
</div>

<div id = "blank_car_div">
  <div class="car_div">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <div class="panel panel-primary" data-collapsed="0">
          <div class="panel-body">
            <h5 class="card-title mb-0"><?php echo get_phrase('car'); ?>
              <button type="button" class="btn btn-danger btn-sm btn-rounded alignToTitleOnPreview" name="button" onclick="removeCar(this)"><?php echo get_phrase('remove_this_car'); ?></button>
            </h5>
            <div class="collapse show" style="padding-top: 10px;">
              <div class="row no-margin">
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="car_name"><?php echo get_phrase('name'); ?></label>
                        <input type="text" name="car_name[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="car_description"><?php echo get_phrase('description'); ?></label>
                        <textarea name="car_description[]" class="form-control" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="car_price"><?php echo get_phrase('price').' ('.currency_code_and_symbol().')'; ?></label>
                        <input type="text" name="car_price[]" class="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="fuel_type"><?php echo get_phrase('fuel_type'); ?></label>
                        <input type="text" name="fuel_type[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="city_mpg"><?php echo get_phrase('city_mpg'); ?></label>
                        <input type="text" name="city_mpg[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="highway_mpg"><?php echo get_phrase('highway_mpg'); ?></label>
                        <input type="text" name="highway_mpg[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="drivetrain"><?php echo get_phrase('drivetrain'); ?></label>
                        <input type="text" name="drivetrain[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="engine"><?php echo get_phrase('engine'); ?></label>
                        <input type="text" name="engine[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="mileage"><?php echo get_phrase('mileage'); ?></label>
                        <input type="text" name="mileage[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="exterior_color"><?php echo get_phrase('exterior_color'); ?></label>
                        <input type="text" name="exterior_color[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="interior_color"><?php echo get_phrase('interior_color'); ?></label>
                        <input type="text" name="interior_color[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="stock"><?php echo get_phrase('stock'); ?></label>
                        <input type="text" name="stock[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="transmission"><?php echo get_phrase('transmission'); ?></label>
                        <input type="text" name="transmission[]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="vin"><?php echo get_phrase('vin'); ?></label>
                        <input type="text" name="vin[]" class="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="car_variants"><?php echo get_phrase('tags'); ?> <small>(<?php echo get_phrase('press_Enter_after_entering_every_tag'); ?>)</small></label>
                        <input type="text" class="form-control bootstrap-tag-input" name="car_variants[]" data-role="tagsinput"/>
                    </div>
                </div>
                <div class="col-lg-4">
                  <div class="wrapper-image-preview">
                    <div class="box">
                      <div class="js--image-preview"></div>
                      <div class="upload-options">
                        <label for="" class="btn"> <i class="entypo-camera"></i> <?php echo get_phrase('upload_car_image'); ?> <small>(200 X 200) </small> </label>
                        <input id="" style="visibility:hidden;" type="file" class="image-upload" name="car_image[]" accept="image/*">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
