<?php $cars = $this->db->get_where('car_details', array('listing_id' => $listing_details['id']))->result_array(); ?>
<div id = "car_parent_div" style="display: none; padding-top: 10px;">
  <div id = "car_div">
    <?php foreach ($cars as $key => $car): ?>
      <div class="car_div">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-primary" data-collapsed="0">
              <div class="panel-body">
                <h5 class="card-title mb-0"><?php echo get_phrase('car'); ?>
                  <?php if ($key > 0 ): ?>
                    <button type="button" class="btn btn-danger btn-sm btn-rounded alignToTitleOnPreview" name="button" id = "<?php echo $car['id']; ?>" onclick="removeCar(this)"><?php echo get_phrase('remove_this_car'); ?></button>
                  <?php endif; ?>
                </h5>
                <div class="collapse show" style="padding-top: 10px;">
                  <div class="row no-margin">
                    <div class="col-lg-12">
                        <div class="col-lg-8">
                          <input type="hidden" name="car_id[]" value="<?php echo $car['id']; ?>">
                            <div class="form-group">
                              <label for="car_name"><?php echo get_phrase('name'); ?></label>
                              <input type="text" name="car_name[]" class="form-control" value="<?php echo $car['name']; ?>"/>
                            </div>
                            <div class="form-group">
                                <label for="new_used"><?php echo get_phrase('new/used'); ?></label>
                                <select name="new_used[]" class="selectboxit">
                                    <option value="new" <?php if($car['new_used'] == 'new') echo 'selected'; ?>>new</option>
                                    <option value="used" <?php if($car['new_used'] == 'used') echo 'selected'; ?>>used</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="mileage"><?php echo get_phrase('mileage'); ?></label>
                                <input type="number" name="mileage[]" class="form-control" value="<?php echo $car['mileage']; ?>"/>
                            </div>
                            <div class="form-group">
                                <label for="car_price"><?php echo get_phrase('price').' ('.currency_code_and_symbol().')'; ?></label>
                                <input type="number" name="car_price[]" class="form-control" value="<?php echo $car['price']; ?>"/>
                            </div>
                        </div>
                        <div class="col-lg-4">
                          <div class="wrapper-image-preview">
                            <div class="box">
                              <div class="js--image-preview" style="background-image: url('<?php echo base_url('uploads/car_images/').$car['photo']; ?>')"></div>
                              <div class="upload-options">
                                <label for="car-image-<?php echo $car['id']; ?>" class="btn"> <i class="entypo-camera"></i> <?php echo get_phrase('upload_car_image'); ?> <small>(200 X 200) </small> </label>
                                <input id="car-image-<?php echo $car['id']; ?>" style="visibility:hidden;" type="file" class="image-upload" name="car_image[]" onchange="console.log(this.value);" accept="image/*">
                                <input type="hidden" name="old_car_images[]" value="<?php echo $car['photo']; ?>">
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="car_description"><?php echo get_phrase('description'); ?></label>
                            <textarea name="car_description[]" class="form-control ckeditor" rows="5"><?php echo $car['description']; ?></textarea>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label for="fuel_type"><?php echo get_phrase('fuel_type'); ?></label>
                            <select name="fuel_type[]" class="selectboxit">
                                <option value="Gasoline" <?php if($car['fuel_type'] == 'Gasoline') echo 'selected'; ?>>Gasoline</option>
                                <option value="Diesel" <?php if($car['fuel_type'] == 'Diesel') echo 'selected'; ?>>Diesel</option>
                                <option value="Biodiesel" <?php if($car['fuel_type'] == 'Biodiesel') echo 'selected'; ?>>Biodiesel</option>
                                <option value="Ethanol" <?php if($car['fuel_type'] == 'Ethanol') echo 'selected'; ?>>Ethanol</option>
                                <option value="Electric" <?php if($car['fuel_type'] == 'Electric') echo 'selected'; ?>>Electric</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-6 pull-right">
                            <label for="city_mpg"><?php echo get_phrase('city_mpg'); ?></label>
                            <input type="text" name="city_mpg[]" class="form-control" value="<?php echo $car['city_mpg']; ?>"/>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label for="highway_mpg"><?php echo get_phrase('highway_mpg'); ?></label>
                            <input type="text" name="highway_mpg[]" class="form-control" value="<?php echo $car['highway_mpg']; ?>"/>
                        </div>
                        <div class="form-group col-lg-6 pull-right">
                            <label for="drivetrain"><?php echo get_phrase('drivetrain'); ?></label>
                            <input type="text" name="drivetrain[]" class="form-control" value="<?php echo $car['drivetrain']; ?>"/>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label for="engine"><?php echo get_phrase('engine'); ?></label>
                            <input type="text" name="engine[]" class="form-control" value="<?php echo $car['engine']; ?>"/>
                        </div>
                        <div class="form-group col-lg-6 pull-right">
                            <label for="exterior_color"><?php echo get_phrase('exterior_color'); ?></label>
                            <input type="text" name="exterior_color[]" class="form-control" value="<?php echo $car['exterior_color']; ?>"/>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label for="interior_color"><?php echo get_phrase('interior_color'); ?></label>
                            <input type="text" name="interior_color[]" class="form-control" value="<?php echo $car['interior_color']; ?>"/>
                        </div>
                        <div class="form-group col-lg-6 pull-right">
                            <label for="stock"><?php echo get_phrase('stock'); ?></label>
                            <input type="text" name="stock[]" class="form-control" value="<?php echo $car['stock']; ?>"/>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label for="transmission"><?php echo get_phrase('transmission'); ?></label>
                            <input type="text" name="transmission[]" class="form-control" value="<?php echo $car['transmission']; ?>"/>
                        </div>
                        <div class="form-group col-lg-6 pull-right">
                            <label for="vin"><?php echo get_phrase('vin'); ?></label>
                            <input type="text" name="vin[]" class="form-control" value="<?php echo $car['vin']; ?>"/>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-12">
                            <label for="category"> <?php echo get_phrase('category'); ?></label>
                            <div id="category_area">
                                <select name="categories[]" style="display: none;">
                                    <option value="<?php echo $category_id; ?>"></option>
                                </select>

                                <?php foreach ($listing_categories as $key => $listing_category): ?>

                                    <?php if($listing_category == $category_id) continue; ?>

                                    <?php if ($key == 1): ?>
                                        <div class="row">
                                            <div class="col-sm-6 pr-0">
                                                <select class="form-control" data-toggle="select2" name="categories[]" id = "category_default" required>
                                                    <option value=""><?php echo get_phrase('select_category'); ?></option>
                                                    <?php foreach ($categories as $category): ?>
                                                        <option value="<?php echo $category['id']; ?>" <?php if ($category['id'] == $listing_category): ?> selected <?php endif; ?>><?php echo $category['name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" class="btn btn-primary btn-sm" style="margin-top: 2px; float: right;" name="button" onclick="appendCategory()"> <i class="fa fa-plus"></i> </button>
                                            </div>
                                        </div>

                                    <?php else: ?>
                                        <div class="row mt-2 appendedCategoryFields" style="margin-top: 10px;">
                                            <div class="col-sm-6 pr-0">
                                                <select class="form-control" name="categories[]">
                                                    <option value=""><?php echo get_phrase('select_category'); ?></option>
                                                    <?php foreach ($categories as $category): ?>
                                                        <option value="<?php echo $category['id']; ?>" <?php if ($category['id'] == $listing_category): ?> selected <?php endif; ?>><?php echo $category['name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" class="btn btn-danger btn-sm" style="margin-top: 2px; float: right;" name="button" onclick="removeCategory(this)"> <i class="fa fa-minus"></i> </button>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>

                            <div id="blank_category_field">
                                <div class="row appendedCategoryFields" style="margin-top: 10px;">
                                    <div class="col-sm-6 pr-0">
                                        <select class="form-control" name="categories[]">
                                            <option value=""><?php echo get_phrase('select_category'); ?></option>
                                            <?php foreach ($categories as $category): ?>
                                                <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-danger btn-sm" style="margin-top: 2px; float: right;" name="button" onclick="removeCategory(this)"> <i class="fa fa-minus"></i> </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="car_variants"><?php echo get_phrase('tags'); ?> <small>(<?php echo get_phrase('press_Enter_after_entering_every_tag'); ?>)</small></label>
                            <input type="text" class="form-control bootstrap-tag-input" name="car_variants[]" data-role="tagsinput" value="<?php echo $car['variant']; ?>"/>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> <!-- end card-->
        </div>
      </div>
    <?php endforeach; ?>
  </div>

  <div class="row text-center" style="display: none">
    <button type="button" class="btn btn-primary" name="button" onclick="appendCar()"> <i class="mdi mdi-plus"></i> <?php echo get_phrase('add_new_car'); ?></button>
  </div>
</div>

<div id = "blank_car_div">
    <div class="car_div">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="panel panel-primary" data-collapsed="0">
                    <div class="panel-body">
                        <h5 class="card-title mb-0"><?php echo get_phrase('car'); ?>
                            <button type="button" class="btn btn-danger btn-sm btn-rounded alignToTitleOnPreview" name="button" onclick="removeCar(this)"><?php echo get_phrase('remove_this_car'); ?></button>
                        </h5>
                        <div class="collapse show" style="padding-top: 10px;">
                            <div class="row no-margin">
                                <div class="col-lg-8">
                                    <input type="hidden" name="car_id[]" value="0">
                                    <div class="form-group">
                                        <label for="car_name"><?php echo get_phrase('name'); ?></label>
                                        <input type="text" name="car_name[]" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="car_description"><?php echo get_phrase('description'); ?></label>
                                        <textarea name="car_description[]" class="form-control" rows="5"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="car_price"><?php echo get_phrase('price').' ('.currency_code_and_symbol().')'; ?></label>
                                        <input type="text" name="car_price[]" class="form-control" />
                                    </div>

                                    <div class="form-group">
                                        <label for="fuel_type"><?php echo get_phrase('fuel_type'); ?></label>
                                        <input type="text" name="fuel_type[]" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="city_mpg"><?php echo get_phrase('city_mpg'); ?></label>
                                        <input type="text" name="city_mpg[]" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="highway_mpg"><?php echo get_phrase('highway_mpg'); ?></label>
                                        <input type="text" name="highway_mpg[]" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="drivetrain"><?php echo get_phrase('drivetrain'); ?></label>
                                        <input type="text" name="drivetrain[]" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="engine"><?php echo get_phrase('engine'); ?></label>
                                        <input type="text" name="engine[]" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="mileage"><?php echo get_phrase('mileage'); ?></label>
                                        <input type="text" name="mileage[]" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="exterior_color"><?php echo get_phrase('exterior_color'); ?></label>
                                        <input type="text" name="exterior_color[]" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="interior_color"><?php echo get_phrase('interior_color'); ?></label>
                                        <input type="text" name="interior_color[]" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="stock"><?php echo get_phrase('stock'); ?></label>
                                        <input type="text" name="stock[]" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="transmission"><?php echo get_phrase('transmission'); ?></label>
                                        <input type="text" name="transmission[]" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="vin"><?php echo get_phrase('vin'); ?></label>
                                        <input type="text" name="vin[]" class="form-control" />
                                    </div>

                                    <div class="form-group">
                                        <label for="car_variants"><?php echo get_phrase('tags'); ?> <small>(<?php echo get_phrase('press_Enter_after_entering_every_tag'); ?>)</small></label>
                                        <input type="text" class="form-control bootstrap-tag-input" name="car_variants[]" data-role="tagsinput"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="wrapper-image-preview">
                                        <div class="box">
                                            <div class="js--image-preview"></div>
                                            <div class="upload-options">
                                                <label for="" class="btn"> <i class="entypo-camera"></i> <?php echo get_phrase('upload_car_image'); ?> <small>(200 X 200) </small> </label>
                                                <input id="" style="visibility:hidden;" type="file" class="image-upload" name="car_image[]" accept="image/*">
                                                <input type="hidden" name="old_car_images[]" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
