<div class="row" style="display: none">
  <div class="col-sm-offset-3 col-sm-3">
      <div class="col-lg-12">
          <div class="custom-control custom-radio">
              <input type="radio" id="real_estate" name="listing_type" class="custom-control-input listing-type-radio" value="real-estate" onclick="showListingTypeForm('real-estate')" checked = "checked">
              <label class="custom-control-label" for="real_estate"><i class="fa fa-hotel" style="color: #636363;"></i> <?php echo get_phrase('real_estate'); ?></label>
          </div>
      </div>
  </div>
</div>

<?php include 'real_estate_form.php'; ?>
