<?php $jobs = $this->db->get_where('job_details', array('listing_id' => $listing_details['id']))->result_array(); ?>
<div id = "job_parent_div" style="display: none; padding-top: 10px;">
  <div id = "job_div">
    <?php foreach ($jobs as $key => $job): ?>
      <div class="job_div">
        <div class="row">
          <div class="col-lg-10">
            <div class="panel panel-primary" data-collapsed="0">
              <div class="panel-body">
                <div class="collapse show" style="padding-top: 10px;">
                  <div class="row no-margin">

                      <!-- Media -->
                      <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo get_phrase('listing_thumbnail'); ?> <br/> <small>(460 X 306)</small> </label>
                          <div class="col-sm-7">
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                  <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;" data-trigger="fileinput">
                                      <img src="<?php echo base_url('uploads/listing_thumbnails/'.$listing_details['listing_thumbnail']); ?>" alt="...">
                                  </div>
                                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                  <div>
                                        <span class="btn btn-white btn-file">
                                          <span class="fileinput-new"><?php echo get_phrase('select_image'); ?></span>
                                          <span class="fileinput-exists"><?php echo get_phrase('change'); ?></span>
                                          <input type="file" name="listing_thumbnail" accept="image/*">
                                        </span>
                                        <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput"><?php echo get_phrase('remove'); ?></a>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo get_phrase('listing_cover'); ?> <br/> <small>(1600 X 600)</small> </label>
                          <div class="col-sm-7">
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                  <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;" data-trigger="fileinput">
                                      <img src="<?php echo base_url('uploads/listing_cover_photo/'.$listing_details['listing_cover']); ?>" alt="...">
                                  </div>
                                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                  <div>
                                        <span class="btn btn-white btn-file">
                                          <span class="fileinput-new"><?php echo get_phrase('select_image'); ?></span>
                                          <span class="fileinput-exists"><?php echo get_phrase('change'); ?></span>
                                          <input type="file" name="listing_cover" accept="image/*">
                                        </span>
                                        <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput"><?php echo get_phrase('remove'); ?></a>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <!-- Form -->
                    <div class="col-lg-9">
                      <input type="hidden" name="job_id[]" value="<?php echo $job['id']; ?>">

                        <div class="form-group">
                            <label for="title"><?php echo get_phrase('title'); ?></label>
                            <input type="hidden" name="user_id" value="<?php echo $listing_details['user_id']?>">
                            <input type="text" name="title" class="form-control" id="title" value="<?php echo $listing_details['name']; ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="employment_term"><?php echo get_phrase('employment_term'); ?></label>

                            <select name="employment_term[]" class="selectboxit">
                                <option value="Permanent" <?php if($job['employment_term'] == 'Permanent') echo 'selected'; ?>>Permanent</option>
                                <option value="Temporary" <?php if($job['employment_term'] == 'Temporary') echo 'selected'; ?>>Temporary</option>
                                <option value="Freelance" <?php if($job['employment_term'] == 'Freelance') echo 'selected'; ?>>Freelance</option>
                                <option value="Contract" <?php if($job['employment_term'] == 'Contract') echo 'selected'; ?>>Contract</option>
                                <option value="Internship" <?php if($job['employment_term'] == 'Internship') echo 'selected'; ?>>Internship</option>
                                <option value="Other" <?php if($job['employment_term'] == 'Other') echo 'selected'; ?>>Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="job_type"><?php echo get_phrase('job_type'); ?></label>
                            <select name="job_type[]" class="selectboxit">
                                <option value="Full time" <?php if($job['job_type'] == 'Full time') echo 'selected'; ?>>Full time</option>
                                <option value="Part time" <?php if($job['job_type'] == 'Part time') echo 'selected'; ?>>Part time</option>
                                <option value="Training" <?php if($job['job_type'] == 'Training') echo 'selected'; ?>>Training</option>
                                <option value="Fixed term contract" <?php if($job['job_type'] == 'Fixed term contract') echo 'selected'; ?>>Fixed term contract</option>
                                <option value="Tender" <?php if($job['job_type'] == 'Tender') echo 'selected'; ?>>Tender</option>
                                <option value="Internship" <?php if($job['job_type'] == 'Internship') echo 'selected'; ?>>Internship</option>
                                <option value="Remote" <?php if($job['job_type'] == 'Remote') echo 'selected'; ?>>Remote</option>
                                <option value="Other" <?php if($job['job_type'] == 'Other') echo 'selected'; ?>>Other</option>
                            </select>
                        </div>
                    </div>

                      <!-- Deadline -->
                      <div class="col-lg-5">
                          <div class="form-group job_deadline">
                              <label style="margin-bottom: 10px"><?php echo get_phrase('deadline'); ?></label>
                              <input type="date" name="deadline" value="<?php if(!empty($job_deadline_details['date'])) echo $job_deadline_details['date']; ?>" class="form-control">
                          </div>
                      </div>

                      <!-- Category -->
                      <div class="col-lg-9">
                          <div class="form-group">
                              <label for="category"> <?php echo get_phrase('job_category'); ?></label>
                              <div id="category_area">
                                  <select name="categories[]" style="display: none;">
                                      <option value="<?php echo $category_id; ?>"></option>
                                  </select>

                                  <?php foreach ($listing_categories as $key => $listing_category): ?>

                                      <?php if($listing_category == $category_id) continue; ?>

                                      <?php if ($key == 1): ?>
                                          <div class="row">
                                              <div class="col-sm-7 pr-0">
                                                  <select class="form-control select2" data-toggle="select2" name="categories[]" id = "category_default" required>
                                                      <option value=""><?php echo get_phrase('select_category'); ?></option>
                                                      <?php foreach ($categories as $category): ?>
                                                          <option value="<?php echo $category['id']; ?>" <?php if ($category['id'] == $listing_category): ?> selected <?php endif; ?>><?php echo $category['name']; ?></option>
                                                      <?php endforeach; ?>
                                                  </select>
                                              </div>
                                              <div class="col-sm-1">
                                                  <button type="button" class="btn btn-primary btn-sm" style="margin-top: 5px; float: right;" name="button" onclick="appendCategory()"> <i class="fa fa-plus"></i> </button>
                                              </div>
                                          </div>
                                      <?php else: ?>
                                          <div class="row mt-2 appendedCategoryFields" style="margin-top: 10px;">
                                              <div class="col-sm-7 pr-0">
                                                  <select class="form-control select2" name="categories[]">
                                                      <option value=""><?php echo get_phrase('select_category'); ?></option>
                                                      <?php foreach ($categories as $category): ?>
                                                          <option value="<?php echo $category['id']; ?>" <?php if ($category['id'] == $listing_category): ?> selected <?php endif; ?>><?php echo $category['name']; ?></option>
                                                      <?php endforeach; ?>
                                                  </select>
                                              </div>
                                              <div class="col-sm-1">
                                                  <button type="button" class="btn btn-danger btn-sm" style="margin-top: 5px; float: right;" name="button" onclick="removeCategory(this)"> <i class="fa fa-minus"></i> </button>
                                              </div>
                                          </div>
                                      <?php endif; ?>
                                  <?php endforeach; ?>
                              </div>

                              <div id="blank_category_field">
                                  <div class="row appendedCategoryFields" style="margin-top: 10px;">
                                      <div class="col-sm-7 pr-0">
                                          <select class="form-control" name="categories[]">
                                              <option value=""><?php echo get_phrase('select_category'); ?></option>
                                              <?php foreach ($categories as $category): ?>
                                                  <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
                                              <?php endforeach; ?>
                                          </select>
                                      </div>
                                      <div class="col-sm-1">
                                          <button type="button" class="btn btn-danger btn-sm" style="margin-top: 5px; float: right;" name="button" onclick="removeCategory(this)"> <i class="fa fa-minus"></i> </button>
                                      </div>
                                  </div>
                              </div>
                        </div>
                      </div>

                      <div class="col-lg-9">
                        <div class="form-group">
                            <label for="job_description"><?php echo get_phrase('description'); ?></label>
                            <textarea name="job_description[]" class="form-control ckeditor" rows="10"><?php echo $job['description']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="job_responsibilities"><?php echo get_phrase('job_responsibilities'); ?></label>
                            <textarea name="job_responsibilities[]" class="form-control ckeditor" rows="10"><?php echo $job['job_responsibilities']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="required_qualifications"><?php echo get_phrase('required_qualifications'); ?></label>
                            <textarea name="required_qualifications[]" class="form-control ckeditor" rows="10"><?php echo $job['required_qualifications']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="job_price"><?php echo get_phrase('salary').' ('.currency_code_and_symbol().')'; ?></label>
                            <input type="text" name="job_price[]" class="form-control" value="<?php echo $job['price']; ?>"/>
                        </div>

                        <div class="form-group">
                          <label for="job_variants"><?php echo get_phrase('skills'); ?> <small>(<?php echo get_phrase('press_Enter_after_entering_every_skill'); ?>)</small></label>
                          <input type="text" class="form-control bootstrap-tag-input" name="job_variants[]" data-role="tagsinput" value="<?php echo $job['variant']; ?>"/>
                        </div>

                        <!-- About company, featured -->
                        <div class="form-group">
                            <label for="description"><?php echo get_phrase('about_company'); ?> <small>(<?php echo get_phrase('about_field_is_optional'); ?>)</small></label>
                            <textarea name="description" id="description" class="form-control ckeditor" rows="8" cols="80" required><?php echo $listing_details['description']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="featured_type"><?php echo get_phrase('featured_type'); ?></label>
                            <select name="is_featured" id = "featured_type" class="selectboxit" required>
                                <option value=""><?php echo get_phrase('select_featured_type'); ?></option>
                                <option value="1"  <?php if($listing_details['is_featured'] == 1) echo 'selected'; ?>><?php echo get_phrase('featured'); ?></option>
                                <option value="0" <?php if($listing_details['is_featured'] == 0) echo 'selected'; ?>><?php echo get_phrase('none_featured'); ?></option>
                            </select>
                        </div>

                        <!-- Video -->
                        <hr>
                        <div class="form-group">
                            <label for="video_provider" class="col-sm-3 control-label"><?php echo get_phrase('video_provider'); ?></label>
                            <div class="col-sm-9">
                                <select name="video_provider" id = "video_provider" class="selectboxit" required>
                                    <option value="youtube" <?php if($listing_details['video_provider'] == 'youtube'): ?> selected <?php endif; ?>>YouTube</option>
                                    <option value="vimeo" <?php if($listing_details['video_provider'] == 'vimeo'): ?> selected <?php endif; ?>>Vimeo</option>
                                    <option value="html5" <?php if($listing_details['video_provider'] == 'html5'): ?> selected <?php endif; ?>>HTML5</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="video_url" class="col-sm-3 control-label"><?php echo get_phrase('video_url'); ?></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="video_url" name="video_url" placeholder="<?php echo get_phrase('you_can_provide_video_url'); ?>" value="<?php echo $listing_details['video_url']; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4" style="display: none">
                      <div class="wrapper-image-preview">
                        <div class="box">
                          <div class="js--image-preview" style="background-image: url('<?php echo base_url('uploads/job_images/').$job['photo']; ?>')"></div>
                          <div class="upload-options">
                            <label for="job-image-<?php echo $job['id']; ?>" class="btn"> <i class="entypo-camera"></i> <?php echo get_phrase('upload_job_image'); ?> <small>(200 X 200) </small> </label>
                            <input id="job-image-<?php echo $job['id']; ?>" style="visibility:hidden;" type="file" class="image-upload" name="job_image[]" onchange="console.log(this.value);" accept="image/*">
                            <input type="hidden" name="old_job_images[]" value="<?php echo $job['photo']; ?>">
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div> <!-- end jobd-->
        </div>
      </div>
    <?php endforeach; ?>
  </div>

  <div class="row text-center" style="display: none">
    <button type="button" class="btn btn-primary" name="button" onclick="appendJob()"> <i class="mdi mdi-plus"></i> <?php echo get_phrase('add_new_job'); ?></button>
  </div>
</div>

<div id = "blank_job_div" style="display: none">
    <div class="job_div">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="panel panel-primary" data-collapsed="0">
                    <div class="panel-body">
                        <div class="collapse show" style="padding-top: 10px;">
                            <div class="row no-margin">
                                <div class="col-lg-8">
                                    <input type="hidden" name="job_id[]" value="0">
                                    <div class="form-group">
                                        <label for="job_name"><?php echo get_phrase('name'); ?></label>
                                        <input type="text" name="job_name[]" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="employment_term"><?php echo get_phrase('employment_term'); ?></label>
                                        <input type="text" name="employment_term[]" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="job_type"><?php echo get_phrase('job_type'); ?></label>
                                        <input type="text" name="job_type[]" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="job_description"><?php echo get_phrase('description'); ?></label>
                                        <textarea name="job_description[]" class="form-control" rows="5"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="job_responsibilities"><?php echo get_phrase('job_responsibilities'); ?></label>
                                        <textarea name="job_responsibilities[]" class="form-control" rows="5"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="required_qualifications"><?php echo get_phrase('required_qualifications'); ?></label>
                                        <textarea name="required_qualifications[]" class="form-control" rows="5"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="job_price"><?php echo get_phrase('salary').' ('.currency_code_and_symbol().')'; ?></label>
                                        <input type="text" name="job_price[]" class="form-control" />
                                    </div>

                                    <div class="form-group">
                                        <label for="job_variants"><?php echo get_phrase('skills'); ?> <small>(<?php echo get_phrase('press_Enter_after_entering_every_skill'); ?>)</small></label>
                                        <input type="text" class="form-control bootstrap-tag-input" name="job_variants[]" data-role="tagsinput"/>
                                    </div>
                                </div>
                                <div class="col-lg-4" style="display: none">
                                    <div class="wrapper-image-preview">
                                        <div class="box">
                                            <div class="js--image-preview"></div>
                                            <div class="upload-options">
                                                <label for="" class="btn"> <i class="entypo-camera"></i> <?php echo get_phrase('upload_job_image'); ?> <small>(200 X 200) </small> </label>
                                                <input id="" style="visibility:hidden;" type="file" class="image-upload" name="job_image[]" accept="image/*">
                                                <input type="hidden" name="old_job_images[]" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
