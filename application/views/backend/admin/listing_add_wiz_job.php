<?php
$countries  = $this->db->get('country')->result_array();
//$categories = $this->db->get('category')->result_array();
$categories = $this->crud_model->get_sub_categories($category_id)->result_array();
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo get_phrase('add_listing_form'); ?>
                </div>
            </div>
            <div class="panel-body">
                <form action="<?php echo site_url('admin/listings/add?type=job'); ?>" method="post" enctype="multipart/form-data" role="form" class="form-horizontal form-groups-bordered listing_add_form">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
                            <li class="active">
                                <a href="#eighth" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-cog"></i></span>
                                    <span class="hidden-xs"><?php echo get_phrase('type'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="#second" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-location"></i></span>
                                    <span class="hidden-xs"><?php echo get_phrase('location'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="#seventh" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-link"></i></span>
                                    <span class="hidden-xs"><?php echo get_phrase('contact'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="#ninth" data-toggle="tab">
                                    <span class="visible-xs"><i class="entypo-check"></i></span>
                                    <span class="hidden-xs"><?php echo get_phrase('finish'); ?></span>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="eighth">
                                <?php include 'add_listing_type_job.php'; ?>

                                <?php echo prev_next(false); ?>
                            </div>

                            <div class="tab-pane" id="second">
                                <?php include 'add_listing_location.php'; ?>

                                <?php echo prev_next(); ?>
                            </div>

                            <div class="tab-pane" id="seventh">
                                <?php include 'add_listing_contact.php'; ?>

                                <?php echo prev_next(); ?>
                            </div>

                            <div class="tab-pane" id="ninth">
                                <?php include 'add_listing_finish.php'; ?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- end col-->
</div>
<script type="text/javascript">
    function getCityList(country_id) {
        $.ajax({
            type : 'POST',
            url : '<?php echo site_url('home/get_city_list_by_country_id'); ?>',
            data : {country_id : country_id},
            success : function(response) {
                $('#city_id').html(response);
            }
        });
    }
    var blank_category = $('#blank_category_field').html();
    var blank_photo_uploader = $('#blank_photo_uploader').html();
    var blank_job_div = $('#blank_job_div').html();
    var listing_type_value = $('.listing-type-radio').val();

    $(document).ready(function() {
        $('#blank_category_field').hide();
        $('#blank_photo_uploader').hide();
        $('#blank_job_div').hide();
        showListingTypeForm('job');
    });

    function appendJob() {
        jQuery('#job_div').append(blank_job_div);
        let selector = jQuery('#job_div .job_div');

        let rand = Math.random().toString(36).slice(3);

        $(selector[selector.length - 1]).find('label.btn').attr('for', 'job-image-' + rand );
        $(selector[selector.length - 1]).find('input.image-upload').attr('id', 'job-image-' + rand );
        $(".bootstrap-tag-input").tagsinput('items');
        initImagePreviewer();
    }

    function removeJob(elem) {
        jQuery(elem).closest('.job_div').remove();
        $(".bootstrap-tag-input").tagsinput('items');
    }

    function appendCategory() {
        jQuery('#category_area').append(blank_category);
    }

    function removeCategory(categoryElem) {
        jQuery(categoryElem).closest('.appendedCategoryFields').remove();
    }

    function appendPhotoUploader() {
        jQuery('#photos_area').append(blank_photo_uploader);
    }

    function removePhotoUploader(photoElem) {
        jQuery(photoElem).closest('.appendedPhotoUploader').remove();
    }

    function showListingTypeForm(listing_type) {
        listing_type_value = listing_type;

        $('#job_parent_div').show();
    }

    // This function checks the minimul required fields of listing form
    function checkMinimumFieldRequired() {
        var title = $('#title').val();
        var defaultCategory = $('#category_default').val();
        if (title === "" || defaultCategory === "") {
            error_notify('<?php echo get_phrase('listing_title').', '.get_phrase('listing_category').', '.get_phrase('can_not_be_empty'); ?>');
        }else {
            $('.listing_add_form').submit();
        }
    }
</script>

<script>
    function service_time(){
        var starting_time = $('#starting_time').val();
        var ending_time = $('#starting_time').val();
        if(starting_time != '' && ending_time != ''){
            $("#ending_time").attr("min", starting_time);
            $("#ending_time").attr("max", "24:00");
        }
    }

    // function service_time_min(){
    // 	var ending_time   = $('#ending_time').val();
    // 	if(ending_time != ''){
    // 		$("#ending_time").attr("min", "1");
    // 		$("#ending_time").attr("max", ending_time);
</script>