<?php
$listing_details = $this->crud_model->get_listings($listing_id)->row_array();
$job_cv = $this->crud_model->get_job_cv_by_listing_id($listing_id)->result_array();
?>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">
					<?php echo $listing_details['name']; ?>
				</div>
			</div>
			<div class="panel-body">
				<div class="col-md-12">
					<div class="tab-content">
                        <div>
                            <table class="table table-bordered datatable">
                                <thead>
                                <tr>
                                    <th width="20"><div>#</div></th>
                                    <th><div><?php echo get_phrase('email');?></div></th>
                                    <th><div><?php echo get_phrase('first_name');?></div></th>
                                    <th><div><?php echo get_phrase('last_name');?></div></th>
                                    <th><div><?php echo get_phrase('phone');?></div></th>
                                    <th><div><?php echo get_phrase('cover_letter');?></div></th>
                                    <th><div><?php echo get_phrase('cv');?></div></th>
                                    <th><div><?php echo get_phrase('optional_attachment');?></div></th>
                                    <th><div><?php echo get_phrase('created_at');?></div></th>
                                </tr>
                                </thead>
                                <tbody id = "listing_table">
                                <?php
                                $counter = 0;
                                foreach ($job_cv as $item): ?>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <div class="custom-control custom-checkbox">
                                                    <label class="custom-control-label" for="<?php echo $counter; ?>">
                                                        <?php echo ++$counter; ?>
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <strong>
                                                <?php echo $item['email']; ?>
                                            </strong>
                                        </td>
                                        <td>
                                            <?php echo $item['first_name']; ?>
                                        </td>
                                        <td>
                                            <?php echo $item['last_name']; ?>
                                        </td>
                                        <td>
                                            <?php echo $item['phone']; ?>
                                        </td>
                                        <td>
                                            <?php echo $item['cover_letter']; ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url('uploads/job_cv/') . $item['cv']; ?>">
                                                <span class="badge badge-secondary">View</span>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url('uploads/job_cv/') . $item['optional_attachment']; ?>">
                                                <span class="badge badge-secondary">View</span>
                                            </a>
                                        </td>
                                        <td>
                                            <?php echo $item['created_at']; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- end col-->
</div>