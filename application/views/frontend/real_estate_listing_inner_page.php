<?php $real_estates = $this->db->get_where('real_estate_details', array('listing_id' => $listing_details['id']))->result_array(); ?>
<h5><?php echo get_phrase('real_estate_details'); ?></h5>
<?php foreach ($real_estates as $real_estate): ?>
	<div class="room_type first">
		<div class="row">
			<div class="col-md-4">
				<img src="<?php echo base_url('uploads/real_estate_images/'.$real_estate['photo']); ?>" class="img-fluid" alt="">
			</div>
			<div class="col-md-8">
                <div><?php echo ucfirst(str_replace('_', ' ', $real_estate['status'])); ?></div>
				<h4><?php echo $real_estate['name']; ?></h4>
				<h3 style="margin-top: 20px;"><?php echo get_phrase('price').': '.currency(number_format($real_estate['price'])); ?></h3>
				<p style="margin-top: 20px;">
                    <?php
                    echo $real_estate['bed'] . ' <span style="font-weight: normal">bed</span> &nbsp;&nbsp;';
                    echo $real_estate['bath'] . ' <span style="font-weight: normal">bath</span> &nbsp;&nbsp;';
                    echo number_format($real_estate['sqft']) . ' <span style="font-weight: normal">sqft</span> &nbsp;&nbsp;';
                    echo number_format($real_estate['sqft_lot']) . ' <span style="font-weight: normal">sqft lot</span>';
                    ?>
                </p>
			</div>
		</div>
        <div class="row">
            <div class="col-md-12" style="margin-top: 10px">
                <div style="margin-bottom: 10px;">
                    <div style="display: inline-block; width: 300px">
                        <span style="font-weight: bold"><?php echo get_phrase('property_type'); ?>:</span>
                        <span><?php echo $real_estate['property_type']; ?></span>
                    </div>

                    <div style="display: inline-block; width: 300px">
                        <span style="font-weight: bold;"><?php echo get_phrase('style'); ?>:</span>
                        <span><?php echo $real_estate['style']; ?></span>
                    </div>
                </div>
                <div style="margin-bottom: 10px;">
                    <div style="display: inline-block; width: 300px">
                        <span style="font-weight: bold"><?php echo get_phrase('year_built'); ?>:</span>
                        <span><?php echo $real_estate['year_built']; ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 10px">
                <h4><?php echo get_phrase('description'); ?>:</h4>
                <p style="margin-bottom: 10px;"><?php echo $real_estate['description']; ?></p>
            </div>
        </div>
	</div>
<?php endforeach; ?>
