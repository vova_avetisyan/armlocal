<?php
$inventory_categories = $this->db->get_where('inventory_category', ['listing_id' => $listing_details['id']])->result_array();
if (count($inventory_categories) > 0) {
  $default_inventory_category_id = (isset($inventory_category_id) && $inventory_category_id != null) ? $inventory_category_id : $inventory_categories[0]['id'];
  $inventories = $this->db->get_where('inventory', ['listing_id' => $listing_details['id'], 'availability' => 1, 'category_id' => $default_inventory_category_id])->result_array();
}else{
  $default_inventory_category_id = null;
  $inventories = array();
}

// GET CART ITEMS
$cart_items = $this->db->get_where('shopping_cart', array('user_id' => $this->session->userdata('user_id'), 'listing_id' => $listing_details['id']))->result_array();
$query = $this->db->select('inventory_id')->where(array('user_id' => $this->session->userdata('user_id'), 'listing_id' => $listing_details['id']))->get('shopping_cart')->result_array();
$inventory_id_in_cart = array();
foreach ($query as $row) {
  if(!in_array($row['inventory_id'], $inventory_id_in_cart)){
    array_push($inventory_id_in_cart, $row['inventory_id']);
  }
}

// SELECT SUMMATION OF THE CART ITEMS
$total_amount = $this->db->select_sum('price')->where(array('user_id' => $this->session->userdata('user_id'), 'listing_id' => $listing_details['id']))->get('shopping_cart')->row()->price;
$total_amount = $total_amount > 0 ? $total_amount : 0;
?>

<!-- SHOW ORDER CONFIRMATION MESSAGE -->
<?php if ($this->session->flashdata('is_order_confirmed')):?>
<?php include 'order_confirmation.php'; ?>
<?php else: ?>
  <!-- SHOP PRODUCTS -->
  <div class="display-products">
    <h5><?php echo get_phrase('shop_products'); ?></h5>
    <form class="" action="<?php echo site_url($listing_details['listing_type'].'/'.slugify($listing_details['name']).'/'.$listing_details['id']); ?>" method="get">
      <div class="row justify-content-center">
        <div class="col-md-2 text-right">
          <label for="inventory_category"><?php echo get_phrase('categories'); ?></label>
        </div>
        <div class="col-md-5">
          <div class="form-group clearfix">
            <div class="custom-select-form">
              <select class="wide" name="inventory_category">
                <option value=""><?php echo get_phrase('inventory_category'); ?></option>
                <?php foreach ($inventory_categories as $inventory_category): ?>
                  <option value="<?php echo sanitizer($inventory_category['id']); ?>" <?php if ($default_inventory_category_id == $inventory_category['id']): ?>selected<?php endif; ?>><?php echo sanitizer($inventory_category['name']); ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-1">
          <button type="submit" class="btn_1"><?php echo get_phrase('filter'); ?></button>
        </div>
      </div>
    </form>
    <div class="row">
      <?php foreach ($inventories as $inventory):
        $inventory_item_wise_cart_details = $this->db->get_where('shopping_cart', array('user_id' => $this->session->userdata('user_id'), 'listing_id' => $listing_details['id'], 'inventory_id' => $inventory['id']))->row_array();
        $quantity = $inventory_item_wise_cart_details['quantity'];
        ?>
        <div class="col-lg-6 col-md-12">
          <ul class="menu_list">
            <li>
              <div class="thumb">
                <img src="<?php echo base_url('uploads/shop/'.$inventory['thumbnail']); ?>" alt="" style="height: 88px; width: 88px;">
              </div>
              <h6><?php echo sanitizer($inventory['name']); ?> <span><?php echo currency($inventory['price']); ?> </span></h6>
              <div class="mb-1">
                <?php echo sanitizer($inventory['details']); ?>
              </div>
              <div class="row">
                <div class="col-md-10">
                  <input class="form-control form-control-sm cart-input" type="number" placeholder="<?php echo get_phrase('quantity'); ?>" name="quantity" id="quantity-<?php echo sanitizer($inventory['id']); ?>" min="1" value="<?php echo sanitizer($quantity); ?>" onchange="cartHandler('<?php echo sanitizer($inventory['id']); ?>')">
                </div>
                <div class="col-md-2">
                  <input class="form-control form-control-sm cart-input" type="checkbox" name="" id="cart-btn-<?php echo sanitizer($inventory['id']); ?>" onchange="cartHandler('<?php echo sanitizer($inventory['id']); ?>')" <?php if(in_array($inventory['id'], $inventory_id_in_cart)) echo "checked"; ?>>
                </div>
              </div>
            </li>
          </ul>
        </div>
      <?php endforeach; ?>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-4">
        <?php if ($total_amount > 0): ?>
          <button type="submit" class="btn_1 full-width" id="order-btn" onclick="showCheckout()"><?php echo get_phrase('confirm_order').'('.currency($total_amount).')'; ?></button>
        <?php else: ?>
          <button type="submit" class="btn_1 full-width" id="order-btn" onclick="showCheckout()"><?php echo get_phrase('order'); ?></button>
        <?php endif; ?>
      </div>
    </div>
  </div>

  <!-- CHECKOUT PORTION -->
  <div class="checkout">

  </div>
<?php endif;?>
<script type="text/javascript">
function cartHandler(inventoryId) {
  var addedToCart = 0;
  var quantity = $('#quantity-' + inventoryId).val();
  if ($('#cart-btn-' + inventoryId).is(":checked")) {
    addedToCart = 1;
    quantity = quantity > 0 ? $('#quantity-' + inventoryId).val() : 1;
    $('#quantity-' + inventoryId).val(quantity);
  }
  makeOrder(addedToCart, inventoryId, quantity);
}

function makeOrder(addedToCart, inventoryId, quantity) {
  $(".cart-input").prop('disabled', true);
  $.ajax({
    type: "post",
    url: "<?php echo site_url('addons/shop/cart_handler'); ?>",
    data: {addedToCart : addedToCart, inventoryId : inventoryId, quantity : quantity},
    success: function(response){
      if(response === "false"){
        toastr.error("<?php echo get_phrase('login_first'); ?>");
      }else if (response === "") {
        $('#order-btn').html('<?php echo get_phrase('order') ?>');
      }else{
        $('#order-btn').html('<?php echo get_phrase('confirm_order') ?> (' + response + ')');
      }
      $(".cart-input").prop('disabled', false);
    }
  });
}

function showCheckout() {
  toastr.warning("<?php echo get_phrase('order_processing'); ?>");
  $.ajax({
    type: "post",
    url: "<?php echo site_url('addons/shop/show_checkout'); ?>",
    data: {listingId : '<?php echo sanitizer($listing_details['id']); ?>'},
    success: function(response){
      if(response === "false"){
        toastr.error("<?php echo get_phrase('you_have_not_added_any_product_yet'); ?>");
      }else{
        $(".display-products").hide();
        $(".checkout").html(response);
      }
    }
  });
}
</script>
