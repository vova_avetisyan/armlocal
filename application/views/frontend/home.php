<section class="hero_single version_2" style="background: #222 url(<?php echo base_url('uploads/system/home_banner.jpg'); ?>) center center no-repeat; background-size: cover;">
	<div class="wrapper">
		<div class="container">
			<h3><?php echo get_frontend_settings('banner_title'); ?>!</h3>
			<p><?php echo get_frontend_settings('slogan'); ?></p>
			<form action="<?php echo site_url('home/search'); ?>" method="get" id="first_page_search_form">
				<div class="row no-gutters custom-search-input-2">
					<div class="col-lg-7">
						<div class="form-group">
							<input class="form-control" type="text" name="search_string" placeholder="<?php echo get_phrase('what_are_you_looking_for'); ?>...">
							<i class="icon_search"></i>
						</div>
					</div>
					<div class="col-lg-3">
						<select class="wide" name="selected_category_id">
							<option value=""><?php echo get_phrase('all_categories'); ?></option>
							<?php
							$categories = $this->crud_model->get_parent_categories()->result_array();
							foreach ($categories as $category):?>
								<option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="col-lg-2">
						<input type="submit" value="<?= get_phrase('search'); ?>">
					</div>
				</div>
				<!-- /row -->
			</form>
		</div>
	</div>
</section>
<!-- /hero_single -->

<div class="bg_color_1">
	<div class="container margin_80_55">
		<div class="main_title_2">
			<span><em></em></span>
			<h2><?php echo get_phrase('popular_categories'); ?></h2>
			<p><?php echo get_phrase('the_popular_categories_are_progressively_below'); ?>.</p>
		</div>
		<div class="row" id="home_category">
			<!-- Single Item of popular category starts -->
			<?php
			$this->db->limit(9);
			$categories = $this->db->get_where('category', array('parent' => 0))->result_array();
			foreach ($categories as $key => $category):
                switch ($category['id']){
                    case 5:
                        $url = site_url('home/filter_job');
                        break;
                    case 6:
                        $url = site_url('home/filter_car');
                        break;
                    case 7:
                        $url = site_url('home/filter_real_estate');
                        break;
                    default:
                        $url = site_url('home/filter_listings?category='.slugify($category['name']).'&&amenity=&&video=0&&status=all');
                        break;
                } ?>
			<div class="col-lg-4 col-md-6">
				<a href="<?php echo $url; ?>" class="grid_item">
					<figure>
						<img src="<?php echo base_url('uploads/category_thumbnails/').$category['thumbnail'];?>" alt="">
						<div class="info">
							<small><?php echo count($this->frontend_model->get_category_wise_listings($category['id'])).' '.get_phrase('listings'); ?></small>
							<h3><?php echo $category['name']; ?></h3>
						</div>
					</figure>
				</a>
			</div>
			<?php endforeach; ?>
			<div class="col-12 text-center" id="home_category_loader" style="display: none; opacity: .5;">
				<img src="<?php echo base_url('assets/frontend/images/loader.gif'); ?>" width="50">
			</div>
			<!-- Single Item of popular category ends -->
			<?php $category_array_count = count($this->db->get_where('category', array('parent' => 0))->result_array()); ?>
			<?php if($category_array_count > 9): ?>
				<div class="col-12">
					<a href="javascript: void(0)" class="float-right btn_1 rounded" onclick="home_categories()"><?php echo get_phrase('view_all'); ?></a>
				</div>
			<?php endif; ?>
		</div>
	<!-- /row -->
</div>
<!-- /container -->
</div>
<!-- /bg_color_1 -->

<!-- Job -->
<div class="container-fluid">
    <div class="container margin_80_55">
        <div class="main_title_2">
            <span><em></em></span>
            <h2>Popular Jobs</h2>
        </div>

        <div class="row">
            <!-- Single Item of popular category starts -->
			<?php
			$listings = $this->frontend_model->get_top_ten_listings('job', 6);

			foreach ($listings as $key => $listing): ?>
				<?php $job = $this->db->get_where('job_details', array('listing_id' => $listing['id']))->result_array(); ?>
                <div class="col-lg-4 col-md-6">
                    <a href="<?php echo get_listing_url($listing['id']); ?>" class="grid_item">
                        <figure>
                            <img src="<?php echo base_url('uploads/listing_thumbnails/').$listing['listing_thumbnail'];?>" width="350" alt="">
                            <div class="info">
                                <small><?php if(!empty($job)) echo $job[0]['job_type']; ?></small>
                                <h3><?php echo $listing['name']; ?></h3>
                            </div>
                        </figure>
                    </a>
                </div>
			<?php endforeach; ?>

            <div class="col-12">
                <a href="<?php echo site_url('home/filter_job'); ?>" class="float-right btn_1 rounded"><?php echo get_phrase('view_all'); ?></a>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<!-- /container -->

<!-- Car -->
<div class="container-fluid margin_80_55">
    <div class="main_title_2">
        <span><em></em></span>
        <h2>Popular Cars</h2>
    </div>

    <div id="reccomended" class="owl-carousel owl-theme reccomended_car">
        <?php
        $listings = $this->frontend_model->get_top_ten_listings('car');

        foreach ($listings as $key => $listing): ?>
	        <?php $cars = $this->db->get_where('car_details', array('listing_id' => $listing['id']))->result_array(); ?>
            <div class="item">
                <div class="strip grid">
                    <figure>
                        <!--redirect to routs file-->
                        <a href="<?php echo get_listing_url($listing['id']); ?>">
                            <img src="<?php
                            if(!empty($cars)) {
	                            echo base_url('uploads/car_images/'.$cars[0]['photo']);
                            }
                            //echo base_url('uploads/listing_thumbnails/'.$listing['listing_thumbnail']);
                            ?>" class="img-fluid" alt="" width="400" height="266">
                            <div class="read_more"><span>Read more</span></div>
                        </a>
                        <small><?php if(!empty($cars)) echo $cars[0]['new_used']; ?></small>
                    </figure>
                    <div class="wrapper">
                        <h3 class="ellipsis">
                            <a href="<?php echo get_listing_url($listing['id']); ?>" class="float-left"><?php echo $listing['name']; ?></a>
                            <?php $claiming_status = $this->db->get_where('claimed_listing', array('listing_id' => $listing['id']))->row('status'); ?>
                            <?php if($claiming_status == 1): ?>
                                <img class="float-left ml-1" data-toggle="tooltip" title="<?php echo get_phrase('this_listing_is_verified'); ?>" src="<?php echo base_url('assets/frontend/images/verified.png'); ?>" style="width: 25px;">
                            <?php endif; ?>
                        </h3>
                        <p>
		                    <?php if(!empty($cars)) echo number_format($cars[0]['mileage']) . ' mi.'; ?>
                        </p>
                        <h3>
		                    <?php if(!empty($cars)) echo currency_code_and_symbol() . number_format($cars[0]['price']); ?>
                        </h3>
                        <small>
		                    <?php
                                $city 	 = $this->db->get_where('city', array('id' =>  $listing['city_id']))->row_array();
                                $country = $this->db->get_where('country', array('id' =>  $listing['country_id']))->row_array();
                                echo $city['name'].', '.$country['name'];
		                    ?>
                        </small>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <!-- /carousel -->
    <div class="container">
        <div class="btn_home_align"><a href="<?php echo site_url('home/filter_car'); ?>" class="btn_1 rounded"><?php echo get_phrase('view_all'); ?></a></div>
    </div>
    <!-- /container -->
</div>
<!-- /container -->

<!-- Real estate -->
<div class="container-fluid margin_80_55">
    <div class="main_title_2">
        <span><em></em></span>
        <h2>Popular Housing</h2>
    </div>

    <div id="reccomended" class="owl-carousel owl-theme reccomended_real_estate">
        <?php
        $listings = $this->frontend_model->get_top_ten_listings('real-estate');

        foreach ($listings as $key => $listing): ?>
	        <?php $housing = $this->db->get_where('real_estate_details', array('listing_id' => $listing['id']))->result_array(); ?>
            <div class="item">
                <div class="strip grid">
                    <figure>
                        <!--redirect to routs file-->
                        <a href="<?php echo get_listing_url($listing['id']); ?>"><img src="<?php echo base_url('uploads/listing_thumbnails/'.$listing['listing_thumbnail']); ?>" class="img-fluid" alt="" width="400" height="266"><div class="read_more"><span>Read more</span></div></a>
                    </figure>
                    <div class="wrapper">
                        <small>
		                    <?php if(!empty($housing)) echo ucfirst(str_replace('_', ' ', $housing[0]['status'])); ?>
                        </small>
                        <h3 class="ellipsis">
                            <a href="<?php echo get_listing_url($listing['id']); ?>" class="float-left"><?php echo $listing['name']; ?></a>
                            <?php $claiming_status = $this->db->get_where('claimed_listing', array('listing_id' => $listing['id']))->row('status'); ?>
                            <?php if($claiming_status == 1): ?>
                                <img class="float-left ml-1" data-toggle="tooltip" title="<?php echo get_phrase('this_listing_is_verified'); ?>" src="<?php echo base_url('assets/frontend/images/verified.png'); ?>" style="width: 25px;">
                            <?php endif; ?>
                        </h3>
                        <p>
		                    <?php
		                    $city 	 = $this->db->get_where('city', array('id' =>  $listing['city_id']))->row_array();
		                    $country = $this->db->get_where('country', array('id' =>  $listing['country_id']))->row_array();
		                    echo $city['name'].', '.$country['name'];
		                    ?>
                        </p>
                        <h3>
		                    <?php if(!empty($housing)) echo currency_code_and_symbol() . number_format($housing[0]['price']); ?>
                        </h3>
                        <small>
		                    <?php if(!empty($housing)) {
			                    echo $housing[0]['bed'] . ' <span style="font-weight: normal">bed</span> &nbsp;&nbsp;';
			                    echo $housing[0]['bath'] . ' <span style="font-weight: normal">bath</span> &nbsp;&nbsp;';
			                    echo number_format($housing[0]['sqft']) . ' <span style="font-weight: normal">sqft</span> &nbsp;&nbsp;';
			                    echo number_format($housing[0]['sqft_lot']) . ' <span style="font-weight: normal">sqft lot</span>';
		                    } ?>
                        </small>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <!-- /carousel -->
    <div class="container">
        <div class="btn_home_align"><a href="<?php echo site_url('home/filter_real_estate'); ?>" class="btn_1 rounded"><?php echo get_phrase('view_all'); ?></a></div>
    </div>
    <!-- /container -->
</div>
<!-- /container -->

<!-- Other -->
<div class="container-fluid margin_80_55">
    <div class="main_title_2">
        <span><em></em></span>
        <h2><?php echo get_phrase('popular_listings'); ?></h2>
    </div>

    <div id="reccomended" class="owl-carousel owl-theme reccomended_other">
        <?php // $listing_number = 0; ?>
        <?php $listings = $this->frontend_model->get_top_ten_listings();

        // foreach ($listings as $key => $listing):
        // 	$package_id = has_package($listing['user_id']);
        // 	$total_listing = $this->db->get_where('package_purchased_history', array('id', $package_id))->row('number_of_listings');

        // 	$listings_2 = $this->db->get_where('listing', array('user_id' => $listing['user_id']));
        // 	foreach($listings_2 as $listing_2){
        // 		$listing_number++;
        // 		if($listing_number < $total_listing || $listing_number == $total_listing){
        // 			echo 'show, ';
        // 		}
        // 	}
        // endforeach;

        foreach ($listings as $key => $listing): ?>
            <div class="item">
                <div class="strip grid">
                    <figure>
                        <!--redirect to routs file-->
                        <a href="<?php echo get_listing_url($listing['id']); ?>"><img src="<?php echo base_url('uploads/listing_thumbnails/'.$listing['listing_thumbnail']); ?>" class="img-fluid" alt="" width="400" height="266"><div class="read_more"><span>Read more</span></div></a>
                        <small><?php echo $listing['listing_type'] == "" ? ucfirst(get_phrase('general')) : ucfirst(get_phrase($listing['listing_type'])) ; ?></small>
                    </figure>
                    <div class="wrapper">
                        <h3>
                            <a href="<?php echo get_listing_url($listing['id']); ?>" class="float-left"><?php echo $listing['name']; ?></a>
                            <?php $claiming_status = $this->db->get_where('claimed_listing', array('listing_id' => $listing['id']))->row('status'); ?>
                            <?php if($claiming_status == 1): ?>
                                <img class="float-left ml-1" data-toggle="tooltip" title="<?php echo get_phrase('this_listing_is_verified'); ?>" src="<?php echo base_url('assets/frontend/images/verified.png'); ?>" style="width: 25px;">
                            <?php endif; ?>
                        </h3>
                        <br>
                        <p class="mt-1"><?php echo substr($listing['description'], 0, 100) . '...'; ?>.</p>
                        <a class="address" href="http://maps.google.com/maps?q=<?php echo $listing['latitude']; ?>,<?php echo $listing['longitude']; ?>" target="_blank"><?php echo get_phrase('get_directions'); ?></a>
                    </div>
                    <ul>
                        <!-- <li><span class="loc_open"><?php echo now_open($listing['id']); ?></span></li> -->
                        <li><span class="<?php echo strtolower(now_open($listing['id'])) == 'closed' ? 'loc_closed' : 'loc_open'; ?>"><?php echo now_open($listing['id']); ?></span></li>
                        <li>
                            <div class="score">
                            <span>
                                <?php
                                if ($this->frontend_model->get_listing_wise_rating($listing['id']) > 0) {
                                    $quality = $this->frontend_model->get_rating_wise_quality($listing['id']);
                                    echo $quality['quality'];
                                } else {
                                    echo get_phrase('unreviewed');
                                } ?>
                                <em><?php echo count($this->frontend_model->get_listing_wise_review($listing['id'])).' '.get_phrase('reviews'); ?></em>
                            </span>
                                <strong><?php echo $this->frontend_model->get_listing_wise_rating($listing['id']); ?></strong>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <!-- /carousel -->
    <div class="container">
        <div class="btn_home_align"><a href="<?php echo site_url('home/listings'); ?>" class="btn_1 rounded"><?php echo get_phrase('view_all'); ?></a></div>
    </div>
    <!-- /container -->
</div>
<!-- /container -->

<!-- /container -->

<script>
	function home_categories(limitation){
		$.ajax({
			url: "<?php echo site_url('home/home_categories/'); ?>",
			success: function(response){
				$('#home_category_loader').show();
				setInterval(function(){
					$('#home_category_loader').hide();
					$('#home_category').html(response);
				},1500);

			}
		});
	}

    $('#first_page_search_form').submit(function (el) {
        if($('[name="selected_category_id"] option:selected').val() == 5){
            window.location.href = '/home/filter_job';
            return false;
        }

        if($('[name="selected_category_id"] option:selected').val() == 6){
            window.location.href = '/home/filter_car';
            return false;
        }

        if($('[name="selected_category_id"] option:selected').val() == 7){
            window.location.href = '/home/filter_real_estate';
            return false;
        }

        return true;
    });
</script>
