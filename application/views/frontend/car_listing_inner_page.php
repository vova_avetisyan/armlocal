<?php $cars = $this->db->get_where('car_details', array('listing_id' => $listing_details['id']))->result_array(); ?>
<h5><?php echo get_phrase('car_details'); ?></h5>
<?php foreach ($cars as $car): ?>
	<div class="room_type first">
		<div class="row">
			<div class="col-md-4">
				<img src="<?php echo base_url('uploads/car_images/'.$car['photo']); ?>" class="img-fluid" alt="">
			</div>
			<div class="col-md-8">
				<div><?php echo $car['new_used']; ?></div>
				<h4><?php echo $car['name']; ?></h4>
				<p><?php echo get_phrase('mileage').': '.number_format($car['mileage']); ?></p>
				<h3><?php echo get_phrase('price').': '.currency(number_format($car['price'])); ?></h3>
			</div>
		</div>
        <div class="row">
            <div class="col-md-12" style="margin-top: 10px">
                <h4><?php echo get_phrase('description'); ?>:</h4>
                <p style="margin-bottom: 10px;"><?php echo $car['description']; ?></p>
            </div>
            <div class="col-md-12" style="margin-top: 10px">
                <h4><?php echo get_phrase('Basics'); ?>:</h4>
                <div style="margin-bottom: 10px;">
                    <div style="display: inline-block; width: 300px">
                        <span style="font-weight: bold"><?php echo get_phrase('fuel_type'); ?>:</span>
                        <span><?php echo $car['fuel_type']; ?></span>
                    </div>

                    <div style="display: inline-block; width: 300px">
                        <span style="font-weight: bold;"><?php echo get_phrase('city_mpg'); ?>:</span>
                        <span><?php echo $car['city_mpg']; ?></span>
                    </div>
                </div>
                <div style="margin-bottom: 10px;">
                    <div style="display: inline-block; width: 300px">
                        <span style="font-weight: bold"><?php echo get_phrase('highway_mpg'); ?>:</span>
                        <span><?php echo $car['highway_mpg']; ?></span>
                    </div>

                    <div style="display: inline-block; width: 300px">
                        <span style="font-weight: bold;"><?php echo get_phrase('drivetrain'); ?>:</span>
                        <span><?php echo $car['drivetrain']; ?></span>
                    </div>
                </div>
                <div style="margin-bottom: 10px;">
                    <div style="display: inline-block; width: 300px">
                        <span style="font-weight: bold"><?php echo get_phrase('exterior_color'); ?>:</span>
                        <span><?php echo $car['exterior_color']; ?></span>
                    </div>

                    <div style="display: inline-block; width: 300px">
                        <span style="font-weight: bold;"><?php echo get_phrase('interior_color'); ?>:</span>
                        <span><?php echo $car['interior_color']; ?></span>
                    </div>
                </div>
                <div style="margin-bottom: 10px;">
                    <div style="display: inline-block; width: 300px">
                        <span style="font-weight: bold"><?php echo get_phrase('engine'); ?>:</span>
                        <span><?php echo $car['engine']; ?></span>
                    </div>

                    <div style="display: inline-block; width: 300px">
                        <span style="font-weight: bold;"><?php echo get_phrase('transmission'); ?>:</span>
                        <span><?php echo $car['transmission']; ?></span>
                    </div>
                </div>
                <div style="margin-bottom: 10px;">
                    <div style="display: inline-block; width: 300px">
                        <span style="font-weight: bold"><?php echo get_phrase('stock'); ?>:</span>
                        <span><?php echo $car['stock']; ?></span>
                    </div>

                    <div style="display: inline-block; width: 300px">
                        <span style="font-weight: bold"><?php echo get_phrase('vin'); ?>:</span>
                        <span><?php echo $car['vin']; ?></span>
                    </div>
                </div>
            </div>
        </div>
	</div>
<?php endforeach; ?>
