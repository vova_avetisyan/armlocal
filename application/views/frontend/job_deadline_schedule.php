
<a class="btn_1 full-width send-cv" onclick="open_send_cv(<?php echo $listing_id; ?>, '<?php echo $listing_details['name']; ?>');">
    SEND CV
</a>

<?php $job_deadline = $this->db->get_where('job_deadline', array('listing_id' => $listing_id))->row_array(); ?>
<div class="text-center <?php echo strtolower(job_open($listing_id)) == 'closed' ? 'job_closed' : ''; ?>">
    <?php echo get_phrase('deadline'); ?>: <?php echo date('M d, Y', strtotime($job_deadline['date'])); ?>
</div>