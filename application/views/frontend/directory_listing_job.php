<div class="hero_in shop_detail" style="background: url(<?php echo base_url('uploads/listing_cover_photo/'.$listing_details['listing_cover']); ?>) center center no-repeat; background-size: cover;">
</div>
<!--/hero_in-->

<nav class="secondary_nav sticky_horizontal_2">
	<div class="container">
		<ul class="clearfix">
			<li><a href="#" data-href="listings_first_page" class="active"><?php echo get_phrase('description'); ?></a></li>

            <?php if(!empty($other_listings)){ ?>
                <li>
                    <a href="#" data-href="listings_second_page"><?php echo get_phrase('listings'); ?></a>
                </li>
		    <?php } ?>
        </ul>
	</div>
</nav>

<div class="container margin_60_35">
	<div class="row listings_first_page">
        <!-- Contact Form Base On Package-->
        <?php if(has_package_feature('ability_to_add_contact_form', $listing_details['user_id']) == 1): ?>
            <aside class="col-lg-4" id="sidebar">
                <div id="filters_col">
                    <a id="filters_col_bt_job">
                        Few more jobs you will love
                    </a>

                    <div class="filter_type">
                        <h6></h6>
                        <ul>
                            <?php foreach ($other_jobs as $other_job) { ?>
                                <li>
                                    <a href="<?php echo get_listing_url($other_job['id']); ?>">
                                        <label style="cursor: pointer"><?php echo $other_job['name']; ?></label>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>

                <div class="box_detail booking">
                    <a href="javascript:" onclick="addToWishList('<?php echo $listing_details['id']; ?>')" class="btn_1 full-width outline wishlist" id = "btn-wishlist"><i class="icon_heart"></i> <?php echo is_wishlisted($listing_details['id']) ? get_phrase('remove_from_wishlist') : get_phrase('add_to_wishlist'); ?></a>
                </div>

                <ul class="share-buttons">
                    <li><a href = "https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url();?>" class="fb-share" target="_blank"><i class="social_facebook"></i> Share</a></li>
                    <li><a href = "https://twitter.com/share?url=<?php echo current_url();?>" target = "_blank" class="twitter-share"><i class="social_twitter"></i> Tweet</a></li>
                    <li><a href = "http://pinterest.com/pin/create/link/?url=<?php echo current_url();?>" target="_blank" class="gplus-share"><i class="social_pinterest"></i> Pin</a></li>
                </ul>
            </aside>
        <?php endif; ?>

		<div class="col-lg-8">
			<section id="description" style="border-bottom: 0">
				<div class="detail_title_1">
					<div class="row">
						<div class="col-8">
                            <h1>
                                <?php echo $listing_details['name']; ?>

                                <?php $claiming_status = $this->db->get_where('claimed_listing', array('listing_id' => $listing_id))->row('status'); ?>
                                <?php if($claiming_status == 1): ?>
                                    <span class="claimed_icon" data-toggle="tooltip" title="<?php echo get_phrase('this_listing_is_verified'); ?>">
								<img src="<?php echo base_url('assets/frontend/images/verified.png'); ?>" width="30" />
							</span>
                                <?php endif; ?>
                            </h1>
						</div>
						<div class="col-4">
                            <!-- Send cv and deadline -->
                            <?php include 'job_deadline_schedule.php'; ?>
						</div>
					</div>
				</div>

				<div class="add_bottom_15">
					<?php
					$categories = json_decode($listing_details['categories']);
					for ($i = 0; $i < sizeof($categories); $i++):
						$this->db->where('id',$categories[$i]);
						$category_name = $this->db->get('category')->row()->name;
						?>
						<span class="loc_open mr-2">
							<a href="<?php echo site_url('home/filter_listings?category='.slugify($category_name).'&&status=all'); ?>"
								style="color: #32a067;">
								<?php echo $category_name;?>
								>
							</a>
						</span>
						<?php
					endfor;
					?>
				</div>
				<!-- /row -->

				<!-- Listing Type Wise Inner Page -->
                <?php include 'job_listing_inner_page.php'; ?>
				<!-- /row -->

                <hr>
                <h5><?php echo get_phrase('about_company'); ?></h5>
                <p>
                    <?php echo nl2br($listing_details['description']); ?>
                </p>

				<!-- Video File Base On Package-->
				<?php include 'video_player.php'; ?>

				<hr>
			</section>
			<!-- /section -->

			<?php $google_analytics_id = $this->db->get_where('listing', array('id' => $listing_id))->row('google_analytics_id'); ?>
			<!-- Global site tag (gtag.js) - Google Analytics -->
			<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $google_analytics_id; ?>"></script>
			<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', '<?php echo $google_analytics_id; ?>');
			</script>
		</div>
		<!-- /col -->
	</div>
	<!-- /row -->

    <?php include 'listings_second_page.php'; ?>
</div>
<!-- /container -->


<script type="text/javascript">
var isLoggedIn = '<?php echo $this->session->userdata('is_logged_in'); ?>';

// This function performs all the functionalities to add to wishlist
function addToWishList(listing_id) {
	if (isLoggedIn === '1') {
		$.ajax({
			type : 'POST',
			url : '<?php echo site_url('home/add_to_wishlist'); ?>',
			data : {listing_id : listing_id},
			success : function(response) {
				if (response == 'added') {
					$('#btn-wishlist').html('<i class="icon_heart"></i> <?php echo get_phrase('remove_from_wishlist'); ?>');
				}else {
					$('#btn-wishlist').html('<i class="icon_heart"></i> <?php echo get_phrase('add_to_wishlist'); ?>');
				}
			}
		});
	}else {
		loginAlert();
	}
}

// This function shows the Report listing form
function showClaimForm(){
	$('#claim_form').toggle();
	$('#report_form').hide();
}
// This function shows the Report listing form
function showReportForm() {
	$('#report_form').toggle();
	$('#claim_form').hide();
}

// This function return the number of different types of guests
function getTheGuestNumberForBooking(listing_type) {
	if (isLoggedIn === '1') {
		if (listing_type === "restaurant" || listing_type === "hotel") {
			$('#adult_guests_for_booking').val($('#adult_guests').val());
			$('#child_guests_for_booking').val($('#child_guests').val());
		}

		$('.contact-us-form').submit();
	}else {
		loginAlert();
	}

}
</script>

<!-- This map-category.php file has all the fucntions for showing the map, marker, map info and all the popup markups -->
<?php include 'assets/frontend/js/map/map-category.php'; ?>

<!-- This script is needed for providing the json file which has all the listing points and required information -->
<script>
createListingsMap({
	mapId: 'map',
	jsonFile: '<?php echo base_url('assets/frontend/single-listing-geojson/listing-id-'.$listing_id.'.json'); ?>'
});
</script>
