<div class="row listings_second_page" style="display: none">
    <div class="col-lg-12">
        <h1 class="text-center add_bottom_45">Active Listings</h1>
    </div>

    <div class="col-lg-12">
        <div class="row">
            <?php
            if(!empty($other_listings)){
                foreach($other_listings as $listing){ ?>
                    <!-- A Single Listing Starts-->
                    <div class="col-lg-4 col-md-4 listing-div " data-marker-id="<?php echo $listing['code']; ?>" id = "<?php echo $listing['code']; ?>">
                        <div class="strip grid <?php if($listing['is_featured'] == 1) echo 'featured-tag-border'; ?>">
                            <figure>
                                <a href="javascript::" class="wishlist-icon" onclick="addToWishList(this, '<?php echo $listing['id']; ?>')">
                                    <i class=" <?php echo is_wishlisted($listing['id']) ? 'fas fa-heart' : 'far fa-heart'; ?> "></i>
                                </a>
                                <?php if($listing['is_featured'] == 1){ ?>
                                    <a href="javascript::" class="featured-tag-grid"><?php echo get_phrase('featured'); ?></a>
                                <?php } ?>
                                <a href="<?php echo get_listing_url($listing['id']); ?>"  id = "listing-banner-image-for-<?php echo $listing['code']; ?>"  class="d-block h-100 img" style="background-image:url('<?php echo base_url('uploads/listing_thumbnails/'.$listing['listing_thumbnail']); ?>')">
                                    <!-- <img src="<?php echo base_url('uploads/listing_thumbnails/'.$listing['listing_thumbnail']); ?>" class="img-fluid" alt=""> -->
                                    <div class="read_more"><span><?php echo get_phrase('watch_details'); ?></span></div>
                                </a>
                                <small><?php echo $listing['listing_type'] == "" ? ucfirst(get_phrase('general')) : ucfirst(get_phrase($listing['listing_type'])) ; ?></small>
                            </figure>
                            <div class="wrapper <?php if($listing['is_featured'] == 1) echo 'featured-body'; ?>">
                                <h3 class="ellipsis">
                                    <a href="<?php echo get_listing_url($listing['id']); ?>"><?php echo $listing['name']; ?></a>
                                    <?php $claiming_status = $this->db->get_where('claimed_listing', array('listing_id' => $listing['id']))->row('status'); ?>
                                    <?php if($claiming_status == 1): ?>
                                        <span class="claimed_icon" data-toggle="tooltip" title="<?php echo get_phrase('this_listing_is_verified'); ?>">
                                                <img src="<?php echo base_url('assets/frontend/images/verified.png'); ?>" width="23" />
                                            </span>
                                    <?php endif; ?>
                                </h3>
                                <small>
                                    <?php
                                    $city 	 = $this->db->get_where('city', array('id' =>  $listing['city_id']))->row_array();
                                    $country = $this->db->get_where('country', array('id' =>  $listing['country_id']))->row_array();
                                    echo $city['name'].', '.$country['name'];
                                    ?>
                                </small>
                                <p class="ellipsis">
                                    <?php echo $listing['description']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- A Single Listing Ends-->
                <?php }
            } ?>
        </div>
    </div>
</div>