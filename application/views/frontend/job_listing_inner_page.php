<?php
$jobs = $this->db->get_where('job_details', array('listing_id' => $listing_details['id']))->result_array();

$category_names = '';
for ($i = 0; $i < sizeof($categories); $i++):
	$this->db->where('id',$categories[$i]);
	if($i != 0){
		$category_names .= ' / ';
	}
	$category_names .= $this->db->get('category')->row()->name;
endfor;

$city = $this->db->get_where('city', array('id' => $listing_details['id']))->row()->name;
?>
<h5><?php echo get_phrase('job_details'); ?></h5>
<?php foreach ($jobs as $job): ?>
	<div class="room_type first">
		<div class="row">
			<div class="col-md-4">
				<img src="<?php echo base_url('uploads/listing_thumbnails/'.$listing_details['listing_thumbnail']); ?>" class="img-fluid" alt="">
			</div>
            <div class="col-md-8">
                <div>
                    <p style="margin-bottom: 10px;">
                        <i class="fa fa-bookmark"></i>
                        <span style="font-weight: bold"><?php echo get_phrase('employment_term'); ?>:</span>
                        <span><?php echo $job['employment_term']; ?></span>
                    </p>
                </div>
                <div>
                    <p style="margin-bottom: 10px;">
                        <i class="fa fa-bookmark"></i>
                        <span style="font-weight: bold"><?php echo get_phrase('job_type'); ?>:</span>
                        <span><?php echo $job['job_type']; ?></span>
                    </p>
                </div>
                <div>
                    <p style="margin-bottom: 10px;">
                        <i class="fa fa-bookmark"></i>
                        <span style="font-weight: bold"><?php echo get_phrase('category'); ?>:</span>
                        <span><?php echo $category_names; ?></span>
                    </p>
                </div>
                <div>
                    <p style="margin-bottom: 20px;">
                        <i class="fa fa-bookmark"></i>
                        <span style="font-weight: bold"><?php echo get_phrase('location'); ?>:</span>
                        <span><?php echo $city; ?></span>
                    </p>
                </div>
                <p style="margin-bottom: 20px;">
                    <?php echo str_replace(',', ' / ', $job['variant']); ?>
                </p>
                <p style="margin-bottom: 10px;"><?php echo get_phrase('salary').': '.currency($job['price']); ?></p>
            </div>
		</div>
        <div class="row">
            <div class="col-md-12" style="margin-top: 10px">
                <h4><?php echo get_phrase('job_description'); ?>:</h4>
                <p style="margin-bottom: 10px;"><?php echo $job['description']; ?></p>
            </div>
            <div class="col-md-12" style="margin-top: 10px">
                <h4><?php echo get_phrase('job_responsibilities'); ?>:</h4>
                <p style="margin-bottom: 10px;"><?php echo $job['job_responsibilities']; ?></p>
            </div>
            <div class="col-md-12" style="margin-top: 10px">
                <h4><?php echo get_phrase('required_qualifications'); ?>:</h4>
                <p style="margin-bottom: 10px;"><?php echo $job['required_qualifications']; ?></p>
            </div>
        </div>
	</div>
<?php endforeach; ?>
