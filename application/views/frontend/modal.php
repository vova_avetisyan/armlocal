<script type="text/javascript">
function showAjaxModal(url, header)
{
    // SHOWING AJAX PRELOADER IMAGE
    jQuery('#modal_ajax .modal-body').html('<div style="text-align:center;margin-top:200px;"><img src="<?php echo base_url().'assets/global/bg-pattern-light.svg'; ?>" /></div>');
    jQuery('#modal_ajax .modal-title').html('...');
    // LOADING THE AJAX MODAL
    jQuery('#modal_ajax').modal('show', {backdrop: 'true'});

    // SHOW AJAX RESPONSE ON REQUEST SUCCESS
    $.ajax({
        url: url,
        success: function(response)
        {
            jQuery('#modal_ajax .modal-body').html(response);
            jQuery('#modal_ajax .modal-title').html(header);
        }
    });
}
</script>

<!-- (Ajax Modal)-->
<div id="modal_ajax" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal_heading"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal"><?php echo get_phrase('close'); ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
	function confirm_modal(delete_url)
	{
    console.log(delete_url);
      jQuery('#alert-modal').modal('show', {backdrop: 'static'});
      document.getElementById('update_link').setAttribute('href' , delete_url);
	}
	</script>

    <!-- (Normal Modal)-->
    <div class="modal fade" id="modal-4">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
                </div>


                <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                    <a href="#" class="btn btn-danger" id="delete_link"><?php echo get_phrase('delete');?></a>
                    <button type="button" class="btn btn-info" data-dismiss="modal"><?php echo get_phrase('cancel');?></button>
                </div>
            </div>
        </div>
    </div>

    <!-- Info Alert Modal -->
     <div id="alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-body p-4">
                      <div class="text-center">
                          <i class="dripicons-information h1 text-info"></i>
                          <h4 class="mt-2">Heads up!</h4>
                          <p class="mt-3">Are you sure?</p>
                          <button type="button" class="btn btn-info my-2" data-dismiss="modal">Cancel</button>
                          <a href="#" id="update_link" class="btn btn-danger my-2">Continue</a>
                      </div>
                  </div>
              </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
<script>
    function open_send_cv(id, name){
        var modal =  $('#send-sv-modal');
        modal.find('[name="listing_id"]').val(id);
        modal.find('.cv_listing_name').text(name);

        modal.find('.sv_first_view').show();
        modal.find('.sv_second_view').hide();
        modal.modal('show');
    }

    function submit_send_sv(){
        var form =  $('#send-sv-modal form');
        var formData = new FormData(form[0]);

        $.ajax({
            type:'POST',
            url: '/home/sendsvsubmit',
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: false,
            data: formData,
            success: function(response) {
                form.find('input').val('');

                form.find('.sv_first_view').hide();
                form.find('.sv_second_view').show();
            }
        });

        return false;
    }
</script>

<!-- (Send cv Modal)-->
<div id="send-sv-modal" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form  method="post" enctype="multipart/form-data" onsubmit="submit_send_sv(); return false;">
            <div class="modal-body">
                <input type="hidden" name="listing_id" value="">

                <div class="sv_first_view">
                    <p class="cv_listing_name" style="font-weight: bold; font-size: 18px;"></p>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="apply-as-guest-label">Email</p>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group field-email required">
                                <input type="text" id="email" class="border-focus-color form-control" name="email" placeholder="Your email" aria-required="true" required>
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p class="apply-as-guest-label">First name</p>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group field-firstName required">
                                <input type="text" id="firstName" class="form-control border-focus-color" name="first_name" placeholder="Please enter your first name" required>
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p class="apply-as-guest-label">Last name</p>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group field-lastName required">
                                <input type="text" id="lastName" class="border-focus-color form-control" name="last_name" placeholder="Please enter your last name" required>
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p class="apply-as-guest-label">Phone number</p>
                        </div>
                        <div class="col-md-8 info_phone_block">
                            <div class="form-group field-applyasguestform-phone required">
                                <div class="iti iti--allow-dropdown">
                                    <input type="tel" id="applyasguestform-phone" class="form-control" name="phone" autocomplete="off" aria-required="true" placeholder="000 123456" required>
                                </div>
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 applyCoverLetter">
                            <div class="form-group field-applyasguestform-cover_letter">
                                <label class="control-label" for="applyasguestform-cover_letter">Cover letter</label>
                                <textarea id="applyasguestform-cover_letter" class="form-control ckeditor" name="cover_letter" rows="8"></textarea>
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="apply-as-guest-file-label">
                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                                <strong>Attach CV in PDF, DOCX or JPG format</strong>
                            </p>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group field-applyasguestform-cv">
                                <input type="file" id="applyasguestform-cv" class="border-focus-color" name="cv" placeholder="cvPlaceHolder" required>
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <p class="apply-as-guest-file-label" style="color:#666;">
                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                                Attach other file in PDF, DOCX or JPG format (optional)
                            </p>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group field-applyasguestform-optional_attachment">
                                <input type="file" id="applyasguestform-optional_attachment" class="border-focus-color" name="optional_attachment" placeholder="Attach other file in PDF, DOCX or JPG format (optional)">
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <p class="sv_second_view">Data sent successfully.</p>
            </div>
            <div class="modal-footer" style="margin: 0 auto;">
                <button type="submit" class="btn btn-success sv_first_view">Submit</button>
                <button type="button" class="btn btn-default sv_second_view" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>

    </div>
</div>